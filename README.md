# Scutwork

A comprehensive real-time scoring app for cricket matches organized in or without a league structure.

## Development

### Pre-requisites
- [Docker](https://www.docker.com/community-edition)
- [docker-compose](https://docs.docker.com/compose/install/)


### Setup
```
$ cd scutwork
$ mkdir redis/app-data ardb
$ docker-compose build
$ docker-compose up -d
```

If everything goes right, you should see an output like below on `docker ps` with 4 containers in operation.
```
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                         NAMES
bfccd41f91b3        scutwork_nginx                "nginx -g 'daemon ..."   44 minutes ago      Up 44 minutes       0.0.0.0:8080->80/tcp               nginx
41714b8acdbe        scutwork_frontend   "/bin/bash /app/run.s"   13 hours ago        Up 12 hours         4000/tcp, 35729/tcp, 0.0.0.0:4000->3000/tcp   scutwork_frontend_1
12600f9d8e2a        scutwork_server     "/usr/local/bin/pytho"   13 hours ago        Up 12 hours         0.0.0.0:3208->3208/tcp                        scutwork_server_1
a076a88684b6        redis               "docker-entrypoint.sh"   13 hours ago        Up 12 hours         6379/tcp                                      scutwork_redis_1
```

### Running tests
```
$ cd scutwork
$ pytest test/
```

#### Running a specific test
```
$ pytest test/models/test_match_state::TestMatchState::test_dot_ball
```

Or if you want to print outputs
```
$ pytest -vvs test/models/test_match_state::TestMatchState::test_dot_ball
```

### Deployment
#### EC2

* Use `docker-machine`'s amazon-ec2 driver to launch an EC2 instance after having set up an AWS account with an IAM role designated for deployments (follow [docker-machine example](https://docs.docker.com/machine/examples/aws/#step-1-sign-up-for-aws-and-configure-credentials) to set up an AWS account with an IAM profile if you haven't done that already)
This will launch an EC2 instance and it would be accessible for use like a docker VM machine: 
```
$ docker-machine ls
NAME              ACTIVE   DRIVER      STATE     URL                        SWARM   DOCKER        ERRORS
my-dev-sandbox   *        amazonec2   Running   tcp://xx.xx.xx.xx:xxxx           v18.01.0-ce
```

You can inspect docker-machine's IP using `docker-machine ip my-dev-sandbox`. Grab the IP from the output of this command and save it to the environment variable `REACT_LIVE_MATCH_SERVICE_URL` as `http://MACHINE_IP`

* Build and deploy: `docker-compose -f docker-compose.prod.yml up -d --build`
* Navigate to `http://MACHINE_IP:8080`