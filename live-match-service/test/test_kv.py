import pytest
from models import store_manager
from models.store_manager import SlowFastKV, RedisKV, KeyMaker

import mockredis

__author__ = 'arbaz'


def test_slow_fast_kv():
    dct1 = store_manager.Testdict()
    dct2 = store_manager.Testdict()

    kv = SlowFastKV(slow_kv=dct1, fast_kv=dct2)

    assert kv.get("k1") is None

    kv.put("k1", "v1")
    assert kv.get("k1") == "v1"

    dct2.put("k2", "v2")
    assert kv.get("k2") == "v2"

    kv.sync(["k1"], fast_to_slow=True)
    assert dct2.get("k1") == "v1"


def test_redis_kv(monkeypatch, mock_redis):
    monkeypatch.setattr("redis.StrictRedis", mock_redis)
    redis_kv = RedisKV(host="localhost", port=6379)
    assert redis_kv.exists("k1") is False
    redis_kv.put("k1", "v1")
    assert redis_kv.exists("k1") is True
    assert redis_kv.get("k1") == "v1"


def test_redis_kv_hash(monkeypatch, mock_redis):
    monkeypatch.setattr("redis.StrictRedis", mock_redis)
    kv = RedisKV(host="localhost", port=6379)

    match_k1 = KeyMaker.make(["match", "1", "k1"])
    match_k2 = KeyMaker.make(["match", "1", "k2"])
    match_k3 = KeyMaker.make(["match", "1", "k3"])
    assert kv.get(match_k1) is None

    kv.put(match_k1, "v1")
    assert kv.get(match_k1) == "v1"

    kv.put(match_k2, "v2")
    kv.put(match_k3, "v3")

    assert kv.get(match_k3) == "v3"
    kv.delete(match_k3)

    assert kv.get(match_k3) is None

    kv.delete_hash(match_k1)
    assert kv.get(match_k2) is None
