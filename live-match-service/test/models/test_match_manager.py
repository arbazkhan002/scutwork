import pytest
from controllers import match_manager
from models import store_manager, dismissal
import pdb
import json
from models.store_manager import StoreManager
from test.core_utils import dummy_match_creator
from models.player_info import PlayerInfo
from models.match_state import MatchResult

__author__ = 'arbaz'


class TestMatchManager:
    """Runs `clear_store_manager` at start of each test"""

    @pytest.fixture(autouse=True)
    def setup_inning(self, request, inning_maker):
        store_manager.StoreManager.clear_redis_connection()
        yield

    def __play_dot_balls(self, inning_maker, number_dot_balls, is_power_play=False):
        local_match_sample = inning_maker.get_inning_sample()
        for i in range(1, number_dot_balls + 1):
            ball_event = inning_maker.get_ball_sample()
            ball_event.is_power_play = is_power_play
            ball_event.ball_number = i
            ball_event.ball_sequence = i
            match_state_after_ball = match_manager.add_ball(ball_event)
        return local_match_sample, match_state_after_ball

    def test_get_live_match(self, inning_maker):
        NUMBER_DOT_BALLS = 2
        local_match_sample, match_state_after_ball = self.__play_dot_balls(
            inning_maker, NUMBER_DOT_BALLS)
        ball_event = inning_maker.get_ball_sample()

        match_details = match_manager.get_match(match_id="1")
        match_state = match_manager.get_current_match_state(match_id="1")
        assert list(sorted(json.loads(match_state.to_json()).items())) == \
            list(sorted(json.loads(match_state_after_ball.to_json()).items())), (
                "current match state returned should be same as the "
                "expected match state after `NUMBER_DOT_BALLS`")

    def test_undo_first_legal_ball(self, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        ball_event.run_info.batruns_scored = 3
        ball_event.ball_number = 1
        ball_event.ball_sequence = 1
        match_state_after_ball = match_manager.add_ball(ball_event)

        match_state_after_undo = match_manager.undo_ball(ball_event)
        #assert match_state_after_undo is None

        ball_event.run_info.batruns_scored = 2
        match_state_after_ball = match_manager.add_ball(ball_event)
        assert match_state_after_ball is not None

        ball_striker = match_state_after_ball.player_striker.batting_details
        ball_non_striker = match_state_after_ball.player_non_striker.batting_details
        bowler = match_state_after_ball.player_current_bowler.bowling_details

        assert bowler.legal_balls_count == 1
        assert bowler.over == 0
        assert bowler.runs == 2
        assert len(bowler.over_balls) == 1

        assert ball_striker.runs == 2
        assert ball_striker.balls_faced == 1

        assert ball_non_striker.runs == 0
        assert ball_non_striker.balls_faced == 0

        assert match_state_after_ball.ball_number == 1

        over_0 = store_manager.StoreManager.get_match_over(
            match_state_after_ball.match_id,
            match_state_after_ball.inning_id, 0)
        assert (over_0.runs) == 2
        assert (over_0.ball_sequences) == [1]

    def test_undo_run_out(self, inning_maker):
        local_match_sample, match_state_after_ball = self.__play_dot_balls(
            inning_maker, 5)
        ball_event = inning_maker.get_ball_sample()
        ball_event.run_info.batruns_scored = 1
        ball_event.ball_number = 6
        ball_event.ball_sequence = 6
        ball_event.dismissal_info.batsman_dismissed = ball_event.get_non_striker()
        ball_event.dismissal_info.strike_changed = True
        ball_event.dismissal_info.dismissal_type = "runout"
        ball_event.dismissal_info.fielder = '2006'
        match_state_after_ball = match_manager.add_ball(ball_event)

        assert match_state_after_ball.player_striker is None
        assert match_state_after_ball.player_non_striker is not None
        match_state_after_undo = match_manager.undo_ball(ball_event)
        assert match_state_after_undo is not None

        ball_striker = match_state_after_undo.player_striker.batting_details
        ball_non_striker = match_state_after_undo.player_non_striker.batting_details
        bowler = match_state_after_undo.player_current_bowler.bowling_details

        assert bowler.legal_balls_count == 5
        assert bowler.over == 0
        assert bowler.runs == 0
        assert len(bowler.over_balls) == 1
        assert len(bowler.over_balls[0]) == 5

        assert ball_striker.runs == 0
        assert ball_striker.balls_faced == 5

        assert ball_non_striker.runs == 0
        assert ball_non_striker.balls_faced == 0

        assert match_state_after_undo.ball_number == 5

    def test_undo_last_bowl_of_over(self, inning_maker):
        local_match_sample, match_state_after_ball = self.__play_dot_balls(
            inning_maker, 6)
        bowler = match_state_after_ball.player_previous_bowler.bowling_details
        assert bowler.maidens == 1
        assert bowler.over == 1
        assert bowler.legal_balls_count == 0
        assert len(bowler.over_balls) == 1
        assert match_state_after_ball.player_striker.get_user_id() == '1002'
        assert match_state_after_ball.player_non_striker.get_user_id() == '1001'

        ball_event = inning_maker.get_ball_sample()
        ball_event.ball_sequence = 6
        ball_event.ball_number = 6
        match_state_after_undo = match_manager.undo_ball(ball_event)
        bowler = match_state_after_undo.player_current_bowler.bowling_details
        assert bowler.maidens == 0
        assert bowler.over == 0
        assert bowler.legal_balls_count == 5
        assert len(bowler.over_balls) == 1
        assert match_state_after_ball.player_striker.get_user_id() == '1002'
        assert match_state_after_ball.player_non_striker.get_user_id() == '1001'

    def test_undo_wide_ball(self, inning_maker):
        local_match_sample, match_state_after_ball = self.__play_dot_balls(
            inning_maker, 2)
        ball_event = inning_maker.get_ball_sample()
        ball_event.run_info.wide_runs = 1
        ball_event.ball_sequence = match_state_after_ball.ball_sequence
        ball_event.ball_number = match_state_after_ball.ball_number
        match_state_after_ball = match_manager.add_ball(ball_event)
        assert match_state_after_ball.wides == 1
        bowler = match_state_after_ball.player_current_bowler
        assert bowler.get_bowling_details().wides == 1
        assert bowler.get_bowling_details().runs == 1
        match_state_after_undo = match_manager.undo_ball(ball_event)
        assert match_state_after_undo is not None
        bowler = match_state_after_undo.player_current_bowler
        assert bowler.get_bowling_details().wides == 0
        assert bowler.get_bowling_details().runs == 0
        batting_details = match_state_after_undo.player_striker.get_batting_details()
        assert batting_details.bat_dismissal is None

    def test_undo_byes_ball(self, inning_maker):
        local_match_sample, match_state_after_ball = self.__play_dot_balls(
            inning_maker, 2)
        ball_event = inning_maker.get_ball_sample()
        ball_event.run_info.byes_runs = 1
        ball_event.ball_sequence = match_state_after_ball.ball_sequence
        ball_event.ball_number = match_state_after_ball.ball_number
        match_state_after_ball = match_manager.add_ball(ball_event)
        bowler = match_state_after_ball.player_current_bowler
        assert match_state_after_ball.score == 1
        assert match_state_after_ball.byes == 1
        assert bowler.get_bowling_details().runs == 0
        match_state_after_undo = match_manager.undo_ball(ball_event)
        assert match_state_after_undo is not None
        assert match_state_after_undo.score == 0
        assert match_state_after_undo.byes == 0
        bowler = match_state_after_undo.player_current_bowler
        assert bowler.get_bowling_details().runs == 0

    def test_undo_wicket(self, inning_maker):
        local_match_sample, match_state_after_ball = self.__play_dot_balls(
            inning_maker, 2)
        ball_event = inning_maker.get_ball_sample()
        ball_event.run_info.batruns_scored = 0
        ball_event.ball_sequence = match_state_after_ball.ball_sequence
        ball_event.ball_number = match_state_after_ball.ball_number
        ball_event.dismissal_info.batsman_dismissed = ball_event.get_striker()
        ball_event.dismissal_info.dismissal_type = "c"
        ball_event.dismissal_info.fielder = '2006'
        striker_id = ball_event.get_striker()
        match_state_after_ball = match_manager.add_ball(ball_event)
        assert match_state_after_ball.player_striker is None
        striker = match_state_after_ball._get_player_from_store(striker_id)
        assert striker.get_batting_details().bat_dismissal is not None
        match_state_after_undo = match_manager.undo_ball(ball_event)
        assert match_state_after_undo is not None
        assert match_state_after_undo.player_striker is not None
        batting_details = match_state_after_undo.player_striker.get_batting_details()
        assert batting_details.bat_dismissal is None

    def test_undo_wide_ball_wicket(self, inning_maker):
        local_match_sample, match_state_after_ball = self.__play_dot_balls(
            inning_maker, 2)
        ball_event = inning_maker.get_ball_sample()
        ball_event.run_info.wide_runs = 1
        ball_event.ball_sequence = match_state_after_ball.ball_sequence
        ball_event.ball_number = match_state_after_ball.ball_number
        ball_event.dismissal_info.batsman_dismissed = ball_event.get_striker()
        ball_event.dismissal_info.dismissal_type = "st"
        ball_event.dismissal_info.fielder = '2006'
        striker_id = ball_event.get_striker()
        match_state_after_ball = match_manager.add_ball(ball_event)
        assert match_state_after_ball.player_striker is None
        striker = match_state_after_ball._get_player_from_store(striker_id)
        assert striker.get_batting_details().bat_dismissal is not None
        match_state_after_undo = match_manager.undo_ball(ball_event)
        assert match_state_after_undo is not None
        assert match_state_after_undo.player_striker is not None
        batting_details = match_state_after_undo.player_striker.get_batting_details()
        assert batting_details.bat_dismissal is None
        bowler = match_state_after_undo.player_current_bowler
        assert bowler.get_bowling_details().runs == 0

    def test_undo_power_play_wicket(self, inning_maker):
        local_match_sample, match_state_after_ball = self.__play_dot_balls(
            inning_maker, 4, is_power_play=True)
        ball_event = inning_maker.get_ball_sample()
        ball_event.is_power_play = True
        ball_event.run_info.batruns_scored = 0
        ball_event.ball_sequence = match_state_after_ball.ball_sequence
        ball_event.ball_number = match_state_after_ball.ball_number
        ball_event.dismissal_info.batsman_dismissed = ball_event.get_striker()
        ball_event.dismissal_info.dismissal_type = "c"
        ball_event.dismissal_info.fielder = '2006'
        match_state_after_ball = match_manager.add_ball(ball_event)
        assert match_state_after_ball.power_play_over_details(
            ball_event.over_number) == (0, 1)
        match_state_after_undo = match_manager.undo_ball(ball_event)
        assert match_state_after_undo is not None
        assert match_state_after_undo.power_play_over_details(
            ball_event.over_number) == (0, 0)

    def test_undo_inning_end(self, inning_maker):
        local_sample = inning_maker.get_inning_sample(max_overs=1)
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={3: 2, 4: 2})

        assert match_state_after_ball.inning_end == True
        assert match_state_after_ball.match_end == False
        # pdb.set_trace()
        next_inning_match_state = match_manager.make_next_innings(
            match_id=match_state_after_ball.match_id)
        assert next_inning_match_state.batting_team == match_state_after_ball.bowling_team
        assert next_inning_match_state.player_striker is None
        assert next_inning_match_state.target_score == 5

        curr_match_state = match_manager.get_current_match_state(match_id="1")
        assert curr_match_state.inning_id == "2"

        # undo last ball of prev inning
        ball_event.ball_number = 5
        ball_event.ball_sequence = 6
        match_manager.undo_ball(ball_event)
        curr_match_state = match_manager.get_current_match_state(match_id="1")
        assert curr_match_state.ball_number == 5
        assert curr_match_state.inning_id == "1"

    def test_force_end(self, inning_maker):
        local_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={3: 2, 4: 2})

        match_manager.force_inning_end(ball_event.match_id, ball_event.inning_id)
        curr_match_state = match_manager.get_current_match_state(match_id="1")

        assert curr_match_state.inning_end == True
        assert curr_match_state.match_end == False

        match_manager.force_match_end(ball_event.match_id)
        curr_match_state = match_manager.get_current_match_state(match_id="1")
        assert curr_match_state.match_end == True
        match = match_manager.get_match(ball_event.match_id)
        assert match.get_match_result() == MatchResult.GAME_ABANDONED

    def test_get_over_history(self, inning_maker):
        local_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={3: 2, 4: 2})

        # Start of 2nd over
        ball_event.ball_sequence = 7
        ball_event.ball_number = 6
        ball_event.bowler = PlayerInfo.player_info_from_map({
            'name': "2002",
            'player_id': "2002",
            'team_id': '2'
        })

        NUMBER_BALLS = 5
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=NUMBER_BALLS, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={3: 2, 4: 2})

        ovh = match_manager.get_over_history("1", "1", over_till=1, num_overs=1)

        assert len(ovh["over_history"]) == 1
        assert len(ovh["over_history"][0]["ball_sequences"]) == NUMBER_BALLS
        assert ovh["next_scroll_at"]["over_till"] == 0
        assert ovh["over_history"][0]["batting_team"]["team_name"] == "1000"

    def test_get_over_history_across_inning(self, inning_maker):
        local_sample = inning_maker.get_inning_sample(max_overs=2)
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={3: 2, 4: 2})

        # Start of 2nd over
        ball_event.ball_sequence = 7
        ball_event.ball_number = 6
        ball_event.bowler = PlayerInfo.player_info_from_map({
            'name': "2002",
            'player_id': "2002",
            'team_id': '2'
        })

        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={3: 2, 4: 2})

        #match_manager.force_inning_end(ball_event.match_id, ball_event.inning_id)
        next_inning_match_state = match_manager.make_next_innings(
            match_id=match_state_after_ball.match_id)
        ball_event = inning_maker.get_second_inning_ball_sample()
        ball_event.run_info.batruns_scored = 2
        match_state_after_ball = match_manager.add_ball(ball_event)

        ovh = match_manager.get_over_history(match_id="1", inning_id="2",
                                             over_till=0, num_overs=3)

        assert len(ovh["over_history"]) == 3
        assert ovh["next_scroll_at"]["inning_id"] == "1"
        assert ovh["next_scroll_at"]["over_till"] == -1


#    def test_undo_first_bowl_of_new_over(self, inning_maker):
#        local_match_sample, match_state_after_ball = self.__play_dot_balls(inning_maker, 6)
#        ball_event = inning_maker.get_ball_sample()
#        ball_event.ball_sequence = 7
#        ball_event.ball_number = 7
#        ball_event.striker = '1002'
#        ball_event.non_striker = '1001'
#        ball_event.bowler = '2007'
#        match_state_after_new_over_start = match_manager.add_ball(ball_event)
#        assert match_state_after_new_over_start.player_current_bowler.get_user_id() == '2007'
#        assert match_state_after_new_over_start.player_striker.get_user_id() == '1002'
#        assert match_state_after_new_over_start.player_non_striker.get_user_id() == '1001'
#
