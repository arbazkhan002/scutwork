import pytest
from models import ball_details
from models.dismissal import DismissalInfoSchema
from copy import deepcopy
from pprint import pprint

__author__ = 'arbaz'


def test_ui_output(ball_data):
    ball_event = ball_details.BallDetails.ball_from_map(deepcopy(ball_data))
    value = ball_event.dismissal_info
    schema = DismissalInfoSchema()
    result = schema.dump(value)
    pprint(result.data)
    # basic tests
    assert result.data['fielder'] is None
    assert result.data['dismissal_type'] is None


def test_ui_input(ball_data):
    value = deepcopy(ball_data)
    schema = DismissalInfoSchema()
    result = schema.load(value)
    pprint(result.data)
    # basic tests
    assert result.data.fielder is None
    assert result.data.dismissal_type is None
