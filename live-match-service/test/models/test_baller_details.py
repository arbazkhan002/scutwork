import pytest
from models import ball_details
from copy import deepcopy
from models.bowler_details import BowlerDetailsUISchema
from pprint import pprint
from models.dismissal import DismissalInfoSchema
import pdb

__author__ = 'arbaz'


def test_ui_output(inning_maker):
    match_state_sample = inning_maker.get_match_state_sample()
    value = match_state_sample.player_current_bowler.get_bowling_details()
    schema = BowlerDetailsUISchema()
    result = schema.dump(value)

    assert result.data['user_id'] == '2001'
    assert result.data['runs'] == 0
    assert result.data['legal_balls_count'] == 0
    assert result.data['over'] == 0
    assert result.data['wides'] == 0
    assert result.data['maidens'] == 0
    assert result.data['dismissals'] == 0
    assert result.data['bowling_figures'] == None

    match_state_sample.player_current_bowler.bowling_details.legal_balls_count = 1
    match_state_sample.player_current_bowler.bowling_details.runs = 1
    value = match_state_sample.player_current_bowler.get_bowling_details()
    assert value.get_bowling_figures() == "0.1-0-1-0"
    result = schema.dump(value)
    assert result.data['bowling_figures'] == "0.1-0-1-0"
