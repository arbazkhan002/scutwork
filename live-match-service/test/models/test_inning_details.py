import pytest
import pdb
from models import store_manager, inning_details, match_state
from controllers import match_manager
from pprint import pprint
from test.conftest import inning_maker
from test.core_utils import dummy_match_creator
from copy import deepcopy


__author__ = 'vaibhav'


class TestUIOutput:

    @pytest.fixture(autouse=True)
    def setup_innings(self, request, inning_maker):
        store_manager.StoreManager.clear_redis_connection()
        yield

    def test_inning_details_ui_output(self, inning_maker):
        NUMBER_BALLS = 4
        local_match_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=NUMBER_BALLS, init_inning_obj=local_match_sample,
            init_ball_event=ball_event, runs_sequence_sparse={1: 1, 3: 2})
        schema = inning_details.InningDetailsUISchema()
        # pdb.set_trace()
        inning = store_manager.StoreManager.get_inning_detail(local_match_sample.match_id,
                                                              local_match_sample.inning_id)
        result = schema.dump(inning).data
        assert result["score"] == 3
        assert result["wickets"] == 0
        assert result["overs"] == 0
        assert result["wides"] == 0
        assert result["nbs"] == 0
        assert result["byes"] == 0
        assert result["ball_number"] == 4
        assert result["batting_team"]["team_name"] == "1000"

        assert result["match_id"] == "1"
        assert result["inning_id"] == "1"
        assert len(result["batsman_ids"]) == 2
        assert len(result["bowler_ids"]) == 1

        assert len(result["batsman_details"]) == 2
        assert len(result["bowler_details"]) == 1

        total_balls_faced = 0
        initial_striker = ball_event.get_striker()
        # Ensure all batsman have their respective details.
        for batsman_detail in result["batsman_details"]:
            assert "name" in batsman_detail
            assert batsman_detail['runs'] == (
                1 if batsman_detail["player_id"] == initial_striker
                else 2)
            assert batsman_detail['fours'] == 0
            assert batsman_detail['sixes'] == 0
            total_balls_faced = total_balls_faced + batsman_detail['balls_faced']
            assert batsman_detail['bat_dismissal'] is None
        assert total_balls_faced == NUMBER_BALLS

        for bowler_detail in result["bowler_details"]:
            assert "name" in bowler_detail
            assert bowler_detail['runs'] == 3
            assert bowler_detail['legal_balls_count'] == NUMBER_BALLS
            assert bowler_detail['user_id'] == '2001'
            assert bowler_detail['over'] == 0
            assert bowler_detail['wides'] == 0
            assert bowler_detail['maidens'] == 0
            assert bowler_detail['dismissals'] == 0

    def test_wicket_fall(self, inning_maker):
        NUMBER_DOT_BALLS = 2
        local_match_sample = inning_maker.get_inning_sample()
        for i in range(NUMBER_DOT_BALLS):
            ball_event = inning_maker.get_ball_sample()
            ball_event.ball_number = i
            ball_event.ball_sequence = i
            match_state_after_ball = match_manager.add_ball(ball_event)

        ball_event.ball_sequence = NUMBER_DOT_BALLS
        ball_event.ball_number = match_state_after_ball.ball_number
        local_ball_data = ball_event
        local_ball_data.dismissal_info.batsman_dismissed = local_ball_data.get_striker()
        local_ball_data.dismissal_info.bowler = local_ball_data.get_bowler()
        local_ball_data.dismissal_info.dismissal_type = "b"

        match_state_after_ball = match_manager.add_ball(local_ball_data)
        inning = store_manager.StoreManager.get_inning_detail(local_match_sample.match_id,
                                                              local_match_sample.inning_id)

        assert len(inning.fow) == 1
        print(inning.fow)
        assert inning.fow[0].over == match_state_after_ball.over_number
        assert inning.fow[0].ball_number == match_state_after_ball.ball_number
        assert inning.fow[0].timestamp > 0

    def test_bowling_order(self, inning_maker):
        NUMBER_BALLS = 6
        local_match_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=NUMBER_BALLS, init_inning_obj=local_match_sample,
            init_ball_event=ball_event)

        # change the bowler
        ball_event.ball_sequence = match_state_after_ball.ball_sequence
        ball_event.bowler = store_manager.StoreManager.get_player_info("2002")
        match_state_after_ball = match_manager.add_ball(ball_event)

        inning = store_manager.StoreManager.get_inning_detail(local_match_sample.match_id,
                                                              local_match_sample.inning_id)
        assert len(inning.bowler_ids) == 2
        assert inning.bowler_ids == ["2001", "2002"]

    def test_batting_order(self, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        NUMBER_BALLS = 1
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=NUMBER_BALLS, init_inning_obj=local_match_sample,
            init_ball_event=ball_event)

        # get batsman out
        ball_event.ball_sequence = NUMBER_BALLS + 1
        ball_event.ball_number = match_state_after_ball.ball_number
        local_ball_data = deepcopy(ball_event)
        local_ball_data.dismissal_info.batsman_dismissed = local_ball_data.get_striker()
        local_ball_data.dismissal_info.bowler = local_ball_data.get_bowler()
        local_ball_data.dismissal_info.dismissal_type = "b"

        match_state_after_ball = match_manager.add_ball(local_ball_data)

        # get new batsman
        ball_event.ball_sequence = NUMBER_BALLS + 2
        ball_event.ball_number = match_state_after_ball.ball_number
        local_ball_data = deepcopy(ball_event)
        ball_event.striker = store_manager.StoreManager.get_player_info("1003")
        match_state_after_ball = match_manager.add_ball(ball_event)

        inning = store_manager.StoreManager.get_inning_detail(local_match_sample.match_id,
                                                              local_match_sample.inning_id)
        assert len(inning.batsman_ids) == 3
        assert inning.batsman_ids == ['1001', '1002', '1003']

    def test_power_play_details(self, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        ball_event.is_power_play = True
        NUMBER_BALLS = 5
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=NUMBER_BALLS, init_inning_obj=local_match_sample,
            init_ball_event=ball_event, runs_sequence_sparse={1: 4, 3: 1})

        schema = inning_details.InningDetailsUISchema()
        # pdb.set_trace()
        inning = store_manager.StoreManager.get_inning_detail(local_match_sample.match_id,
                                                              local_match_sample.inning_id)
        result = schema.dump(inning).data

        assert "power_play_details" in result
        assert len(result["power_play_details"]) == 1
        assert result["power_play_details"][0]["bowler"]["player_id"] == "2001"
        assert result["power_play_details"][0]["runs"] == 5
