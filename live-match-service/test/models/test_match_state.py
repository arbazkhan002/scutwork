import pytest
from models import ball_details, store_manager, match_state
from copy import deepcopy
import pdb
from models.match_state import MatchStateUISchema, MatchResult
from pprint import pprint
from controllers import match_manager
from test.conftest import inning_maker
from models.player_info import PlayerInfo
from test.core_utils import dummy_match_creator
__author__ = 'arbaz'


class TestMatchState:
    """Runs `clear_store_manager` at start of each test"""

    @pytest.fixture(autouse=True)
    def setup_inning(self, request, inning_maker):
        store_manager.StoreManager.clear_redis_connection()
        yield

    def test_first_match_state_is_empty(self, inning_maker):
        inning_sample = inning_maker.get_inning_sample()
        match_state_object = match_state.MatchState.from_inning(inning_sample)
        assert match_state_object.is_empty(), "First match state should be empty"

    def test_dot_ball(self, inning_maker, ball_data):
        """ball_data is defined in test/conftest.py"""
        local_sample = inning_maker.get_inning_sample()
        ball_event = ball_details.BallDetails.ball_from_map(deepcopy(ball_data))

        #print("Start: {}".format(local_sample.match_state.to_json()))
        #print("Event: {}".format(ball_event.to_json()))
        # pdb.set_trace()
        match_state_after_ball = match_manager.add_ball(ball_event)
        #print("End: {}".format(local_sample.match_state.to_json()))
        ball_striker = match_state_after_ball.player_striker.batting_details
        bowler = match_state_after_ball.player_current_bowler.bowling_details
        assert bowler.legal_balls_count == 1
        assert bowler.over == 0
        assert bowler.runs == 0
        assert len(bowler.over_balls) == 1

        #print("Old-striker runs: {}({})".format(ball_striker.runs, ball_striker.balls_faced))
        assert ball_striker.runs == 0
        assert ball_striker.balls_faced == 1

        assert match_state_after_ball.ball_number == 1

    def test_single_legal_ball(self, inning_maker):
        """ball_data is defined in test/conftest.py"""
        local_sample = inning_maker.get_inning_sample()
        local_ball_data = deepcopy(inning_maker.get_ball_sample())
        local_ball_data.run_info.batruns_scored = 1
        ball_event = local_ball_data

        match_state_after_ball = match_manager.add_ball(ball_event)

        ball_striker = match_state_after_ball.player_non_striker.batting_details
        bowler = match_state_after_ball.player_current_bowler.bowling_details
        assert bowler.legal_balls_count == 1
        assert bowler.over == 0
        assert bowler.runs == 1
        assert len(bowler.over_balls) == 1

        assert ball_striker.runs == 1
        assert ball_striker.balls_faced == 1

    def test_wide_ball(self, inning_maker):
        """ball_data is defined in test/conftest.py"""
        local_sample = inning_maker.get_inning_sample()
        local_ball_data = deepcopy(inning_maker.get_ball_sample())
        local_ball_data.run_info.wide_runs = 1
        ball_event = local_ball_data

        match_state_after_ball = match_manager.add_ball(ball_event)

        ball_striker = match_state_after_ball.player_non_striker.batting_details
        bowler = match_state_after_ball.player_current_bowler.bowling_details
        assert bowler.runs == 1
        assert bowler.wides == 1
        assert bowler.legal_balls_count == 0
        assert bowler.over == 0
        assert len(bowler.over_balls) == 1

        assert ball_striker.runs == 0
        assert ball_striker.balls_faced == 0
        assert match_state_after_ball.wides == 1

    def test_end_over(self, inning_maker, ball_data):
        """ball_data is defined in test/conftest.py"""
        local_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={})
        assert match_state_after_ball.ball_number == 0, "At over end, balls are reset for innings"
        assert match_state_after_ball.over_number == 1, "Over number should be incremented"
        assert match_state_after_ball.player_current_bowler is None, ("Bowler is set to None so that"
                                                                      "UI knows to request for new bowler")
        bowler = match_state_after_ball.player_previous_bowler
        assert bowler is not None
        assert bowler.bowling_details is not None
        assert bowler.bowling_details.over == 1,  "Over number should be incremented"
        assert bowler.bowling_details.legal_balls_count == 0, "At over end, balls are reset for bowler"

        new_striker = match_state_after_ball.player_striker.get_user_id()
        assert ball_event.get_non_striker() == new_striker

    def test_start_new_over(self, inning_maker, ball_data):
        local_sample = inning_maker.get_inning_sample()
        for i in range(1, 7):
            ball_event = inning_maker.get_ball_sample()
            ball_event.ball_number = i
            ball_event.ball_sequence = i
            match_state_after_ball = match_manager.add_ball(ball_event)

        ball_event.ball_sequence = 7
        ball_event.bowler = PlayerInfo.player_info_from_map({
            'name': "2002",
            'player_id': "2002",
            'team_id': '2'
        })

        match_state_after_ball = match_manager.add_ball(ball_event)
        assert match_state_after_ball.ball_sequence == 7
        assert match_state_after_ball.player_current_bowler.get_user_id() == "2002"
        assert match_state_after_ball.bowlers == [
            "2001", "2002"], "Error on current set of bowlers"
        assert "2002" not in match_state_after_ball.bowlers_to_bowl, "Current bowler can't bowl next over"

        over_0 = store_manager.StoreManager.get_match_over(
            match_state_after_ball.match_id,
            match_state_after_ball.inning_id, 0)
        assert len(over_0.ball_sequences) == 6
        assert set(over_0.ball_sequences) == set(range(1, 7))
        assert over_0.player_striker == "1001"

        over_1 = store_manager.StoreManager.get_match_over(
            match_state_after_ball.match_id,
            match_state_after_ball.inning_id, 1)
        assert len(over_1.ball_sequences) == 1
        assert over_1.ball_sequences == [7]

    def test_wicket_ball(self, inning_maker):
        NUMBER_DOT_BALLS = 3
        local_match_sample = inning_maker.get_inning_sample()
        for i in range(1, NUMBER_DOT_BALLS):
            ball_event = inning_maker.get_ball_sample()
            ball_event.ball_number = i
            ball_event.ball_sequence = i
            match_state_after_ball = match_manager.add_ball(ball_event)

        ball_event.ball_sequence = NUMBER_DOT_BALLS
        local_ball_data = ball_event
        local_ball_data.dismissal_info.batsman_dismissed = local_ball_data.get_striker()
        local_ball_data.dismissal_info.dismissal_type = "b"
        local_ball_data.dismissal_info.bowler = local_ball_data.get_bowler()

        match_state_after_ball = match_manager.add_ball(local_ball_data)
        last_dismissed_batsman = match_state_after_ball.last_dismissed_batsman
        assert match_state_after_ball.player_striker is None
        assert last_dismissed_batsman.get_user_id() == local_ball_data.get_striker()
        bowler_details = match_state_after_ball.player_current_bowler.get_bowling_details()
        assert len(bowler_details.dismissals) == 1
        assert bowler_details.dismissals[0] == local_ball_data.dismissal_info
        batting_details = last_dismissed_batsman.get_batting_details()
        assert batting_details.bat_dismissal == local_ball_data.dismissal_info

        assert batting_details.balls_faced == NUMBER_DOT_BALLS
        assert batting_details.runs == 0
        over_0 = store_manager.StoreManager.get_match_over(
            match_state_after_ball.match_id,
            match_state_after_ball.inning_id, 0)
        assert (over_0.runs) == 0
        assert (over_0.wickets) == 1

    def test_wicket_with_runs_ball(self, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        local_ball_data = inning_maker.get_ball_sample()

        local_ball_data.run_info.batruns_scored = 1
        local_ball_data.dismissal_info.batsman_dismissed = local_ball_data.get_non_striker()
        local_ball_data.dismissal_info.dismissal_type = "runout"
        local_ball_data.dismissal_info.fielder = "2002"
        local_ball_data.dismissal_info.bowler = local_ball_data.get_bowler()

        match_state_after_ball = match_manager.add_ball(local_ball_data)
        last_dismissed_batsman = match_state_after_ball.last_dismissed_batsman
        assert match_state_after_ball.player_striker is None, (
            "No matter what happened at the runout, striker is decided by runs scored")
        assert last_dismissed_batsman.get_user_id() == local_ball_data.get_non_striker()
        out_batsman_details = last_dismissed_batsman.get_batting_details()
        assert out_batsman_details.balls_faced == 0
        assert out_batsman_details.bat_dismissal == local_ball_data.dismissal_info
        n_o_batsman_details = match_state_after_ball.player_non_striker.get_batting_details()
        n_o_batsman_details.balls_faced == 1
        n_o_batsman_details.runs == 1

    def test_consecutive_wickets(self, inning_maker):
        NUMBER_DOT_BALLS = 3
        local_match_sample = inning_maker.get_inning_sample()
        for i in range(1, NUMBER_DOT_BALLS):
            ball_event = inning_maker.get_ball_sample()
            ball_event.ball_number = i
            ball_event.ball_sequence = i
            match_state_after_ball = match_manager.add_ball(ball_event)

        ball_event.ball_sequence = NUMBER_DOT_BALLS
        ball_event.ball_number = NUMBER_DOT_BALLS
        local_ball_data = ball_event
        local_ball_data.dismissal_info.batsman_dismissed = local_ball_data.get_striker()
        local_ball_data.dismissal_info.dismissal_type = "b"

        # 1st-wicket
        match_manager.add_ball(local_ball_data)

        ball_event.ball_sequence = NUMBER_DOT_BALLS + 1
        ball_event.ball_number = NUMBER_DOT_BALLS + 1
        NEW_BATSMAN_ID = "1003"
        local_ball_data.striker = PlayerInfo.player_info_from_map(
            {'name': NEW_BATSMAN_ID, 'player_id': NEW_BATSMAN_ID, 'team_id': '1'})
        local_ball_data.dismissal_info.batsman_dismissed = NEW_BATSMAN_ID
        local_ball_data.dismissal_info.dismissal_type = "b"

        match_state_after_2nd_wicket = match_manager.add_ball(local_ball_data)

    def test_bowlers_to_bowl(self, inning_maker):
        local_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={})

        # Start of 2nd over
        assert len(match_state_after_ball.bowlers_to_bowl) == 10

        ball_event.ball_sequence = 7
        ball_event.bowler = PlayerInfo.player_info_from_map({
            'name': "2002",
            'player_id': "2002",
            'team_id': '2'
        })

        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_match_state=match_state_after_ball,
            init_ball_event=ball_event, runs_sequence_sparse={})

        # Start of 3rd over
        assert len(match_state_after_ball.bowlers_to_bowl) == 10

        first_bowler = match_state_after_ball.bowlers_to_bowl[0]
        assert first_bowler == "2001"

    def test_power_play_details(self, inning_maker):
        local_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        ball_event.is_power_play = True

        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=1, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={})

        assert len(match_state_after_ball.power_play_details) == 1
        assert 0 in match_state_after_ball.power_play_details
        assert match_state_after_ball.power_play_details[0] == "2001"

    def test_inning_end(self, inning_maker):
        local_sample = inning_maker.get_inning_sample(max_overs=1)
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={3: 2, 4: 2})

        assert match_state_after_ball.inning_end == True
        assert match_state_after_ball.match_end == False
        next_inning_match_state = match_manager.make_next_innings(
            match_id=match_state_after_ball.match_id)
        assert next_inning_match_state.batting_team == match_state_after_ball.bowling_team
        assert next_inning_match_state.player_striker is None
        assert next_inning_match_state.target_score == 5

    def test_forced_end(self, inning_maker):
        local_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={3: 2, 4: 2})

        match_state_after_ball.set_inning_end(True)
        new_match_state = match_manager.get_current_match_state(match_id="1")
        assert new_match_state.inning_end == True

    def test_match_end(self, inning_maker):
        local_sample = inning_maker.get_inning_sample(max_overs=1)
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={3: 0, 4: 1})
        next_inning_match_state = match_manager.make_next_innings(
            match_id=match_state_after_ball.match_id)
        ball_event = inning_maker.get_second_inning_ball_sample()
        ball_event.run_info.batruns_scored = 2
        match_state_after_ball = match_manager.add_ball(ball_event)
        assert match_state_after_ball.score == 2
        assert match_state_after_ball.inning_end == True
        assert match_state_after_ball.match_end == True
        match = match_manager.get_match(ball_event.match_id)
        assert match.get_match_result() == MatchResult.AWAY_TEAM_WON


class TestUIOutput:

    @pytest.fixture(autouse=True)
    def setup_inning(self, request, inning_maker):
        store_manager.StoreManager.clear_redis_connection()
        yield

    def test_match_state_ui_output(self, inning_maker):
        NUMBER_DOT_BALLS = 4
        local_match_sample = inning_maker.get_inning_sample()
        for i in range(1, NUMBER_DOT_BALLS):
            ball_event = inning_maker.get_ball_sample()
            ball_event.ball_number = i
            ball_event.ball_sequence = i
            match_state_after_ball = match_manager.add_ball(ball_event)
        schema = MatchStateUISchema()
        result = schema.dump(match_state_after_ball).data
        assert result["match_id"] == "1"

        # Test if profiles are getting embedded in player fields
        assert "name" in result["player_striker"]
        assert "name" in result["player_non_striker"]
        assert "name" in result["player_current_bowler"]

        # Test if batting/bowling details are getting embedded in player fields
        assert "runs" in result["player_striker"]
        assert "maidens" in result["player_current_bowler"]

        assert result["player_striker"]["name"] == "1001"

        assert isinstance(result["players_bowling_team"], list)
        assert isinstance(result["batsmen_to_bat"], list)
        assert len(result["players_bowling_team"]) == 11
        assert len(result["batsmen_to_bat"]) == 9, "11 to bat minus 2 openers"
        assert len(result["bowlers_to_bowl"]) == 10, "11 to bowl minus 1st over's bowler"
        assert isinstance(result["players_bowling_team"], list)

        assert all(
            'player_id' in x and 'name' in x for x in result["players_bowling_team"])

        assert result["players_bowling_team"][0]["player_id"] == "2001"
        assert result["players_bowling_team"][0]["name"] == "2001"

        assert result["batsmen_to_bat"][0]["player_id"] == "1003"

        assert result["batting_team"]["team_id"] == "1"
        assert result["bowling_team"]["team_id"] == "2"
        assert len(result["over_history"]) == 1
        assert len(result["over_history"][0]["balls"]) == NUMBER_DOT_BALLS - 1
        assert result["over_history"][0]["runs"] == 0
        assert "score" in result["over_history"][0]["player_striker"]
        assert result["over_history"][0]["player_striker"][
            "score"] == "0({})".format(NUMBER_DOT_BALLS - 1)

        assert "match_details" in result
        assert len(result["match_details"]["short_inning_details"]) == 1

    def test_bowlers_to_bowl_output(self, inning_maker):
        local_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={})

        # Start of 2nd over
        ball_event.ball_sequence = 7
        ball_event.bowler = PlayerInfo.player_info_from_map({
            'name': "2002",
            'player_id': "2002",
            'team_id': '2'
        })

        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_match_state=match_state_after_ball,
            init_ball_event=ball_event, runs_sequence_sparse={})

        # Start of 3rd over
        first_bowler = match_state_after_ball.bowlers_to_bowl[0]
        assert first_bowler == "2001"
        schema = MatchStateUISchema()
        result = schema.dump(match_state_after_ball).data

        assert "bowling_figures" in result["bowlers_to_bowl"][0]
        assert result["bowlers_to_bowl"][0]["bowling_figures"] == "1-1-0-0"

    def test_powerplay_details(self, inning_maker):
        local_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        ball_event.is_power_play = True
        match_state_after_ball = dummy_match_creator.play_balls(
            number_balls=6, init_inning_obj=local_sample,
            init_ball_event=ball_event, runs_sequence_sparse={3: 2, 5: 2})

        schema = MatchStateUISchema()
        result = schema.dump(match_state_after_ball).data
        assert len(result["power_play_details"]) == 1
        assert result["power_play_details"][0]["runs"] == 4
        assert result["power_play_details"][0]["wickets"] == 0

    def test_powerplay_with_wickets(self, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        ball_event = inning_maker.get_ball_sample()
        ball_event.ball_sequence = 1
        ball_event.is_power_play = True
        local_ball_data = ball_event
        local_ball_data.dismissal_info.batsman_dismissed = local_ball_data.get_striker()
        local_ball_data.dismissal_info.dismissal_type = "b"
        local_ball_data.dismissal_info.bowler = local_ball_data.get_bowler()
        match_state_after_ball = match_manager.add_ball(local_ball_data)
        schema = MatchStateUISchema()
        result = schema.dump(match_state_after_ball).data
        assert len(result["power_play_details"]) == 1
        assert result["power_play_details"][0]["wickets"] == 1
