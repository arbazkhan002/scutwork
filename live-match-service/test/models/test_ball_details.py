import pytest
from models import ball_details
from copy import deepcopy
from pprint import pprint
from models.dismissal import DismissalInfoSchema
from models.run_details import RunDetailsSchema
from core_utils.dict_utils import map_keys
from core_utils import string_utils
from models.player_info import PlayerInfo

__author__ = 'arbaz'


def sample_ui_ball():
    return map_keys(lambda x: string_utils.to_snake_case(x),
                    {'ballNumber': 1,
                     'ballSequence': 0,
                     'inningId': '1',
                     'isPowerPlay': None,
                     'matchId': '1',
                     'overNumber': 0,
                     'playerStriker': {
                         'name': '1001',
                         'playerId': '1001',
                         'teamId': '1'
                     },
                     'playerNonStriker': {
                         'name': '1002',
                         'playerId': '1002',
                         'teamId': '1'
                     },
                     'playerCurrentBowler': {
                         'name': '2001',
                         'playerId': '2001',
                         'teamId': '2'
                     },
                     'runs': {'batrunsScored': 1,
                              'byesRuns': None,
                              'is4': None,
                              'is6': None,
                              'isNb': None,
                              'wideRuns': None},
                     'wicket': {'batsmanDismissed': None,
                                'dismissalType': None,
                                'fielder': None,
                                'strikeChanged': None}
                     })


def test_ball_details_load():
    sample_data = sample_ui_ball()
    schema = ball_details.BallDetailsUISchema()
    result = schema.load(sample_data)
    assert result.data.ball_number == sample_data["ball_number"]

    # Check custom mappings
    assert result.data.striker.get_user_id() == PlayerInfo.player_info_from_map(
        sample_data["player_striker"]).get_user_id()
    assert result.data.dismissal_info.batsman_dismissed == sample_data[
        "wicket"]["batsman_dismissed"]
    assert result.data.run_info.batruns_scored == sample_data["runs"]["batruns_scored"]
