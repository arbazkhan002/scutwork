import pytest
from pprint import pprint
import pdb
from core_utils.dict_utils import map_keys
from core_utils.string_utils import to_snake_case, to_camel_case
from test.core_utils import dummy_match_creator
from models.player_info import PlayerInfo
from models.fall_of_wicket import FallOfWicketUISchema, FallOfWicket
from models.store_manager import StoreManager

__author__ = 'arbaz'


def sample_fall_of_wicket():
    return {
        'batsman_id': '1001',
        'score': 10,
        'ball_number': 2,
        'over': 1,
        'timestamp': 1517529600
    }


def sample_ui_fall_of_wicket():
    return map_keys(to_snake_case, {
        'batsman': {
            "playerId": '1001',
            "name": '1001'
        },
        'score': 10,
        'ballNumber': 2,
        'over': 1,
        'timestamp': 1517529600
    })


def test_ui_load():
    value = sample_ui_fall_of_wicket()
    schema = FallOfWicketUISchema()
    result = schema.load(value)
    assert result.data.batsman_id == '1001'
    assert result.data.over == 1
    assert result.data.ball_number == 2


def test_ui_dump():
    player = PlayerInfo("1001", "1001", "1")
    StoreManager.put_player_info(player)
    fow = FallOfWicket.from_dict(sample_fall_of_wicket())
    schema = FallOfWicketUISchema()
    result = schema.dump(fow)
    assert result.data["batsman"]["name"] == "1001"
    assert result.data["over"] == 1
    assert result.data["timestamp"] == 1517529600


def test_to_dict():
    fow = FallOfWicket.from_dict(sample_fall_of_wicket())
    fow_dict = fow.to_dict()
    assert fow_dict["batsman_id"] == "1001"
