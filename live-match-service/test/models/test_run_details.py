import pytest
from models import ball_details
from copy import deepcopy
from models.run_details import RunDetailsSchema
from pprint import pprint

__author__ = 'arbaz'


def test_ui_output(ball_data):
    ball_event = ball_details.BallDetails.ball_from_map(deepcopy(ball_data))
    run_details = ball_event.run_info
    run_schema = RunDetailsSchema()
    result = run_schema.dump(run_details)
    # basic tests
    assert result.data['is_nb'] == False
    assert result.data["byes_runs"] is 0


def test_ui_input(ball_data):
    value = deepcopy(ball_data)
    schema = RunDetailsSchema()
    result = schema.load(value)
    pprint(result.data)
    # basic tests
    assert result.data.is_nb is False
    assert result.data.byes_runs is 0
