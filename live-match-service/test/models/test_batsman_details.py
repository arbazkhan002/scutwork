import pytest
from models import ball_details
from copy import deepcopy
from models.batsman_details import BatsmanDetailsUISchema
from pprint import pprint
from models.dismissal import DismissalInfoSchema
import pdb

__author__ = 'arbaz'


def test_ui_output(inning_maker):
    match_state_sample = inning_maker.get_match_state_sample()
    value = match_state_sample.player_striker.get_batting_details()
    schema = BatsmanDetailsUISchema()
    result = schema.dump(value)
    assert result.data['user_id'] == '1001'
    assert result.data['runs'] == 0
    assert result.data['balls_faced'] == 0
    assert result.data['fours'] == 0
    assert result.data['sixes'] == 0
    assert result.data['bat_dismissal'] is None
