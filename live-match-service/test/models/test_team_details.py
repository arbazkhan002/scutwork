import pytest
from models.team_details import TeamDetailsUISchema, TeamDetails
from pprint import pprint
import pdb
from core_utils.dict_utils import map_keys
from core_utils.string_utils import to_snake_case, to_camel_case
from test.core_utils import dummy_match_creator
from models.player_info import PlayerInfo

__author__ = 'arbaz'


def sample_team():
    return map_keys(to_snake_case, {
        'teamId': '1',
        'teamName': '1000',
        'roster': ['1001', '1002', '1003']
    })


def sample_ui_team():
    return map_keys(to_snake_case, {
        'teamId': '1',
        'roster': [
            {'name': '1001', 'playerId': '1001', 'teamId': '1'},
            {'name': '1002', 'playerId': '1002', 'teamId': '1'},
            {'name': '1003', 'playerId': '1003', 'teamId': '1'}],
        'teamName': '1000'
    })


def test_ui_output():
    value = sample_ui_team()
    schema = TeamDetailsUISchema()
    result = schema.load(value)
    assert result.data.team_id == '1'
    assert result.data.team_name == '1000'
    assert result.data.roster[0].get_user_id() == '1001'


def test_from_dict():
    dct = sample_team()
    dummy_match_creator.create_team(apiversion="1", team_id=dct['team_id'])
    team_details = TeamDetails.from_dict(dct)
    assert team_details.team_id == '1'
    assert team_details.team_name == '1000'
    assert len(team_details.roster) > 0
    assert isinstance(team_details.roster[0], PlayerInfo)
    assert team_details.roster[0].player_id is not None
