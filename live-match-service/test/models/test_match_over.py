import pytest
from models import store_manager
from models.match_over import MatchOver, MatchOverUISchema
import pdb

__author__ = 'arbaz'


@pytest.fixture
def over_dict():
    return {
        "over_number": 1,
        "runs": 2,
        "wickets": 1,
        "inning_score": 2,
        "inning_wickets": 1,
        "match_id": "1",
        "inning_id": "1",
        "ball_sequences": [0, 1, 2],
        "player_striker": "1001",
        "player_non_striker": "1002",
        "player_current_bowler": "2001"
    }


def test_from_dict(over_dict):
    ov = MatchOver.from_dict(over_dict)
    assert ov.runs == 2
    assert ov.inning_score == 2
    assert ov.ball_sequences == [0, 1, 2]


def test_to_dict(over_dict):
    ov = MatchOver.from_dict(over_dict)
    ov_dict = ov.to_dict()
    assert ov_dict["runs"] == 2
    assert ov_dict["match_id"] == "1"
    assert ov_dict["ball_sequences"] == [0, 1, 2]

"""
def test_add_ball(over_dict, inning_maker):
    ov = MatchOver.from_dict(over_dict)
    ball_sample = inning_maker.get_ball_sample()
    ov.add_ball(ball_sample)
    assert len(ov.ball_sequences) == 1


def test_undo_ball(over_dict, inning_maker):
    ov = MatchOver.from_dict(over_dict)
    ball_sample = inning_maker.get_ball_sample()
    ov.add_ball(ball_sample)
    ov.undo_ball(ball_sample)
    assert len(ov.ball_sequences) == 0
"""


def test_ui_dict(inning_maker, monkeypatch, over_dict):
    ball_sample = inning_maker.get_ball_sample()
    monkeypatch.setattr(store_manager.StoreManager,
                        "get_match_ball",
                        lambda *x: ball_sample)
    schema = MatchOverUISchema()
    ov = MatchOver.from_dict(over_dict)
    ov_dct = schema.dump(ov).data
    assert ov_dct["match_id"] == "1"
    assert len(ov_dct["balls"]) == 3
    assert "player_striker" in ov_dct["balls"][0]
    assert "runs" in ov_dct["balls"][0]
    # pdb.set_trace()
    assert "batting_team" in ov_dct
