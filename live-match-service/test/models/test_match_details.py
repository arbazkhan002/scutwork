import pytest
from controllers import admin_manager, match_manager
from test.core_utils import dummy_match_creator
from models import store_manager, match_details
from models.match_details import MatchDetailsUISchema
from pprint import pprint

__author__ = 'arbaz'


class TestUIOutput:

    @pytest.fixture(autouse=True)
    def setup_innings(self, request, inning_maker):
        store_manager.StoreManager.clear_redis_connection()
        yield

    def test_match_details_ui_output(self, inning_maker):
        NUMBER_DOT_BALLS = 4
        local_match_sample = inning_maker.get_inning_sample()
        match = store_manager.StoreManager.get_match_details(local_match_sample.match_id)
        schema = match_details.ScorecardUISchema()
        for i in range(1, NUMBER_DOT_BALLS + 1):
            ball_event = inning_maker.get_ball_sample()
            ball_event.ball_number = i
            ball_event.ball_sequence = i
            match_state_after_ball = match_manager.add_ball(ball_event)
        match = store_manager.StoreManager.get_match_details(local_match_sample.match_id)
        result = schema.dump(match).data

        assert result["match_id"] == "1"
        assert result["toss_won_by"] is not None
        assert result["team_batting_first"] is not None
        assert result["home_team"] is not None
        assert result["away_team"] is not None

        assert len(result["innings_details"]) == 1

        # Ensure that the innings has also been populated
        inning_detail = result["innings_details"][0]
        assert len(inning_detail["batsman_ids"]) == 2
        assert len(inning_detail["bowler_ids"]) == 1

        assert len(inning_detail["batsman_details"]) == 2
        assert len(inning_detail["bowler_details"]) == 1


@pytest.fixture
def sample_match():
    dummy_match_creator.create_team(apiversion="v1", team_id="1")
    dummy_match_creator.create_team(apiversion="v1", team_id="2")
    dummy_match_creator.create_team(apiversion="v1", team_id="3")
    dummy_match_creator.create_team(apiversion="v1", team_id="4")
    dummy_match_creator.create_match(apiversion="v1", match_id="1")
    return store_manager.StoreManager.get_match_details(match_id="1")


@pytest.fixture(autouse=True)
def clear_store():
    store_manager.StoreManager.clear_redis_connection()


@pytest.fixture
def sample_ui_match():
    return {'away_team': {'team_id': '2', 'team_name': '2000'},
            'home_team': {'team_id': '1', 'team_name': '1000'},
            'players_away_team': [{'name': '2001', 'player_id': '2001', 'team_id': '2'},
                                  {'name': '2002', 'player_id': '2002', 'team_id': '2'}],
            'home_team': {'team_id': '1', 'team_name': '1000'},
            'players_home_team': [{'name': '1001', 'player_id': '1001', 'team_id': '1'},
                                  {'name': '1002', 'player_id': '1002', 'team_id': '1'}],
            'inning_ids': [],
            'match_id': '1',
            'max_overs': 20,
            'team_batting_first': {'team_id': '1', 'team_name': '1000'},
            'toss_won_by': {'team_id': '2', 'team_name': '2000'}
            }


def test_start_innings(sample_match):
    # start the match so that players are assigned to teams randomly
    match = dummy_match_creator.start_match(apiversion="1")
    inning_detail_sample = match.start_innings(inning_id="1",
                                               batting_team="1",
                                               bowling_team="2")
    assert 0 < len(inning_detail_sample.players_batting_team) <= 11
    assert 0 < len(inning_detail_sample.players_bowling_team) <= 11

    match_state = inning_detail_sample.match_state
    assert match_state is None


def test_match_details_ui_output(sample_match):
    schema = MatchDetailsUISchema()
    match = sample_match
    match_details_as_dct = schema.dump(match).data
    assert match_details_as_dct["toss_won_by"] is None, ("unless toss details are added,"
                                                         "initial value should be None")
    match = dummy_match_creator.start_match(apiversion="1")
    match_details_as_dct = schema.dump(match).data
    assert match_details_as_dct["home_team"]["team_id"] == "1"
    assert match_details_as_dct["home_team"]["team_name"] == "1000"

    assert len(match_details_as_dct["players_home_team"]) > 0
    assert len(match_details_as_dct["players_away_team"]) > 0
    assert match_details_as_dct[
        "players_home_team"] != match_details_as_dct["players_away_team"]
    first_player = match_details_as_dct["players_away_team"][0]
    assert isinstance(first_player, dict), "pass players as objects instead of ids"
    assert "name" in first_player and "player_id" in first_player


def test_match_details_ui_input(sample_match, sample_ui_match):
    analogous_match_object = sample_match
    assert analogous_match_object.match_id == "1", ("match-id=1 should be "
                                                    "created and stored before testing")
    schema = MatchDetailsUISchema()
    value = sample_ui_match
    match_details = schema.load(value).data
    assert match_details.match_id == value["match_id"]
    assert value["home_team"]["team_id"] in match_details.players
    assert value["away_team"]["team_id"] in match_details.players
    assert (sorted(match_details.players[value["away_team"]["team_id"]]) ==
            sorted(list(map(lambda x: x["player_id"], value["players_away_team"]))))
    assert match_details.toss_won_by == value["toss_won_by"]["team_id"]
    assert match_details.team_batting_first == value["team_batting_first"]["team_id"]
