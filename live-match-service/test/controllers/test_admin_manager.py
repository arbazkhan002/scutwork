import pytest
from controllers import admin_manager
import pdb
import datetime
from models.store_manager import StoreManager, RedisKV, SlowFastKV
__author__ = 'arbaz'

mock_data = {"matches": [
    {"matchId": 17795,
     "matchDate": "6/2/2018",
     "matchTime": "08:30AM",
     "homeTeamId": 132,
     "homeTeamName": "Synergy",
     "awayTeamId": 234,
     "awayTeamName": "Fighting Broncos",
     "gameGroup": "Platinum",
     "groundName": "Capitol",
     "groundType": "BB",
     "city": "San Jose",
     "umpire1TeamId": 111,
     "umpire1TeamName": "Terminators",
     "umpire1TeamGroup": "Gold",
     "umpire2TeamId": 111,
     "umpire2TeamName": "Terminators",
     "umpire2TeamGroup": "Gold"
     },
    {"matchId": 17792,
     "matchDate": "6/9/2018",
     "matchTime": "08:30AM",
     "homeTeamId": 153,
     "homeTeamName": "United",
     "awayTeamId": 133,
     "awayTeamName": "Daredevils",
     "gameGroup": "Platinum",
     "groundName": "Great Oaks - Softball",
     "groundType": "BB",
     "city": "San Jose",
     "umpire1TeamId": 156,
     "umpire1TeamName": "Panthers",
     "umpire1TeamGroup": "Gold",
     "umpire2TeamId": 156,
     "umpire2TeamName": "Panthers",
     "umpire2TeamGroup": "Gold"
     }
]}


mock_match_players_data = {
    "Player List": [{
        "teamId": 40,
        "teamName": "Aryans CC",
        "playerId": 13415,
        "playerName": "Amith Kumar Raul"
    }, {
        "teamId": 68,
        "teamName": "Challengers CC",
        "playerId": 2078,
        "playerName": "Vishnu Selvaraj"
    }, {
        "teamId": 72,
        "teamName": "Warriors CC",
        "playerId": 11746,
        "playerName": "Akram Ali"
    }]
}


def test_get_all_teams(inning_maker):
    local_match_sample = inning_maker.get_inning_sample()
    all_teams = admin_manager.get_all_teams()
    assert len(all_teams) == 4, "4 teams expected - home, away, umpire_1, umpire_2"
    all_teams = sorted(all_teams, key=lambda x: x.team_id)
    assert all_teams[0].team_id == "1"
    assert all_teams[0].team_name == "1000"
    assert all_teams[0].roster is not None


def test_get_all_teams_redis(monkeypatch, mock_redis, inning_maker):
    monkeypatch.setattr("redis.StrictRedis", mock_redis)
    kv = RedisKV(host="localhost", port=6379)
    StoreManager.set_redis_connection(kv)
    local_match_sample = inning_maker.get_inning_sample()

    all_teams = admin_manager.get_all_teams()
    assert len(all_teams) == 4, "4 teams expected - home, away, umpire_1, umpire_2"


def test_pull_matches(monkeypatch, mock_redis):
    def mocked_get(url, params):
        return mock_data

    class Fakedatetime:

        @classmethod
        def now(cls):
            return datetime(2018, 6, 6)

    monkeypatch.setattr("controllers.api_manager.ApiManager.get", mocked_get)
    monkeypatch.setattr(datetime, "datetime", Fakedatetime)
    monkeypatch.setattr("redis.StrictRedis", mock_redis)
    kv = RedisKV(host="localhost", port=6379)
    StoreManager.set_redis_connection(kv)
    admin_manager.pull_matches(remote_auth_token=None)
    all_matches = admin_manager.get_all_matches(move_old_to_archive=False)
    # 2 matches come from mock_data,
    # rest are in the session such as match-id=1
    assert len(all_matches) >= 2


def test_pull_match_players(monkeypatch):
    def mocked_get(url, params):
        return mock_match_players_data
    monkeypatch.setattr(
        "controllers.api_manager.ApiManager.get", mocked_get)
    admin_manager.pull_match_players(match_id="1", remote_auth_token=None)
    player = StoreManager.get_player_info(2078)
    assert player.team_id == 68
    assert player.name == "Vishnu Selvaraj"


def test_pull_teams(monkeypatch):
    def mocked_get(url, params):
        return mock_data
    monkeypatch.setattr(
        "controllers.api_manager.ApiManager.get", mocked_get)
    admin_manager.pull_teams(remote_auth_token=None)
    teams = StoreManager.get_all_teams()
    assert len(teams) == 6
    assert set(["Daredevils", "Synergy", "United", "Fighting Broncos"]) < \
        set(t.team_name for t in teams)
