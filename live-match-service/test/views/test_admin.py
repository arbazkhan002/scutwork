
from views import admin as admin_m
from views.admin import admin, add_venue_using
import json
import pdb
from models import store_manager
import pytest
from controllers.match_manager import get_match
from controllers import match_manager
from config import Config
from io import StringIO, BytesIO
import csv
from controllers.admin_manager import get_all_venues

__author__ = 'arbaz'


class TestAdmin:

    @pytest.fixture(autouse=True)
    def setup(self, app, request, monkeypatch):
        app.register_blueprint(admin)
        store_manager.StoreManager.clear_redis_connection()
        monkeypatch.setattr(
            "views.admin.read_admin_token",
            lambda x: True)
        yield

    def test_matches(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        response = client.get('/api/v1/match')
        data = json.loads(response.data.decode())
        assert len(data["matches"]) == 1
        match = data["matches"][0]
        assert match["homeTeam"]["teamId"] == "1"
        assert match["awayTeam"]["teamId"] == "2"
        assert match["maxOvers"] == 20

    def test_create_match(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        response = client.post('/api/v1/match',
                               data=json.dumps({
                                   "matchId": "2",
                                   "team1Id": "1",
                                   "team2Id": "2",
                                   "umpiringTeam1Id": "3",
                                   "umpiringTeam2Id": "4",
                                   "venue": "TestPark",
                                   "maxOvers": "20",
                                   "matchPin": "1000"
                               }))

        data = json.loads(response.data.decode())
        assert response.status_code == 200

        match = get_match("2")
        assert match.home_team == "1"
        assert match.away_team == "2"
        assert match.max_overs == 20
        assert data["homeTeam"]["teamId"] == "1"
        assert data["awayTeam"]["teamId"] == "2"

    def test_update_match(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        response = client.put('/api/v1/match/1',
                              data=json.dumps({
                                  "matchId": "1",
                                  "matchPin": "4000"
                              }))
        data = json.loads(response.data.decode())
        assert response.status_code == 200

        match = get_match("1")
        assert match_manager.is_valid_credential("4000", match.match_pin)

    def test_delete_match(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        response = client.delete('/api/v1/match/1')
        data = json.loads(response.data.decode())
        assert response.status_code == 200

        match = get_match("1")
        assert match is None

    def test_get_after_delete(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        response = client.delete('/api/v1/match/1')
        response = client.get('/api/v1/match')
        data = json.loads(response.data.decode())
        assert len(data["matches"]) == 0

    def test_teams(self, client, inning_maker):
        """Client comes from pytest-flask,
        inning_maker comes from conftest"""
        local_match_sample = inning_maker.get_inning_sample()
        response = client.get('/api/v1/team')
        data = json.loads(response.data.decode())
        assert len(
            data["teams"]) == 4, "4 teams expected - home, away, umpire_1, umpire_2"
        assert set(i["teamId"]
                   for i in data["teams"]) == set(["1", "2", "3", "4"])
        assert set(i["teamName"]
                   for i in data["teams"]) == set(["1000", "2000", "3000", "4000"])

    def test_update_team(self, client, inning_maker):
        """Client comes from pytest-flask,
        inning_maker comes from conftest"""
        local_match_sample = inning_maker.get_inning_sample()
        response = client.put('/api/v1/team/1',
                              data=json.dumps({
                                  "teamId": "1",
                                  "teamName": "10000"
                              }))
        data = json.loads(response.data.decode())
        assert response.status_code == 200

        team = store_manager.StoreManager.get_team_details("1")
        assert team.team_name == "10000"

    def test_create_team(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        response = client.post('/api/v1/team',
                               data=json.dumps({
                                   "teamId": "5",
                                   "teamName": "5000"
                               }))
        data = json.loads(response.data.decode())
        assert response.status_code == 200

        team = store_manager.StoreManager.get_team_details("5")
        assert team.team_name == "5000"

    def test_file_upload_team(self, client):
        data_temp = StringIO()
        fieldnames = ["team_id", "team_name"]
        writer = csv.DictWriter(data_temp, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({'team_name': '3000', 'team_id': '3'})
        bytes_temp = BytesIO(data_temp.getvalue().encode("utf-8"))
        response = client.post('/api/v1/upload/team',
                               data={
                                   'file': (bytes_temp, 'hello.csv'),
                               })
        assert response.status_code == 200

        team = store_manager.StoreManager.get_team_details("3")
        assert team.team_name == "3000"

    def test_file_upload_player(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        data_temp = StringIO()
        fieldnames = ["player_id", "name", "team_id"]
        writer = csv.DictWriter(data_temp, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({'player_id': '2022', 'team_id': '2', 'name': '2022'})
        bytes_temp = BytesIO(data_temp.getvalue().encode("utf-8"))
        response = client.post('/api/v1/upload/players',
                               data={
                                   'file': (bytes_temp, 'hello.csv'),
                               })
        assert response.status_code == 200

        player = store_manager.StoreManager.get_player_info("2022")
        assert player.team_id == "2"
        assert player.name == "2022"

    def test_get_team(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        response = client.get('/api/v1/team/1')
        data = json.loads(response.data.decode())
        assert response.status_code == 200
        assert data["teamId"] == "1"
        assert data["teamName"] == "1000"
        assert len(data["roster"]) > 10
        assert sorted(data["roster"], key=lambda x: x["playerId"])[
            0]["playerId"] == "1001"

    def test_delete_team(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        response = client.delete('/api/v1/team/1')
        data = json.loads(response.data.decode())
        assert response.status_code == 200

        team = store_manager.StoreManager.get_team_details("1")
        assert team is None

    def test_player_info(self, client, inning_maker):
        """Client comes from pytest-flask,
        inning_maker comes from conftest"""
        local_match_sample = inning_maker.get_inning_sample()
        response = client.get('/api/v1/player/1001')
        data = json.loads(response.data.decode())
        assert response.status_code == 200
        assert data["name"] == "1001"
        assert data["teamId"] == "1"

    def test_create_player(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        response = client.post('/api/v1/player',
                               data=json.dumps({
                                   "teamId": "2",
                                   "playerId": "2100",
                                   "name": "21000"
                               }))
        data = json.loads(response.data.decode())
        assert response.status_code == 200

        player = store_manager.StoreManager.get_player_info("2100")
        assert player.team_id == "2"
        assert player.name == "21000"

        team = store_manager.StoreManager.get_team_details(player.team_id)
        assert player.player_id in list(map(lambda x: x.player_id, team.roster))

    def test_delete_player(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        response = client.delete('/api/v1/player/1001')
        data = json.loads(response.data.decode())
        assert response.status_code == 200

        player = store_manager.StoreManager.get_player_info("1001")
        assert player is None

        team = store_manager.StoreManager.get_team_details("1")
        assert "1001" not in list(map(lambda x: x.player_id, team.roster))

    def test_create_venue(self, client, inning_maker):
        local_match_sample = inning_maker.get_inning_sample()
        response = client.post('/api/v1/venue',
                               data=json.dumps({
                                   "venue_id": "1",
                                   "name": "11",
                                   "city": "12",
                                   "type": "13"
                               }))
        data = json.loads(response.data.decode())
        assert response.status_code == 200

        venue = store_manager.StoreManager.get_venue_details("1")
        assert venue.name == "11"

    def test_update_venue(self, client, inning_maker):
        """Client comes from pytest-flask,
        inning_maker comes from conftest"""
        add_venue_using({
            "venue_id": "1",
            "name": "11",
            "city": "12",
            "type": "13"
        })
        response = client.put('/api/v1/venue/1',
                              data=json.dumps({
                                  "venue_id": "1",
                                  "name": "11",
                                  "city": "120",
                                  "type": "13"
                              }))
        data = json.loads(response.data.decode())
        assert response.status_code == 200

        venue = store_manager.StoreManager.get_venue_details("1")
        assert venue.city == "120"

    def test_delete_venue(self, client):
        add_venue_using({
            "venue_id": "1",
            "name": "11",
            "city": "12",
            "type": "13"
        })
        venues = get_all_venues()
        response = client.delete('/api/v1/venue/1')
        data = json.loads(response.data.decode())
        assert response.status_code == 200

        venue = store_manager.StoreManager.get_venue_details("1")
        assert venue is None

    def test_venues(self, client):
        """Client comes from pytest-flask,
        inning_maker comes from conftest"""
        add_venue_using({
            "venue_id": "1",
            "name": "11",
            "city": "12",
            "type": "13"
        })
        add_venue_using({
            "name": "21",
            "city": "22",
            "type": "23"
        })
        response = client.get('/api/v1/venue')
        data = json.loads(response.data.decode())
        assert len(data["venues"]) == 2, "2 venues expected"
        assert set(i["venueId"]
                   for i in data["venues"]) > set(["1"])
        assert set(i["name"]
                   for i in data["venues"]) == set(["11", "21"])
