import pytest
from test.core_utils import dummy_match_creator
from models.match_state import MatchState
from models.ball_details import BallDetails
from pprint import pprint
import mockredis

from flask_cors import CORS
from flask import Flask

__author__ = 'arbaz'


class InningMaker(object):

    def get_inning_sample(self, max_overs=20):
        return dummy_match_creator.setup_match("v1", max_overs)

    def get_match_state_sample(self):
        inning_object = self.get_inning_sample()
        ball_object = self.get_ball_sample()
        return MatchState.from_inning_and_ball(inning_object,
                                               ball_object)

    def get_ball_sample(self):
        return BallDetails.ball_from_map(ball_data())

    def get_second_inning_ball_sample(self):
        return BallDetails.ball_from_map(second_inning_ball_data())


@pytest.fixture(autouse=True)
def ball_data():
    return {
        "ball_number": 1,
        "ball_sequence": 1,
        "bowler": {
            'name': '2001',
            'player_id': '2001',
            'team_id': '2'
        },
        "dismissal_info": {
            "batsman_dismissed": None,
            "dismissal_type": None,
            "fielder": None,
            "strike_changed": False,
            "bowler": None
        },
        "inning_id": "1",
        "is_batting_powerplay": False,
        "is_power_play": None,
        "match_id": "1",
        "non_striker": {
            'name': '1002',
            'player_id': '1002',
            'team_id': '1'
        },
        "over_number": 0,
        "run_info": {
            "batruns_scored": 0,
            "byes_runs": 0,
            "is4": None,
            "is6": None,
            "is_nb": False,
            "wide_runs": 0
        },
        "striker": {
            'name': '1001',
            'player_id': '1001',
            'team_id': '1'
        }
    }


@pytest.fixture(autouse=True)
def second_inning_ball_data():
    return {
        "ball_number": 1,
        "ball_sequence": 1,
        "bowler": {
            'name': '1001',
            'player_id': '1001',
            'team_id': '1'
        },
        "dismissal_info": {
            "batsman_dismissed": None,
            "dismissal_type": None,
            "fielder": None,
            "strike_changed": False,
            "bowler": None
        },
        "inning_id": "2",
        "is_batting_powerplay": False,
        "is_power_play": None,
        "match_id": "1",
        "non_striker": {
            'name': '2002',
            'player_id': '2002',
            'team_id': '1'
        },
        "over_number": 0,
        "run_info": {
            "batruns_scored": 0,
            "byes_runs": 0,
            "is4": None,
            "is6": None,
            "is_nb": False,
            "wide_runs": 0
        },
        "striker": {
            'name': '2001',
            'player_id': '2001',
            'team_id': '2'
        }
    }


@pytest.fixture
def inning_maker():
    return InningMaker()


@pytest.fixture
def mock_redis():
    def make_mock_redis(*args, **kwargs):
        redis = mockredis.MockRedis(*args, **kwargs)
        return redis
    return make_mock_redis


@pytest.fixture
def app():
    app = Flask(__name__)
    # enable CORS
    CORS(app)
    app.config.from_object('config.DevConfig')
    return app
