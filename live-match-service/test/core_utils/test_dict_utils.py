import pytest
from core_utils import dict_utils

__author__ = 'arbaz'


def test_map_keys():
    item_dict = {
        'keyx': {
            'keyxx': 1
        }
    }
    mapped_dict = dict_utils.map_keys(lambda x: x.upper(), item_dict)
    assert mapped_dict == {
        'KEYX': {
            'KEYXX': 1
        }
    }

    item_dict = {
        'keyx': [{
            'keyz': 1
        },
            {
            'keyy': 2
        }
        ]
    }
    mapped_dict = dict_utils.map_keys(lambda x: x.upper(), item_dict)
    assert mapped_dict == {
        'KEYX': [{
            'KEYZ': 1
        },
            {
            'KEYY': 2
        }
        ]
    }


def test_map_keys_complex():
    item_dict = {
        "bowling_team": "2",
        "batsmen": [
            "1001"
        ],
        "batting_team": "1",
        "bowlers": [],
        "inning_id": "1",
        "match_id": "1",
        "player_current_bowler": None,
        "player_non_striker": None,
        "player_previous_bowler": None,
        "player_striker": "1001",
        "score": 0,
        "wickets": 0
    }

    mapped_dict = dict_utils.map_keys(lambda x: x.upper(), item_dict)
