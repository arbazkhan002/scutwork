import logging

from logging import StreamHandler
import os
import sys
import json
from collections import Counter

from controllers import match_manager, admin_manager

from models.store_manager import StoreManager
from models import ball_details, player_info, match_details, store_manager,\
    match_state
import pdb
from models.team_details import TeamDetails
from copy import deepcopy

__author__ = 'arbaz'


def setup_match(apiversion, max_overs=20):
    create_team(apiversion="v1", team_id="1")
    create_team(apiversion="v1", team_id="2")
    create_team(apiversion="v1", team_id="3")
    create_team(apiversion="v1", team_id="4")
    create_match(apiversion, max_overs=max_overs)
    start_match(apiversion)
    inning = start_innings(apiversion)
    match = StoreManager.get_match_details("1")
    return inning


def create_match(apiversion, match_id="1", team1_id="1",
                 team2_id="2", match_pin="4040",
                 umpire_team1_id="3", umpire_team2_id="4",
                 venue="TestPark", max_overs=20):
    admin_manager.create_match(match_id, team1_id, team2_id,
                               umpiring_team_1=umpire_team1_id,
                               umpiring_team_2=umpire_team2_id,
                               match_pin=match_pin, venue=venue,
                               max_overs=max_overs)


def start_match(apiversion, match_id="1", team1_id="1", team2_id="2"):
    team1_players = list(get_players_for_team(team1_id))
    team2_players = list(get_players_for_team(team2_id))
    #-----#
    match = StoreManager.get_match_details(match_id)
    team1_id = match.home_team
    team2_id = match.away_team
    match = match_manager.add_players_to_team(match_id, team1_id, players=team1_players)
    match = match_manager.add_players_to_team(match_id, team2_id, players=team2_players)
    match.add_toss_details(team1_id, team1_id)
    match.captain_1 = "1001"
    match.captain_2 = "2001"
    match.umpire_1 = "3001"
    match.umpire_2 = "4001"
    return match


def create_team(apiversion, team_id):
    create_players_for_team(team_id)


def start_innings(apiversion, match_id="1", inning_id="1",
                  batting_team="1", bowling_team="2"):
    inning = match_manager.start_innings(match_id, inning_id, batting_team, bowling_team)
    return inning


def create_players_for_team(team_id):
    team = TeamDetails(team_id, team_name=str(int(team_id) * 1000))
    for player in range(int(team_id) * 1000 + 1, int(team_id) * 1000 + 12):
        player_info_dict = player_info.PlayerInfo(player_id=str(player),
                                                  name=str(player),
                                                  team_id=team_id)
        store_manager.StoreManager.put_player_info(player_info_dict)
        team.add_player(player_info_dict)
    return team


def get_players_for_team(team_id):
    for player in range(int(team_id) * 1000 + 1, int(team_id) * 1000 + 12):
        yield str(player)


def play_balls(number_balls, init_ball_event, init_inning_obj=None,
               runs_sequence_sparse=None, init_match_state=None):
    if runs_sequence_sparse is None:
        runs_sequence_sparse = {3: 1, 5: 2}

    match_state_after_ball = init_match_state or match_state.MatchState.from_inning_and_ball(
        init_inning_obj, init_ball_event)
    for i in range(number_balls):
        ball_event = deepcopy(init_ball_event)
        ball_event.striker = match_state_after_ball.player_striker.get_player_info()
        ball_event.non_striker = match_state_after_ball.player_non_striker.get_player_info()
        ball_event.ball_number = match_state_after_ball.ball_number
        ball_event.ball_sequence = match_state_after_ball.ball_sequence + 1
        ball_event.run_info.batruns_scored = runs_sequence_sparse.get(
            ball_event.ball_sequence, 0)
        match_state_after_ball = match_manager.add_ball(ball_event)
    return match_state_after_ball
