from flask import Blueprint, jsonify, request, current_app
from flask_cors import CORS
from models.team_details import TeamDetailsUISchema
from controllers.admin_manager import get_team, get_player, make_hash_for,\
    get_venue
from werkzeug.exceptions import BadRequest, NotFound, Unauthorized
from core_utils.views_utils import OkResponse, to_ui_format, to_object_format,\
    OkLoginResponse
from controllers import admin_manager, match_manager
from models import store_manager
import logging
from models.player_info import PlayerInfoUISchema
from models.player import Player
import pdb
from controllers.match_manager import get_match, is_valid_credential
from models.match_details import MatchDetailsUISchema
from config import Config
from functools import wraps
import sys
import traceback
import json
import csv
from io import StringIO, BytesIO
from models.venue import VenueUISchema, Venue

__author__ = 'arbaz'

admin = Blueprint('admin', __name__)
CORS(admin)


ALLOWED_EXTENSIONS = set(['csv'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def read_admin_token(request):
    auth_header = request.headers.get('Authorization')
    if auth_header:
        auth_token = auth_header.split(' ')[1]
        admin_user = match_manager.decode_auth_token(auth_token)
        if admin_user != Config.config.ADMIN_USER:
            return Unauthorized(admin_user)
        return True
    return BadRequest("No authorization header in request")


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        is_admin_user = read_admin_token(request)
        if is_admin_user == True:
            return f(*args, **kwargs)
        else:
            return is_admin_user
    return decorated_function


@admin.route('/api/<string:apiversion>/ping', methods=['GET'])
def ping(apiversion):
    return OkResponse()


@admin.route('/api/<string:apiversion>/login', methods=['POST'])
def login_scorer(apiversion):
    if request.method == "POST":
        login_data = to_object_format(request.get_json(force=True))
        if not login_data:
            return BadRequest("Login data is not provided")

        admin_hash = Config.config.ADMIN_HASH
        password = login_data.get("password")
        if not is_valid_credential(password, admin_hash):
            return Unauthorized()

        auth_token = match_manager.encode_auth_token(
            Config.config.ADMIN_USER)
        return OkLoginResponse(auth_token.decode())


@admin.route("/api/<string:apiversion>/match", methods=["POST"])
@admin_required
def create_match(apiversion):
    match_data = to_object_format(request.get_json(force=True))
    if "match_id" not in match_data:
        return BadRequest("No matchId field in POST data")

    match_exists = get_match(match_data["match_id"])
    if match_exists is not None:
        return BadRequest("Match already exists")

    current_app.logger.debug("match_data:{}".format(match_data))
    try:
        match = admin_manager.create_match(match_data["match_id"],
                                           match_data["team1_id"],
                                           match_data["team2_id"],
                                           match_data["match_pin"],
                                           match_data["umpiring_team1_id"],
                                           match_data["umpiring_team2_id"],
                                           match_data["venue"],
                                           int(match_data["max_overs"]))
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        msg = traceback.format_exception(exc_type, exc_value, exc_traceback)
        msg_json = {"Schema Load Error": msg}
        current_app.logger.error(msg_json)
        return BadRequest("Invalid POST data")

    current_app.logger.debug("match away id:{}".format(match.away_team))
    td = store_manager.StoreManager.get_team_details(match.away_team)
    current_app.logger.debug("match away td:{}".format(td.to_json()))
    schema = MatchDetailsUISchema(
        exclude=("short_inning_details", "inning_ids"))
    match_dct = schema.dump(match).data
    match_ui_dct = to_ui_format(match_dct)
    return jsonify(match_ui_dct)


# endpoint to show all matches
@admin.route("/api/<string:apiversion>/match", methods=["GET"])
@admin_required
def get_matches(apiversion):
    matches = admin_manager.get_all_matches()
    schema = MatchDetailsUISchema(
        exclude=("short_inning_details", "inning_ids"), many=True)
    matches_dct = {"matches": schema.dump(matches).data}
    matches_ui_dct = to_ui_format(matches_dct)
    return jsonify(matches_ui_dct)


# endpoint to get match details by id
@admin.route("/api/<string:apiversion>/match/<string:match_id>", methods=["GET"])
@admin_required
def match_detail(apiversion, match_id):
    match_data = admin_manager.get_match(match_id)
    schema = MatchDetailsUISchema(exclude=("short_inning_details", "inning_ids"))
    match = to_ui_format(schema.dump(match_data).data) if match_data else None
    return jsonify(match) if match else NotFound()


# endpoint to update user
@admin.route("/api/<string:apiversion>/match/<string:id>", methods=["PUT"])
@admin_required
def match_update(apiversion, id):
    """Used only for changing match pin
    To change anything else. delete the match"""
    match_data = to_object_format(request.get_json(force=True))
    if "match_id" not in match_data:
        return BadRequest("No matchId field in POST data")

    if match_data["match_id"] != id:
        return BadRequest("Id mismatch id: {}, data:{}".format(id,
                                                               match_data["match_id"]))

    match_exists = get_match(match_data["match_id"])
    if match_exists is None:
        return NotFound("No match found with id:{}".format(id))

    try:
        pin_hash = make_hash_for(match_data["match_pin"])
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        msg = traceback.format_exception(exc_type, exc_value, exc_traceback)
        msg_json = {"Schema Load Error": msg}
        current_app.logger.error(msg_json)
        return BadRequest("Invalid POST data")

    match_exists.match_pin = pin_hash
    match_exists.persist()
    return OkResponse()


# endpoint to delete user
@admin.route("/api/<string:apiversion>/match/<string:id>", methods=["DELETE"])
@admin_required
def match_delete(apiversion, id):
    match_exists = get_match(id)
    if match_exists is None:
        return NotFound("No match found with id:{}".format(id))
    store_manager.StoreManager.delete_match(id)
    return OkResponse()


@admin.route("/api/<string:apiversion>/team", methods=["POST"])
@admin_required
def add_team(apiversion):
    team_data = to_object_format(request.get_json(force=True))
    return add_team_using(team_data)


def add_team_using(team_data):
    if "team_id" not in team_data:
        return BadRequest("No teamId field in POST data")

    team_exists = get_team(team_data["team_id"])
    if team_exists is not None:
        return BadRequest("Team already exists")

    schema = TeamDetailsUISchema()
    try:
        team = schema.load(team_data).data
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        msg = traceback.format_exception(exc_type, exc_value, exc_traceback)
        msg_json = {"Schema Load Error": msg}
        current_app.logger.error(msg_json)
        return BadRequest("Invalid POST data")

    team.persist()
    return OkResponse()


# endpoint to show all team
@admin.route("/api/<string:apiversion>/team", methods=["GET"])
@admin_required
def get_teams(apiversion):
    teams = admin_manager.get_all_teams()
    schema = TeamDetailsUISchema(only=("team_name", "team_id"), many=True)
    teams_dct = {"teams": schema.dump(teams).data}
    teams_ui_dct = to_ui_format(teams_dct)
    return jsonify(teams_ui_dct)


# endpoint to get team detail by id
@admin.route("/api/<string:apiversion>/team/<string:team_id>", methods=["GET"])
@admin_required
def team_detail(apiversion, team_id):
    current_app.logger.debug("Get-team:{}".format(team_id))
    team_data = admin_manager.get_team(team_id)
    schema = TeamDetailsUISchema()
    team = to_ui_format(schema.dump(team_data).data) if team_data else None
    current_app.logger.debug("Sent-team:{}".format(team_id))
    return jsonify(team) if team else NotFound()


# endpoint to update user
@admin.route("/api/<string:apiversion>/team/<string:id>", methods=["PUT"])
@admin_required
def team_update(apiversion, id):
    team_data = to_object_format(request.get_json(force=True))
    if "team_id" not in team_data:
        return BadRequest("No teamId field in POST data")

    team_exists = get_team(team_data["team_id"])
    if team_exists is None:
        return NotFound("No team found with id:{}".format(id))

    schema = TeamDetailsUISchema()
    try:
        team = schema.load(team_data).data
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        msg = traceback.format_exception(exc_type, exc_value, exc_traceback)
        msg_json = {"Schema Load Error": msg}
        current_app.logger.error(msg_json)
        return BadRequest("Invalid POST data")

    team.persist()
    return OkResponse()


# endpoint to delete user
@admin.route("/api/<string:apiversion>/team/<string:id>", methods=["DELETE"])
@admin_required
def team_delete(apiversion, id):
    team_exists = get_team(id)
    if team_exists is None:
        return NotFound("No team found with id:{}".format(id))
    store_manager.StoreManager.delete_team(id)
    return OkResponse()


@admin.route("/api/<string:apiversion>/player", methods=["POST"])
@admin_required
def add_player(apiversion):
    player_data = to_object_format(request.get_json(force=True))
    return add_player_using(player_data)


def add_player_using(player_data):
    if "player_id" not in player_data:
        return BadRequest("No playerId field in POST data")

    player_exists = get_player(player_data["player_id"])
    if player_exists is not None:
        return BadRequest("Player already exists")

    schema = PlayerInfoUISchema()
    try:
        player_info = schema.load(player_data).data

    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        msg = traceback.format_exception(exc_type, exc_value, exc_traceback)
        msg_json = {"Schema Load Error": msg}
        current_app.logger.error(msg_json)
        return BadRequest("Invalid POST data")

    admin_manager.create_player(player_info)
    return OkResponse()


# endpoint to show all team
@admin.route("/api/<string:apiversion>/player/<string:player_id>", methods=["GET"])
@admin_required
def get_player_info(apiversion, player_id):
    player_data = admin_manager.get_player(player_id)
    schema = PlayerInfoUISchema()
    player = to_ui_format(schema.dump(player_data).data) if player_data else None
    return jsonify(player) if player else NotFound()


# endpoint to update user
@admin.route("/api/<string:apiversion>/player/<string:player_id>", methods=["PUT"])
@admin_required
def player_update(apiversion, player_id):
    player_data = to_object_format(request.get_json(force=True))
    if "team_id" not in player_data:
        return BadRequest("No playerId field in PUT data")

    player_exists = get_player(player_data["player_id"])
    if player_exists is None:
        return NotFound("No player found with id:{}".format(player_id))

    schema = PlayerInfoUISchema()
    try:
        player_info = schema.load(player_data).data
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        msg = traceback.format_exception(exc_type, exc_value, exc_traceback)
        msg_json = {"Schema Load Error": msg}
        current_app.logger.error(msg_json)
        return BadRequest("Invalid POST data")

    player = Player(player_info)
    player.persist_info()
    team = store_manager.StoreManager.get_team_details(player_info.team_id)
    if team is not None:
        team.add_player(player_info)
    return OkResponse()


# endpoint to delete user
@admin.route("/api/<string:apiversion>/player/<string:player_id>", methods=["DELETE"])
@admin_required
def player_delete(apiversion, player_id):
    player_info_exists = get_player(player_id)
    if player_info_exists is None:
        return NotFound("No player found with id:{}".format(player_id))
    team = store_manager.StoreManager.get_team_details(player_info_exists.team_id)
    if team is not None:
        team.remove_player(player_info_exists)
    store_manager.StoreManager.delete_player_info(player_id)

    return OkResponse()


@admin.route("/api/<string:apiversion>/upload/team", methods=["POST"])
@admin_required
def upload_teams(apiversion):
    error = "none"
    return process_upload(add_team_using)


@admin.route("/api/<string:apiversion>/upload/players", methods=["POST"])
@admin_required
def upload_players(apiversion):
    error = "none"
    return process_upload(add_player_using)


@admin.route("/api/<string:apiversion>/venue", methods=["POST"])
@admin_required
def add_venue(apiversion):
    venue_data = to_object_format(request.get_json(force=True))
    return add_venue_using(venue_data)


def add_venue_using(venue_data):
    if "name" not in venue_data and "venue_id" not in venue_data:
        return BadRequest("No id or name field in POST data")

    id = venue_data.get('venue_id') or Venue.make_id(venue_data['name'])
    venue_exists = get_venue(id)
    if venue_exists is not None:
        return BadRequest("Venue already exists")

    schema = VenueUISchema()
    try:
        venue = schema.load(venue_data).data

    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        msg = traceback.format_exception(exc_type, exc_value, exc_traceback)
        msg_json = {"Schema Load Error": msg}
        current_app.logger.error(msg_json)
        return BadRequest("Invalid POST data")

    venue.persist()
    return OkResponse()


# endpoint to update user
@admin.route("/api/<string:apiversion>/venue/<string:id>", methods=["PUT"])
@admin_required
def venue_update(apiversion, id):
    venue_data = to_object_format(request.get_json(force=True))
    if "name" not in venue_data:
        return BadRequest("No name field in POST data")

    venue_exists = get_venue(id)
    if venue_exists is None:
        return NotFound("No venue found with id:{}".format(id))

    schema = VenueUISchema()
    try:
        venue = schema.load(venue_data).data

    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        msg = traceback.format_exception(exc_type, exc_value, exc_traceback)
        msg_json = {"Schema Load Error": msg}
        current_app.logger.error(msg_json)
        return BadRequest("Invalid POST data")

    venue.persist()
    return OkResponse()

# endpoint to delete user


@admin.route("/api/<string:apiversion>/venue/<string:id>", methods=["DELETE"])
@admin_required
def venue_delete(apiversion, id):
    venue_exists = get_venue(id)
    if venue_exists is None:
        return NotFound("No venue found with id:{}".format(id))
    store_manager.StoreManager.delete_venue(id)
    return OkResponse()

# endpoint to show all team


@admin.route("/api/<string:apiversion>/venue", methods=["GET"])
@admin_required
def get_venues(apiversion):
    venues = admin_manager.get_all_venues()
    schema = VenueUISchema(many=True)
    venues_dct = {"venues": schema.dump(venues).data}
    venues_ui_dct = to_ui_format(venues_dct)
    return jsonify(venues_ui_dct)


def process_upload(row_processing_function):
    if request.method == 'POST':
        f = request.files['file']
        if not f:
            return BadRequest("Empty file uploaded!")
        elif not allowed_file(f.filename):
            return BadRequest("Unsupported file extension in {}".format(f.filename))
        if f and allowed_file(f.filename):
            # Read input file
            current_app.logger.debug("In F")
            temp = BytesIO()
            f.save(dst=temp)
            tempstr = StringIO(temp.getvalue().decode("utf-8"))
            # pdb.set_trace()
            reader = csv.DictReader(tempstr)
            for row in reader:
                row_processing_function(row)
            return OkResponse()
        return BadRequest("What!")
