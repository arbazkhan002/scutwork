import logging
import os
import sys
import json
import traceback

from collections import Counter
from functools import wraps
from logging import StreamHandler
from pprint import pprint
from controllers import admin_manager, match_manager


import config
from config import bcrypt
from controllers import admin_manager
from controllers.match_manager import get_over_history
from core_utils.dict_utils import map_keys
from core_utils.string_utils import to_snake_case, to_camel_case
from core_utils.views_utils import to_object_format, to_ui_format, OkResponse, OkLoginResponse
from models.store_manager import StoreManager, SlowFastKV, RedisKV, KeyMaker
from models import ball_details, match_over
from models.ball_details import BallDetailsUISchema
from models.match_state import MatchStateUISchema
from models.match_details import MatchDetailsUISchema, ScorecardUISchema
from models.team_details import TeamDetailsUISchema
from views.admin import admin


from flask import Flask, request, jsonify
from flask_cors import CORS
from werkzeug.exceptions import BadRequest, NotFound, Unauthorized
from models.player_info import PlayerInfoUISchema
from controllers.api_manager import CricbayApi, ApiManager
from datetime import datetime, timedelta

__author__ = 'arbaz'

flask_app = Flask(__name__)
flask_app.secret_key = '!secretKey!'
flask_app.register_blueprint(admin, url_prefix='/admin')

CORS(flask_app)


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

RECENT_RESULTS = []


LAST_REMOTE_PULL = None


def check_and_pull_from_remote(remote_auth_token):
    global LAST_REMOTE_PULL
    now = datetime.now()
    if LAST_REMOTE_PULL is None or \
            LAST_REMOTE_PULL + timedelta(hours=4) < now:
        data = admin_manager.pull_matches(remote_auth_token)
        admin_manager.pull_venues(remote_auth_token, data)
        admin_manager.pull_teams(remote_auth_token, data)
        LAST_REMOTE_PULL = now


def read_match_token(match_id, request):
    logger.debug("request-headers: {}".format(request.headers))
    auth_header = request.headers.get('Authorization')
    if auth_header:
        auth_token = auth_header.split(' ')[1]
        logger.debug("auth:{}".format(auth_token))
        match = match_manager.decode_match_token(auth_token,
                                                 candidate_match_id=match_id)
        if match is None:
            return NotFound()
        elif isinstance(match, str):
            return Unauthorized(match)
        elif match.match_id == match_id:
            return True
        else:
            return Unauthorized(match)
    return BadRequest("No authorization header in request")


def authorized_for_match(f):
    @wraps(f)
    def decorated_function(apiversion, match_id, **kwargs):
        match_header = read_match_token(match_id, request)
        if match_header == True:
            return f(apiversion, match_id, **kwargs)
        else:
            return match_header
    return decorated_function


@flask_app.route('/api/<string:apiversion>/team/<string:team_id>', methods=['GET'])
def get_team(apiversion, team_id):
    flask_app.logger.debug("Get-team:{}".format(team_id))
    flask_app.logger.error("Error Get-team:{}".format(team_id))
    team_data = admin_manager.get_team(team_id)
    schema = TeamDetailsUISchema()
    team = to_ui_format(schema.dump(team_data).data) if team_data else None
    logger.debug("Sent-team:{}".format(team_id))
    return jsonify(team) if team else NotFound()


@flask_app.route('/api/<string:apiversion>/match/<string:match_id>/clear', methods=['GET'])
@authorized_for_match
def clear_match(apiversion, match_id):
    # TODO: This should be abstracted in StoreManager itself
    # Obviously also needs admin privileges
    conn = StoreManager.get_redis_connection()
    conn.fast_kv.delete_hash(KeyMaker.make(["match", match_id]))
    return OkResponse()


@flask_app.route('/api/<string:apiversion>/setup_match', methods=['GET', 'POST'])
def setup_match(apiversion):
    if request.method == "GET":
        team1_id = request.args.get('homeTeam', "1")
        team2_id = request.args.get('awayTeam', "2")
        max_overs = int(request.args.get('maxOvers'))
        match_pin = request.args.get('matchPin', '4040')
        admin_manager.create_match("1", team1_id, team2_id,
                                   match_pin=match_pin,
                                   max_overs=max_overs)
        return OkResponse()

    logger.info("setup match")
    create_match(apiversion)
    logger.info("created match")
    return OkResponse()


# endpoint to show all matches
@flask_app.route("/api/<string:apiversion>/match", methods=["GET"])
def get_matches(apiversion):
    # data = admin_manager.pull_matches()
    # admin_manager.pull_venues(data)
    matches = admin_manager.get_all_matches()
    schema = MatchDetailsUISchema(many=True, only=("short_inning_details",
                                                   "match_id",
                                                   "venue",
                                                   "group_order",
                                                   "home_team",
                                                   "away_team",
                                                   "match_date",
                                                   "match_time",
                                                   "scoring_enabled",
                                                   "game_group"))
    matches_dct = {"matches": schema.dump(matches).data}
    matches_ui_dct = to_ui_format(matches_dct)
    return jsonify(matches_ui_dct)

# endpoint to get all possible players in a match including umpires


@flask_app.route("/api/<string:apiversion>/match_players/<string:match_id>", methods=["POST"])
def pull_match_players(apiversion, match_id):
    request_data = to_object_format(request.get_json(force=True))
    schema = ApiManager.api_stub.DataPullSchema()
    pull_object, errors = schema.load(request_data)
    if errors:
        raise Exception("match_players request error:{}".format(errors))
    match_players = admin_manager.pull_match_players(match_id, pull_object.auth_token)

    if match_players:
        schema = PlayerInfoUISchema(many=True)
        dct = {"players": schema.dump(match_players).data}
        ui_dct = to_ui_format(dct)
        return jsonify(ui_dct)
    else:
        return jsonify({})


@flask_app.route('/api/<string:apiversion>/create_match', methods=['GET', 'POST'])
def create_match(apiversion):
    match_id = "1"
    #-----#
    team1_id = "1"
    team2_id = "2"
    admin_manager.create_players_for_team(team1_id)
    admin_manager.create_players_for_team(team2_id)
    admin_manager.create_match(match_id, team1_id, team2_id,
                               match_pin='4040', max_overs=20)
    return OkResponse()


@flask_app.route('/api/<string:apiversion>/match/<string:match_id>', methods=['GET'])
def get_match(apiversion, match_id):
    match = match_manager.get_match(match_id)
    schema = MatchDetailsUISchema()
    match_data = to_ui_format(schema.dump(match).data) if match else None
    logger.debug("Sent-match:{}".format(match_id))
    return jsonify(match_data) if match_data else NotFound()


@flask_app.route('/api/<string:apiversion>/match/<string:match_id>/live', methods=['POST'])
@authorized_for_match
def post_match_live(apiversion, match_id):
    if request.method == "POST":
        match_setup_data = to_object_format(request.get_json(force=True))
        schema = MatchDetailsUISchema()

        match_details, error = schema.load(match_setup_data)
        if error:
            msg_json = {"Schema Load Error": error}
            logger.error(error)
            return BadRequest("Match data schema not recognized: {}".format(error))

        # if not match_details.is_scoring_enabled():
        #    return BadRequest("Scoring not yet enabled for this match.")

        match_state_object = match_manager.start_match(match_details)
        state_schema = MatchStateUISchema()
        match_state = to_ui_format(
            state_schema.dump(match_state_object).data) if match_state_object else None
        return jsonify(match_state) if match_state else BadRequest()


@flask_app.route('/api/<string:apiversion>/match/<string:match_id>/live', methods=['GET'])
def get_match_live(apiversion, match_id):
    if request.method == "GET":
        match_state = match_manager.get_current_match_state(match_id)
        state_schema = MatchStateUISchema()
        match_state = to_ui_format(
            state_schema.dump(match_state).data) if match_state else None
        return jsonify(match_state) if match_state else NotFound()


@flask_app.route('/api/v1/match/<string:match_id>/login', methods=['POST'])
def login_scorer_with_pin(match_id):
    if request.method == "POST":
        login_data = to_object_format(request.get_json(force=True))

        if not login_data:
            return BadRequest()

        username = login_data.get("username")
        match_pin = login_data.get("match_pin")
        match = match_manager.get_match(match_id)

        if match is None:
            return NotFound()

        if not match_manager.is_valid_credential(match_pin, match.match_pin):
            return Unauthorized()

        auth_token = match_manager.encode_auth_token(match.match_id)
        return OkLoginResponse(auth_token.decode())


@flask_app.route('/api/<string:apiversion>/scorer/login', methods=['POST'])
def login_scorer_with_email(apiversion):

    if request.method == "POST":
        login_data = to_object_format(request.get_json(force=True))

        if not login_data:
            return BadRequest()

        logged_user = ApiManager.login(login_data)
        if not logged_user or isinstance(logged_user, str):
            return BadRequest(logged_user)

        check_and_pull_from_remote(logged_user['auth_token'])

        team_matches = match_manager.get_live_matches_for_team(
            logged_user['team_id'])

        if not team_matches:
            match_id = "000"
        else:
            for match in team_matches:
                admin_manager.pull_matches(logged_user['auth_token'], match.match_id)
            match_id = ",".join(map(lambda x: x.match_id, team_matches))

        auth_token = match_manager.encode_auth_token(match_id)
        logged_user.update({'match_id': match_id})
        logged_user_ui = to_ui_format(logged_user)
        resp = OkLoginResponse(auth_token.decode(), logged_user_ui)
        return resp


@flask_app.route('/api/<string:apiversion>/match/login', methods=['POST'])
def login_scorer_with_api(apiversion):
    if request.method == "POST":
        login_data = to_object_format(request.get_json(force=True))
        if not login_data:
            return BadRequest("No authentication data")

        logged_user = ApiManager.login(login_data)
        if logged_user and isinstance(logged_user, dict):
            logged_user_ui = to_ui_format(logged_user)
            return jsonify(logged_user_ui)
        else:
            return BadRequest(logged_user)


@flask_app.route('/api/<string:apiversion>/match/<string:match_id>/next', methods=['GET', 'POST'])
@authorized_for_match
def get_next_innings(apiversion, match_id):
    if request.method == "POST":
        # TODO: In case more than two innings are played,
        #     changes can be put here to read batting/bowling team
        return BadRequest()

    if request.method == "GET":
        # check if its a valid call at this point
        new_match_state = match_manager.make_next_innings(match_id)
        if new_match_state:
            state_schema = MatchStateUISchema()
            match_state_ui = to_ui_format(state_schema.dump(new_match_state).data)
            return jsonify(match_state_ui)
        else:
            curr_match_state = match_manager.get_current_match_state(match_id)
            if curr_match_state:
                state_schema = MatchStateUISchema()
                match_state = to_ui_format(
                    state_schema.dump(curr_match_state).data)
                return jsonify(match_state)
            else:
                return NotFound()


@flask_app.route('/api/<string:apiversion>/match/<string:match_id>/end', methods=['POST'])
@authorized_for_match
def force_state_end(apiversion, match_id):
    if request.method == "POST":
        force_end_data = to_object_format(request.get_json(force=True))
        logger.debug("Input: {}".format(force_end_data))
        if force_end_data.get("match_end", False) == True:
            new_match_state = match_manager.force_match_end(match_id)
            if new_match_state:
                state_schema = MatchStateUISchema()
                match_state_ui = to_ui_format(
                    state_schema.dump(new_match_state).data)
                return jsonify(match_state_ui)
            return NotFound()
        elif force_end_data.get("inning_end", False) == True:
            new_match_state = match_manager.force_inning_end(
                match_id, force_end_data.get("inning_id"))
            if new_match_state:
                logger.debug("Output: {}".format(new_match_state))
                state_schema = MatchStateUISchema()
                match_state_ui = to_ui_format(
                    state_schema.dump(new_match_state).data)
                return jsonify(match_state_ui)
            return NotFound()
        else:
            return BadRequest()


@flask_app.route('/api/<string:apiversion>/match/<string:match_id>/scorecard', methods=['GET'])
def get_match_scorecard(apiversion, match_id):
    match = match_manager.get_match(match_id)
    schema = ScorecardUISchema()
    scorecard = to_ui_format(schema.dump(match).data) if match else None
    return jsonify(scorecard) if scorecard else NotFound()


@flask_app.route('/api/<string:apiversion>/match/<string:match_id>/overs', methods=['GET'])
def get_match_overs(apiversion, match_id):
    if request.method == "GET":
        num_overs = int(request.args.get('numOvers', 3))
        over_till = request.args.get('overTill')
        over_till = int(over_till) if over_till else None
        match_id = request.args.get("matchId")
        inning_id = request.args.get("inningId")
        over_history = get_over_history(match_id, inning_id, over_till, num_overs)
        return jsonify(to_ui_format(over_history)) if over_history else BadRequest()


@flask_app.route('/api/<string:apiversion>/create_team', methods=['GET', 'POST'])
def create_team(apiversion):
    team_id = "1"
    admin_manager.create_players_for_team(team_id)


def start_innings(apiversion):
    match_id = "1"
    inning_id = "1"
    batting_team = "1"
    bowling_team = "2"
    striker = None
    non_striker = None
    inning = match_manager.start_innings(match_id, inning_id, batting_team,
                                         bowling_team)
    match_state_data = inning.match_state
    state_schema = MatchStateUISchema()
    match_state = to_ui_format(state_schema.dump(match_state_data).data)
    return match_state


@flask_app.route('/api/<string:apiversion>/create_match', methods=['GET', 'POST'])
def start_over(apiversion):
    match_id = "1"
    inning_id = "1"
    bowler_id = "2001"
    over_number = 1
    #-----#
    inning = match_manager.start_over(
        match_id, inning_id, bowler_id, over_number, is_powerplay=False)
    match_state_data = inning.match_state
    state_schema = MatchStateUISchema()
    match_state = to_ui_format(state_schema.dump(match_state_data).data)
    logger.debug("Inning-JSON: {}".format(match_state))
    return match_state


@flask_app.route('/api/<string:apiversion>/match/<string:match_id>/add_ball_event', methods=['POST'])
@authorized_for_match
def add_ball_event(apiversion, match_id):
    if request.method == "POST":
        ball_raw_json = request.get_json(force=True)
        # logger.debug("##############:{}".format(ball_raw_json))
        ball_ui_dict = to_object_format(ball_raw_json)
        ball_schema = BallDetailsUISchema()
        ball = ball_schema.load(ball_ui_dict).data
        match_state = match_manager.get_current_match_state(match_id)
        if match_state.ball_sequence >= ball.ball_sequence:
            return BadRequest("Ball out of sequence. Expected:{}\n"
                              "Received: {}".format(
                                  match_state.ball_sequence + 1,
                                  ball.ball_sequence))
        match_id = ball.match_id
        inning_id = ball.inning_id
        bowler_id = ball.get_bowler()
        logger.debug((match_id, inning_id, bowler_id))
        over_number = ball.over_number
        match_state_data = match_manager.add_ball(ball)
        state_schema = MatchStateUISchema()
        match_state = to_ui_format(state_schema.dump(match_state_data).data)
        RECENT_RESULTS.append((ball_ui_dict, "-------------------", match_state))

        if len(RECENT_RESULTS) == 5:
            # logger.debug("\n#########\n".join(map(str, RECENT_RESULTS)))
            RECENT_RESULTS.clear()

        return jsonify(match_state)


@flask_app.route('/api/<string:apiversion>/match/<string:match_id>/undo_ball_event', methods=['POST'])
@authorized_for_match
def undo_ball_event(apiversion, match_id):
    if request.method == "POST":
        ball_raw_json = request.get_json(force=True)
        # logger.debug("##############:{}".format(ball_raw_json))
        ball_ui_dict = to_object_format(ball_raw_json)
        ball_schema = BallDetailsUISchema()
        ball = ball_schema.load(ball_ui_dict).data
        match_state = match_manager.get_current_match_state(match_id)
        if match_state.ball_sequence > ball.ball_sequence:
            return BadRequest("Ball out of sequence. Expected:{}\n"
                              "Received: {}".format(
                                  match_state.ball_sequence,
                                  ball.ball_sequence))
        match_state_before_ball = match_manager.undo_ball(ball)
        state_schema = MatchStateUISchema()
        match_state = to_ui_format(state_schema.dump(match_state_before_ball).data)
        return jsonify(match_state)


@flask_app.before_first_request
def setup_server():
    if not flask_app.debug:
        # In production mode, __main__ is not run
        # Thus everything needs to be setup before first request
        pass


@flask_app.errorhandler(404)
def not_found_error(error):
    raise NotFound()

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    gunicorn_logger = logging.getLogger('gunicorn.error')
    flask_app.logger.handlers.extend(gunicorn_logger.handlers)
    flask_app.logger.setLevel(logging.DEBUG)
    flask_app.logger.debug('this will show in the log')
    logger.handlers = gunicorn_logger.handlers
    logger.setLevel(gunicorn_logger.level)
    fast_kv = RedisKV(env_prefix="FASTKV_")
    slow_kv = RedisKV(env_prefix="SLOWKV_")
    slow_fast_kv = SlowFastKV(slow_kv=slow_kv, fast_kv=fast_kv, archive_mode=True)
    StoreManager.set_redis_connection(slow_fast_kv)

if __name__ == "__main__":
    # Fire up Flask once we have the model
    env = os.getenv('ENV', 'development')
    config = config.get_server_config(env)
    handler = StreamHandler()
    handler.setLevel(logging.DEBUG)
    flask_app.logger.addHandler(handler)

    fast_kv = RedisKV(env_prefix="FASTKV_")
    slow_kv = RedisKV(env_prefix="SLOWKV_")
    slow_fast_kv = SlowFastKV(slow_kv=slow_kv, fast_kv=fast_kv, archive_mode=True)
    StoreManager.set_redis_connection(slow_fast_kv)

    flask_app.run(**config)
    # if not DRUG_TO_INT:
    #     DRUG_TO_INT, RX_DATA = _cache_data()
