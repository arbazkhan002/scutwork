import json

from core_utils.schema_utils import BaseSchema
from models import store_manager, match_state

from marshmallow import post_load, fields
from models import store_manager
import pdb
from models.ball_details import BallDetailsUISchema
from models.player_info import PlayerInfoUISchema
from models import inning_details as inning_details_m


__author__ = 'arbaz'


class MatchOver:

    def __init__(self, match_id, inning_id, over_number,
                 runs=0, wickets=0, inning_score=0,
                 inning_wickets=0, ball_sequences=None,
                 player_striker=None, player_non_striker=None,
                 player_current_bowler=None, striker_score=None,
                 non_striker_score=None, bowler_score=None):
        """
        runs: int
            runs in over
        wickets: int
            wickets in over
        ball_sequences: list(str)
            list of ball_sequence in over
        inning_score: int
        inning_wickets: int
        player_striker: str
            striker-id at end of over
        player_non_striker: str
            striker-id at end of over
        player_current_bowler: str
            bowler who bowled last ball of over
        bowler_score: dict
            stats that show up at the end of over        
        striker_score: dict
            stats that show up at the end of over        
        non_striker_score: dict
            stats that show up at the end of over
        """
        self.match_id = match_id
        self.inning_id = inning_id
        self.over_number = over_number
        self.runs = runs
        self.wickets = wickets
        self.ball_sequences = ball_sequences if ball_sequences is not None else []
        self.inning_score = inning_score
        self.inning_wickets = inning_wickets
        self.player_striker = player_striker
        self.player_non_striker = player_non_striker
        self.player_current_bowler = player_current_bowler
        self.striker_score = striker_score
        self.non_striker_score = non_striker_score
        self.bowler_score = bowler_score

    def add_ball(self, ball, match_state_after_ball):
        self.ball_sequences.append(ball.ball_sequence)
        self.runs += ball.get_totalruns_scored()
        self.wickets += (1 if ball.get_dismissal_info_details() is not None else 0)
        self.inning_score = match_state_after_ball.score
        self.inning_wickets = match_state_after_ball.wickets
        self.player_striker = self.safe_get(
            lambda: match_state_after_ball.player_striker.get_user_id())
        self.player_non_striker = self.safe_get(
            lambda: match_state_after_ball.player_non_striker.get_user_id())
        self.player_current_bowler = self.safe_get(
            lambda: match_state_after_ball.player_current_bowler.get_user_id())
        self.striker_score = self.get_batsman(match_state_after_ball.player_striker)
        self.non_striker_score = self.get_batsman(
            match_state_after_ball.player_non_striker)
        self.bowler_score = self.get_bowler(match_state_after_ball.player_current_bowler)
        self.__persist_state()

    def undo_ball(self, ball, match_state_before_ball):
        assert ball.ball_sequence == self.ball_sequences[-1],\
            ("Undoing ball: {}, but balls in over:{}".format(
                ball.ball_sequence, self.ball_sequences))
        self.ball_sequences.pop()
        self.runs -= ball.get_totalruns_scored()
        self.wickets -= (1 if ball.get_dismissal_info_details() is not None else 0)
        if match_state_before_ball:
            self.inning_score = match_state_before_ball.score
            self.inning_wickets = match_state_before_ball.wickets
            self.player_striker = self.safe_get(
                lambda: match_state_before_ball.player_striker.get_user_id())
            self.player_non_striker = self.safe_get(
                lambda: match_state_before_ball.player_non_striker.get_user_id())
            self.player_current_bowler = self.safe_get(
                lambda: match_state_before_ball.player_current_bowler.get_user_id())
            self.striker_score = self.get_batsman(match_state_before_ball.player_striker)
            self.non_striker_score = self.get_batsman(
                match_state_before_ball.player_non_striker)
            self.bowler_score = self.get_bowler(
                match_state_before_ball.player_current_bowler)
        else:
            # first ball of inning
            self.inning_score = 0
            self.inning_wickets = 0
        self.__persist_state()

    def get_batsman(self, batsman):
        # pdb.set_trace()
        if batsman:
            player_info = batsman.get_player_info()
            schema = PlayerInfoUISchema()
            player_score = {
                "score": batsman.get_batting_details().get_short_figures()
            }
            return {**schema.dump(player_info).data, **player_score}

    def get_bowler(self, bowler):
        if bowler:
            player_info = bowler.get_player_info()
            schema = PlayerInfoUISchema()
            player_score = {
                "score": bowler.get_bowling_details().get_short_figures()
            }
            return {**schema.dump(player_info).data, **player_score}

    def safe_get(self, functional_obj):
        try:
            return functional_obj()
        except AttributeError:
            return None

    def to_dict(self):
        schema = MatchOverSchema()
        return schema.dump(self).data

    def to_json(self):
        return json.dumps(self.to_dict())

    def __persist_state(self):
        store_manager.StoreManager.put_match_over(
            self.match_id, self.inning_id, self)

    @classmethod
    def from_dict(cls, dct):
        schema = MatchOverSchema()
        obj = schema.load(dct).data
        return obj

    @classmethod
    def from_json(cls, json_str):
        return cls.from_dict(json.loads(json_str))


class MatchOverSchema(BaseSchema):
    ball_sequences = fields.List(fields.Int())

    class Meta:
        additional = (
            "match_id", "inning_id", "runs", "wickets",
            "inning_score", "over_number", "inning_wickets",
            "player_striker", "player_non_striker",
            "player_current_bowler", "non_striker_score",
            "striker_score", "bowler_score")

    @post_load
    def make_object(self, dct):
        return MatchOver(**dct)


class MatchOverUISchema(MatchOverSchema):
    balls = fields.Method("get_balls")
    player_striker = fields.Dict(attribute="striker_score")
    player_non_striker = fields.Dict(attribute="non_striker_score")
    player_current_bowler = fields.Dict(attribute="bowler_score")
    batting_team = fields.Method("get_batting_team")

    def get_balls(self, obj):
        ball_objects = list(map(
            lambda x: store_manager.StoreManager.get_match_ball(
                obj.match_id, obj.inning_id, x),
            obj.ball_sequences))
        schema = BallDetailsUISchema()
        ball_dcts = [schema.dump(x).data for x in ball_objects]
        return ball_dcts

    def get_batting_team(self, obj):
        inning_detail = store_manager.StoreManager.get_inning_detail(
            obj.match_id, obj.inning_id)
        schema = inning_details_m.InningDetailsUISchema(only=("batting_team",))
        data = schema.dump(inning_detail).data
        return data["batting_team"]
