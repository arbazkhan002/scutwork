import json
import logging
import pdb
from models import ball_details, dismissal
from models.dismissal import DismissalInfoSchema
from core_utils.schema_utils import BaseSchema
from marshmallow import fields
logger = logging.getLogger(__name__)

__author__ = 'arbaz'


class BatsmanDetails:

    def __init__(self, user_id):
        self.user_id = user_id
        self.runs = 0
        self.balls_faced = 0
        self.fours = 0
        self.sixes = 0
        self.bat_dismissal = None
        self.balls = []

    def add_ball(self, ball):
        if ball.get_striker() != self.user_id:
            self.__read_dismissal(ball)
            return

        logger.debug("Adding ball for batsman : " + str(self.user_id))
        if ball.get_run_info().is_wb():
            self.__add_wide_ball(ball)
        else:
            self.__add_legal_ball(ball)
        logger.debug("Score runs : " + str(self.runs))

    def undo_ball(self, ball):
        if (ball.get_striker() != self.user_id):
            self.__undo_read_dismissal(ball)
            return

        # Revert in case of a wide ball just needs to know the dismissal
        if (ball.get_run_info().is_wb()):
            self.__undo_wide_ball(ball)
        else:
            self.__undo_legal_ball(ball)

    def get_short_figures(self):
        return "{}({})".format(self.runs, self.balls_faced)

    def __read_dismissal(self, ball):
        dismissal_data = ball.get_dismissal_info_details()

        # Batsman could be dismissed of a wide.
        if dismissal_data is not None:
            if dismissal_data.batsman_dismissed != self.user_id:
                return
            assert self.bat_dismissal is None
            self.bat_dismissal = dismissal_data

    def clear_dismissal(self):
        self.bat_dismissal = None

    def __undo_read_dismissal(self, ball):
        dismissal_data = ball.get_dismissal_info_details()

        if dismissal_data is not None:
            if dismissal_data.batsman_dismissed != self.user_id:
                return
            assert self.bat_dismissal is not None
            self.bat_dismissal = None

    def __add_wide_ball(self, ball):
        assert ball.get_run_info().is_wb()

        # Batsman could be dismissed of a wide.
        if ball.get_dismissal_info_details() is not None:
            assert self.bat_dismissal is None
            self.bat_dismissal = ball.get_dismissal_info_details()

    def __undo_wide_ball(self, ball):
        assert ball.get_run_info().is_wb()
        self.__undo_read_dismissal(ball)

    def __add_legal_ball(self, ball):
        self.balls_faced += 1
        self.runs += ball.get_run_info().get_batruns_scored()
        self.balls.append(ball)
        if (ball.get_run_info().is4):
            self.fours += 1
        if (ball.get_run_info().is6):
            self.sixes += 1
        self.__read_dismissal(ball)

    def __undo_legal_ball(self, ball):
        saved_ball = self.balls[-1]
        assert saved_ball == ball,\
            ("Expecting last ball: {}".format(saved_ball.to_json()),
             "Undo-ball requested for: {}".format(ball.to_json()))

        saved_ball = self.balls.pop()
        self.balls_faced -= 1
        self.runs -= ball.get_run_info().get_batruns_scored()
        if (ball.get_run_info().is4):
            self.fours -= 1
        if (ball.get_run_info().is6):
            self.sixes -= 1
        self.__undo_read_dismissal(ball)

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def to_dict(self):
        return {k: v for k, v in self.__dict__.items()}

    def set_values(self, balls_faced, fours, runs, sixes, bat_dismissal, balls):
        self.runs = runs
        self.balls_faced = balls_faced
        self.fours = fours
        self.sixes = sixes
        self.bat_dismissal = bat_dismissal
        self.balls = balls

    @staticmethod
    def batting_details_from_map(attribution_fields):
        bat_dismissal = None
        balls = []
        if "bat_dismissal" in attribution_fields:
            bat_dismissal = dismissal.Dismissal.dismissal_from_map(
                attribution_fields.pop("bat_dismissal", None))
        if "balls" in attribution_fields:
            balls_faced_by_batsman = attribution_fields.pop("balls", [])
            for faced in balls_faced_by_batsman:
                balls.append(ball_details.BallDetails.ball_from_map(faced))
        bat_details = BatsmanDetails(user_id=attribution_fields.pop("user_id", None))
        bat_details.set_values(
            **attribution_fields, bat_dismissal=bat_dismissal, balls=balls)
        return bat_details


class BatsmanDetailsUISchema(BaseSchema):
    bat_dismissal = fields.Nested(DismissalInfoSchema)

    class Meta:
        additional = (
            "user_id",
            "runs",
            "balls_faced",
            "fours",
            "sixes")
