import os
import json
import logging
import pdb
from abc import abstractclassmethod
import redis

from models import (match_details, player, inning_details,
                    player_info, match_state, ball_details, team_details,
                    match_over, venue)


logger = logging.getLogger(__name__)


class KVStore:

    def __init__(self):
        pass

    @abstractclassmethod
    def put(self, key, value):
        pass

    @abstractclassmethod
    def get(self, key):
        pass

    @abstractclassmethod
    def delete(self, key):
        pass

    @abstractclassmethod
    def exists(self, key):
        pass

    @abstractclassmethod
    def rename(self, key, new_key):
        pass

    @abstractclassmethod
    def dump(self, key):
        pass

    @abstractclassmethod
    def restore(self, key, ttl, value):
        pass

    @abstractclassmethod
    def get_nested_key(self, top_key):
        """To allow for optimization 
        when keys are grouped under a top-level key

        Returns
        _______
        dict
        key -> value
        """
        pass


class Testdict(KVStore):

    def __init__(self):
        self.object_map = dict()

    def put(self, key, value):
        self.object_map[key] = value

    def get(self, key):
        return self.object_map.get(key, None)

    def delete(self, key):
        self.object_map.pop(key, None)

    def delete_hash(self, key):
        self.object_map.pop(key, None)

    def exists(self, key):
        return key in self.object_map.keys()

    def rename(self, key, new_key):
        self.object_map[new_key] = self.get(key)
        self.delete(key)

    def dump(self, key):
        return self.get(key)

    def restore(self, key, ttl, value):
        self.object_map[key] = value
        return

    def get_nested_key(self, key):
        return {k: self.get(k) for k in self.object_map
                if k.startswith(key) and self.get(k) is not None}


class RedisKV(KVStore):
    env_prefix = "REDISKV_"

    def __init__(self, host=None, port=None, db=None,
                 env_prefix=None, **kwargs):
        self.env_prefix = env_prefix or RedisKV.env_prefix
        self.host = host or os.getenv(self.env_prefix + "HOST")
        self.port = port or os.getenv(self.env_prefix + "PORT")
        self.db = 0 if db is None else db
        self.__conn = redis.StrictRedis(host=self.host,
                                        port=self.port,
                                        db=db,
                                        **kwargs)

    def _get_hash_for(self, key):
        """Partition keys by hash after inferring hashes for each key type"""
        if key.startswith("match"):
            # For keys iwth prefix match, use match_id to form a hash
            hash = KeyMaker.make(KeyMaker.split(key)[:2])
        else:
            hash = KeyMaker.make([KeyMaker.split(key)[0]])
        return hash

    def put(self, key, value):
        hash = self._get_hash_for(key)

        self.__conn.hset(hash, key, value)

    def get(self, key):
        hash = self._get_hash_for(key)
        value = self.__conn.hget(hash, key)
        return value.decode("utf-8") if value else value

    def delete(self, key):
        hash = self._get_hash_for(key)
        self.__conn.hdel(hash, key)

    def exists(self, key):
        hash = self._get_hash_for(key)
        return self.__conn.hexists(hash, key)

    def rename(self, key, new_key):
        self.__conn.rename(key, new_key)

    def dump(self, key):
        return self.__conn.dump(key)

    def restore(self, key, ttl, value):
        return self.__conn.restore(key, ttl, value, replace=True)

    def delete_hash(self, key):
        hash = self._get_hash_for(key)
        self.__conn.delete(hash)

    def get_nested_key(self, top_key):
        hash = self._get_hash_for(top_key)
        return {k.decode("utf-8"): v.decode("utf-8")
                for k, v in self.__conn.hgetall(hash).items()}


class SlowFastKV(KVStore):

    def __init__(self, slow_kv, fast_kv, archive_mode=False):
        """Initialize key-value stores:

        Parameters
        __________
        slow_kv: KeyStore
        fast_kv: KeyStore
        archive_mode: boolean
            If set to true, slowKV is treated as archive and
            nothing gets deleted from there
        """
        self.slow_kv = slow_kv
        self.fast_kv = fast_kv
        self.archive_mode = archive_mode

    def put(self, key, value):
        self.fast_kv.put(key, value)

    def get(self, key):
        value = self.fast_kv.get(key)
        if value is None:
            value = self.slow_kv.get(key)
        return value

    def delete(self, key):
        self.fast_kv.delete(key)
        if not self.archive_mode:
            self.slow_kv.delete(key)

    def delete_hash(self, hash):
        self.fast_kv.delete_hash(hash)
        if not self.archive_mode:
            self.slow_kv.delete_hash(hash)

    def exists(self, key):
        return self.fast_kv.exists(key) or self.slow_kv.exists(key)

    def rename(self, key, new_key):
        self.fast_kv.rename(key, new_key)
        self.slow_kv.rename(key, new_key)

    def sync(self, keys, fast_to_slow=True):
        """Sync `keys` from one KV to another.
        Default is to sync fastKV to slowKV
        """
        if fast_to_slow:
            kv1 = self.fast_kv
            kv2 = self.slow_kv
        else:
            kv1 = self.slow_kv
            kv2 = self.fast_kv
        for key in keys:
            # TODO: Use this instead:
            # https://gist.github.com/josegonzalez/6049a72cb163337a18102743061dfcac
            value = kv1.dump(key)
            if value is not None:
                kv2.restore(key, None, value)

    def get_nested_key(self, top_key):
        """TODO: Works only if fast_kv stores all matches 
        which are usually queried together for, or
        syncs them to slow_kv entirely"""
        value = self.fast_kv.get_nested_key(top_key)
        if value is None or len(value) == 0:
            value = self.slow_kv.get_nested_key(top_key)
        return value


class KeyMaker:
    SEPARATOR = ":"

    @staticmethod
    def make(elements):
        return (KeyMaker.SEPARATOR).join(list(map(str, elements)))

    @staticmethod
    def split(key):
        return key.split(KeyMaker.SEPARATOR)


class StoreManager:

    __redis_connection = None

    @staticmethod
    def get_redis_connection():
        return StoreManager.__redis_connection

    @staticmethod
    def set_redis_connection(connection):
        StoreManager.__redis_connection = connection

    @staticmethod
    def clear_redis_connection():
        StoreManager.__redis_connection = None

    @classmethod
    def key_for(cls, elements):
        return KeyMaker.make(elements)

    @classmethod
    def __ensureRedisConnection(cls):
        # pdb.set_trace()
        connection = cls.get_redis_connection()
        if connection is None:
            test_dict = Testdict()
            cls.set_redis_connection(test_dict)
        # if cls.__redis_connection is None:
        #    __redis_connection = redis.StrictRedis.from_url(os.environ['REDIS_URL'])

    @classmethod
    def get_or_make_player_for_match_inning(cls, player_id, match_id=None,
                                            inning_id=None, make_player=True):
        if player_id is None:
            return None

        if match_id is not None and inning_id is not None:
            store_player = cls.get_player_details(match_id, inning_id, player_id)
            if store_player is not None:
                return store_player

        player_info = cls.get_player_info(player_id)
        store_player = player.Player(player_info) if player_info else None
        if make_player is True and store_player is not None:
            cls.put_player_details(match_id, inning_id, store_player)
        return store_player

    @classmethod
    def get_player_info(cls, player_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["player", player_id])
        if cls.__redis_connection.exists(key):
            return player_info.PlayerInfo.player_info_from_json(cls.__redis_connection.get(key))
        else:
            return None

    @classmethod
    def delete_player_info(cls, player_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["player", player_id])
        cls.__redis_connection.delete(key)

    @classmethod
    def put_player_info(cls, player_info):
        cls.__ensureRedisConnection()
        key = cls.key_for(["player", player_info.get_user_id()])
        cls.__redis_connection.put(key, player_info.to_json())

    @classmethod
    def get_player_details(cls, match_id, inning_id, player_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, player_id])
        if cls.__redis_connection.exists(key):
            return player.Player.player_from_json(cls.__redis_connection.get(key))
        else:
            return None

    @classmethod
    def put_player_details(cls, match_id, inning_id, player):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, player.get_user_id()])
        cls.__redis_connection.put(key, player.to_json())

    @classmethod
    def get_match_state(cls, match_id, inning_id, ball_sequence):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, ball_sequence, "_match_state"])
        if cls.__redis_connection.exists(key):
            match_state_object = match_state.MatchState.match_state_from_json(
                cls.__redis_connection.get(key))
            return match_state_object
        else:
            return None

    @classmethod
    def put_match_state(cls, match_id, inning_id, ball_sequence, match_state_object):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, ball_sequence, "_match_state"])
        cls.__redis_connection.put(key, match_state_object.to_json())

    @classmethod
    def delete_match_state(cls, match_id, inning_id, ball_sequence):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, ball_sequence, "_match_state"])
        cls.__redis_connection.delete(key)

    @classmethod
    def get_inning_detail(cls, match_id, inning_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, "_inning_detail"])
        if cls.__redis_connection.exists(key):
            inning_detail_json = cls.__redis_connection.get(key)
            return inning_details.InningDetails.inning_detail_from_json(inning_detail_json)
        else:
            return None

    @classmethod
    def delete_inning_details(cls, match_id, inning_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, "_inning_detail"])
        cls.__redis_connection.delete(key)

    @classmethod
    def put_inning_details(cls, match_id, inning_id, inning_detail):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, "_inning_detail"])
        cls.__redis_connection.put(key, inning_detail.to_json())

    @classmethod
    def put_match_ball(cls, match_id, inning_id, ball):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, ball.ball_sequence, "ball"])
        cls.__redis_connection.put(key, ball.to_json())

    @classmethod
    def get_match_ball(cls, match_id, inning_id, ball_sequence):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, ball_sequence, "ball"])
        if cls.__redis_connection.exists(key):
            return ball_details.BallDetails.from_json(cls.__redis_connection.get(key))
        else:
            return None

    @classmethod
    def put_match_over(cls, match_id, inning_id, match_over):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, match_over.over_number, "over"])
        cls.__redis_connection.put(key, match_over.to_json())

    @classmethod
    def get_match_over(cls, match_id, inning_id, over_number):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id, inning_id, over_number, "over"])
        if cls.__redis_connection.exists(key):
            return match_over.MatchOver.from_json(cls.__redis_connection.get(key))
        else:
            return None

    @classmethod
    def get_match_details(cls, match_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id])
        if cls.__redis_connection.exists(key):
            return match_details.MatchDetails.from_json(cls.__redis_connection.get(key))
        else:
            return None

    @classmethod
    def put_match_details(cls, match_id, match_details):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id])
        cls.__redis_connection.put(key, match_details.to_json())

    @classmethod
    def put_in_match_store(cls, match):
        # TODO: Match date can be used to group matches
        cls.__ensureRedisConnection()
        key = cls.key_for(["live_matches", match.match_id])
        cls.__redis_connection.put(key, match.match_id)

    @classmethod
    def exists_in_match_store(cls, match_id):
        # TODO: Match date can be used to group matches
        cls.__ensureRedisConnection()
        key = cls.key_for(["live_matches", match_id])
        return cls.__redis_connection.exists(key)

    @classmethod
    def delete_match(cls, match_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id])
        cls.__redis_connection.delete_hash(key)

    @classmethod
    def archive_match(cls, match_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["match", match_id])
        cls.__redis_connection.sync(key)
        cls.delete_match(match_id)

    @classmethod
    def put_team_details(cls, team_id, team_details):
        cls.__ensureRedisConnection()
        key = cls.key_for(["team", team_id])
        cls.__redis_connection.put(key, team_details.to_json())

    @classmethod
    def delete_team(cls, team_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["team", team_id])
        cls.__redis_connection.delete(key)

    @classmethod
    def get_team_details(cls, team_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["team", team_id])
        if cls.__redis_connection.exists(key):
            return team_details.TeamDetails.from_json(cls.__redis_connection.get(key))
        else:
            return None

    @classmethod
    def get_venue_details(cls, venue_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["venue", venue_id])
        if cls.__redis_connection.exists(key):
            return venue.Venue.from_json(cls.__redis_connection.get(key))
        else:
            return None

    @classmethod
    def put_venue_details(cls, venue_id, venue_details):
        cls.__ensureRedisConnection()
        key = cls.key_for(["venue", venue_id])
        cls.__redis_connection.put(key, venue_details.to_json())

    @classmethod
    def delete_venue(cls, venue_id):
        cls.__ensureRedisConnection()
        key = cls.key_for(["venue", venue_id])
        cls.__redis_connection.delete(key)

    @classmethod
    def get_all_teams(cls):
        cls.__ensureRedisConnection()
        key = cls.key_for(["team"])
        teams = cls.__redis_connection.get_nested_key(key)
        if not teams:
            return []
        result = []
        for t in teams:
            if t is None or not teams[t]:
                continue
            detail = team_details.TeamDetails.from_json(teams[t])
            if detail is None:
                continue
            result.append(detail)
        return result

    @classmethod
    def get_all_matches(cls):
        cls.__ensureRedisConnection()
        key = cls.key_for(["live_matches"])
        matches = cls.__redis_connection.get_nested_key(key)
        if not matches:
            return []
        result = []
        for t in matches:
            if t is None or not matches[t]:
                continue
            detail = cls.get_match_details(matches[t])
            if detail is None:
                continue
            result.append(detail)
        return result

    @classmethod
    def get_all_venues(cls):
        cls.__ensureRedisConnection()
        key = cls.key_for(["venue"])
        venues = cls.__redis_connection.get_nested_key(key)
        if not venues:
            return []
        result = []
        for t in venues:
            if t is None or not venues[t]:
                continue
            detail = venue.Venue.from_json(venues[t])
            if detail is None:
                continue
            result.append(detail)
        return result

    @classmethod
    def archive_matches(self, tag):
        "`tag` to be used as suffix to archive"
        cls.__ensureRedisConnection()
        key = cls.key_for(["live_matches"])
        cls.__redis_connection.rename("live_matches",
                                      "live_matches" + tag)
