import json

from core_utils.schema_utils import BaseSchema
from models import ball_details, dismissal, store_manager

from marshmallow import post_load, fields
from models.player_info import PlayerInfoUISchema
import pdb


__author__ = 'arbaz'


class TeamDetails:

    def __init__(self, team_id, team_name, roster=None):
        self.team_name = team_name
        self.team_id = team_id
        # In KV stores, you need a reverse index if you are
        # querying by an attribute
        # So getting roster for a team is expensive if not
        # stored explicity
        self.roster = roster if roster is not None else []

    def get_team_id(self):
        return self.team_id

    def get_team_name(self):
        return self.team_name

    def add_player(self, player_info):
        index = self.exists(player_info)
        if index == -1:
            self.roster.append(player_info)
            self.__persist_state()

    def remove_player(self, player_info):
        index = self.exists(player_info)
        if index > -1:
            del self.roster[index]
            self.__persist_state()

    def exists(self, player_info):
        if player_info is None:
            return -1
        for index, p in enumerate(self.roster):
            if p.player_id == player_info.player_id:
                return index
        return -1

    def to_json(self):
        return json.dumps(self.to_dict(), sort_keys=True, indent=4)

    def to_dict(self):
        team_details = {k: v for k, v in self.__dict__.items()}
        roster = team_details.pop('roster', [])
        player_ids = list(set(roster_player.player_id
                              for roster_player in roster
                              if roster_player is not None))
        team_details['roster'] = player_ids
        return team_details

    @staticmethod
    def from_dict(dct):
        roster_player_ids = dct.pop('roster', [])
        roster_players = [
            store_manager.StoreManager.get_player_info(x) for x in roster_player_ids]
        # Some deleted players which aren't in store
        # might still show up as Ids, remove them
        dct['roster'] = [x for x in roster_players if x is not None]
        return TeamDetails(**dct)

    @staticmethod
    def from_json(json_str):
        json_dict = json.loads(json_str)
        return TeamDetails.from_dict(json_dict)

    def persist(self):
        return self.__persist_state()

    def __persist_state(self):
        store_manager.StoreManager.put_team_details(self.team_id, self)


class TeamDetailsUISchema(BaseSchema):
    roster = fields.Nested(PlayerInfoUISchema, many=True)

    class Meta:
        additional = ("team_name", "team_id")

    @post_load
    def make_object(self, data):
        return TeamDetails(**data)
