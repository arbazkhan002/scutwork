#! /usr/bin/env python
"""
ball_details.py
~~~~~~~~~~~~~~~~~~~

This class object is a direct representation of each ball in the game
of cricket. It encapsulates the different information/metrics that 
need to be collected for each ball.
"""
import models.run_details as run_details
from models import dismissal, player_info
import json
from core_utils import string_utils, dict_utils
import logging
from marshmallow import fields
from models.dismissal import DismissalInfoSchema
from models.run_details import RunDetailsSchema
from core_utils.schema_utils import BaseSchema
from marshmallow.decorators import post_load
from models.player_info import PlayerInfo, PlayerInfoUISchema
from datetime import datetime

logger = logging.getLogger(__name__)


class BallDetails:

    NUMBER_OF_BALLS_IN_OVER = 6

    def __init__(self, match_id, inning_id, over_number, ball_number, ball_sequence,
                 run_info, dismissal_info, striker, non_striker, bowler,
                 is_power_play=False, is_batting_powerplay=False,
                 commentary_text="no run", commentary_input=None,
                 timestamp=None):
        self.match_id = match_id
        self.inning_id = inning_id
        self.ball_number = ball_number
        self.ball_sequence = ball_sequence
        self.over_number = over_number
        self.dismissal_info = dismissal_info
        self.run_info = run_info
        self.is_power_play = is_power_play
        self.is_batting_powerplay = is_batting_powerplay
        self.bowler = bowler
        self.striker = striker
        self.non_striker = non_striker
        self.commentary_text = commentary_text
        self.commentary_input = commentary_input
        self.timestamp = timestamp or int(datetime.utcnow().timestamp())

    def get_over_number(self):
        return self.over_number

    def get_ball_number(self):
        return self.ball_number

    def get_run_info(self):
        return self.run_info

    def get_dismissal_info_details(self):
        return self.dismissal_info if self.dismissal_info.get_dismissal_type() else None

    def get_totalruns_scored(self):
        return self.run_info.get_totalruns_scored()

    def get_striker(self):
        return self.striker.get_user_id()

    def get_non_striker(self):
        return self.non_striker.get_user_id()

    def get_bowler(self):
        return self.bowler.get_user_id()

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def __eq__(self, other):
        if isinstance(self, other.__class__):
            return self.ball_number == other.ball_number and self.over_number == other.over_number
        return False

    @classmethod
    def from_json(cls, json_str):
        json_dict = json.loads(json_str)
        return cls.ball_from_map(json_dict)

    @staticmethod
    def ball_from_map(attribution_fields):
        run_info = None
        dismissal_details = None
        for key in attribution_fields:
            if key in ['striker', 'non_striker', 'bowler']:
                attribution_fields[key] = PlayerInfo.player_info_from_map(
                    attribution_fields[key])
        if "run_info" in attribution_fields:
            run_info = run_details.RunDetails.run_details_from_map(
                attribution_fields.pop("run_info", None))
        if "dismissal_info" in attribution_fields:
            dismissal_details = dismissal.Dismissal.dismissal_from_map(
                attribution_fields.pop("dismissal_info", None))
        return BallDetails(**attribution_fields, run_info=run_info, dismissal_info=dismissal_details)


class BallDetailsUISchema(BaseSchema):
    dismissal_info = fields.Nested(DismissalInfoSchema,
                                   load_from="wicket",
                                   dump_to="wicket")
    run_info = fields.Nested(RunDetailsSchema,
                             load_from="runs",
                             dump_to="runs")
    bowler = fields.Nested(PlayerInfoUISchema, load_from="player_current_bowler",
                           dump_to="player_current_bowler")
    striker = fields.Nested(PlayerInfoUISchema, load_from="player_striker",
                            dump_to="player_striker")
    non_striker = fields.Nested(PlayerInfoUISchema, load_from="player_non_striker",
                                dump_to="player_non_striker")
    timestamp = fields.Int(default=None)

    @post_load
    def make_object(self, data):
        return BallDetails(**data)

    class Meta:
        additional = ("match_id", "inning_id", "ball_number", "ball_sequence",
                      "over_number", "is_power_play", "is_batting_powerplay",
                      "commentary_input")
