"""
Each player in cricket has metrics in two major formats for a game.
One would their batting details which include the number of runs scored,
boundries, dismissal_mode and balls faced. In terms of bowling details,
the metrics include number of overs balled, number of wickets taken, 
number of extras given and the number of maidens. This class helps
keep track of such things for each user.
"""

from models import ball_details, dismissal, batsman_details, bowler_details,\
    store_manager
from models.player_info import PlayerInfo, PlayerInfoUISchema
import json
import logging
import pdb
from core_utils.schema_utils import BaseSchema
from marshmallow import fields
from models.batsman_details import BatsmanDetailsUISchema
from models.bowler_details import BowlerDetailsUISchema

logger = logging.getLogger(__name__)


class Player:

    def __init__(self, player_info):
        self.player_info = player_info
        user_id = player_info.get_user_id()
        self.batting_details = batsman_details.BatsmanDetails(user_id)
        self.bowling_details = bowler_details.BowlerDetails(user_id)

    def get_user_id(self):
        return self.player_info.get_user_id()

    def set_batting_details(self, batting_details):
        self.batting_details = batting_details

    def get_batting_details(self):
        return self.batting_details

    def set_bowling_details(self, bowling_details):
        self.bowling_details = bowling_details

    def get_player_info(self):
        return self.player_info

    def get_bowling_details(self):
        return self.bowling_details

    def add_batting_ball(self, ball):
        self.batting_details.add_ball(ball)

    def undo_batting_ball(self, ball):
        self.batting_details.undo_ball(ball)

    def add_bowling_ball(self, ball):
        self.bowling_details.add_ball(ball)

    def undo_bowling_ball(self, ball):
        self.bowling_details.undo_ball(ball)

    @staticmethod
    def get_user_id_or_none(player):
        if player is None:
            return None
        return player.get_user_id()

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def to_dict(self):
        return {k: v for k, v in self.__dict__.items()}

    def persist_info(self):
        store_manager.StoreManager.put_player_info(self.player_info)

    @staticmethod
    def player_from_json(dct):
        attribution_fields = json.loads(dct)
        player_info = None
        bat_details = None
        ball_info = None
        if "player_info" in attribution_fields:
            player_info = PlayerInfo.player_info_from_map(
                attribution_fields.pop("player_info", None))
        if "batting_details" in attribution_fields:
            bat_details = batsman_details.BatsmanDetails.batting_details_from_map(
                attribution_fields.pop("batting_details", None))
        if "bowling_details" in attribution_fields:
            ball_info = bowler_details.BowlerDetails.bowling_details_from_map(
                attribution_fields.pop("bowling_details", None))
        player_details = Player(player_info=player_info)
        player_details.set_batting_details(bat_details)
        player_details.set_bowling_details(ball_info)
        return player_details


class PlayerUISchema(BaseSchema):
    player_info = fields.Nested(PlayerInfoUISchema)
    batting_details = fields.Nested(BatsmanDetailsUISchema)
    bowling_details = fields.Nested(BowlerDetailsUISchema)
    user_id = fields.Str()
