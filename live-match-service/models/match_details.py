"""
Class keeps information pertaining to the match itself
"""
import json
import logging
import pytz
from models import store_manager
from models import player, ball_details
from models.inning_details import InningDetails, InningDetailsUISchema
from core_utils.schema_utils import BaseSchema
from core_utils.dict_utils import map_keys
from core_utils import string_utils
from marshmallow import fields
import pdb
from core_utils.schema_utils import BaseSchema
from marshmallow import fields
from models.team_details import TeamDetailsUISchema
from models.player_info import PlayerInfoUISchema
from marshmallow.decorators import post_dump, post_load
from datetime import datetime, timedelta
from config import Config
from models.venue import VenueUISchema
from models.match_state import MatchResult


logger = logging.getLogger(__name__)


class MatchDetails:

    MAX_PLAYERS_IN_TEAM = 11

    MINIMUN_NUMBER_OF_BOWLERS = 5

    def __init__(self, match_id, home_team, away_team, max_overs,
                 match_pin, umpiring_team_1=None,
                 umpiring_team_2=None, captain_1=None,
                 captain_2=None, umpire_1=None, umpire_2=None,
                 keeper_1=None, keeper_2=None,
                 venue=None, players=None, innings_details=None,
                 toss_won_by=None, team_batting_first=None,
                 match_date=None, match_time=None, game_group=None,
                 max_innings=2):
        self.innings_details = [] if not innings_details else innings_details
        self.match_id = match_id
        self.max_overs = max_overs
        self.max_innings = max_innings
        # teams as team-id
        self.home_team = home_team
        self.away_team = away_team
        # umpiring team-ids
        self.umpiring_team_1 = umpiring_team_1
        self.umpiring_team_2 = umpiring_team_2
        # umpires as player ids
        self.umpire_1 = umpire_1
        self.umpire_2 = umpire_2
        # captains as player ids
        self.captain_1 = captain_1
        self.captain_2 = captain_2
        # keepers as ids
        self.keeper_1 = keeper_1
        self.keeper_2 = keeper_2
        # venue-id
        self.venue = venue

        # strings
        self.match_date = match_date
        self.match_time = match_time

        self.game_group = game_group

        self.players = {} if not players else players
        self.players.setdefault(home_team, [])
        self.players.setdefault(away_team, [])
        self.add_toss_details(toss_won_by, team_batting_first)
        self.match_pin = match_pin
        self.__persist_state()

    def get_datetime(self):
        if self.match_date and self.match_time:
            gmtdatetime = datetime.strptime(self.match_date + ' ' + self.match_time,
                                            '%m/%d/%Y %I:%M%p')
            pst_datetime = pytz.timezone("US/Pacific").localize(gmtdatetime)
            return pst_datetime

    def add_toss_details(self, toss_won_by, team_batting_first):
        self.toss_won_by = toss_won_by
        self.team_batting_first = team_batting_first
        self.__persist_state()

    def add_player_to_team(self, team_id, player_id):
        assert len(self.players[team_id]) < MatchDetails.MAX_PLAYERS_IN_TEAM
        self.players[team_id].append(player_id)
        self.__persist_state()

    def add_players_to_team(self, team_id, player_ids):
        self.players[team_id].extend(player_ids)
        self.__persist_state()

    def is_scoring_enabled(self):
        if not self.match_date or not self.match_time:
            return True
        dt = datetime.strptime(self.match_date + ' ' + self.match_time,
                               '%m/%d/%Y %I:%M%p')
        now = datetime.now()

        return now > dt - timedelta(hours=1)

    def is_setup(self):
        return (self.home_team in self.players
                and self.away_team in self.players
                and self.toss_won_by is not None)

    def start_innings(self, inning_id, batting_team, bowling_team):
        prev_innings = self.innings_details
        if int(inning_id) == self.max_innings:
            target_score = sum(t.match_state.score for t in prev_innings
                               if t.batting_team == bowling_team) + 1
        else:
            target_score = None

        current_innings = InningDetails(match_id=self.match_id, inning_id=inning_id,
                                        batting_team=batting_team,
                                        bowling_team=bowling_team,
                                        max_overs=self.max_overs,
                                        batsman_ids=[],
                                        target_score=target_score,
                                        players_batting_team=self.players[batting_team],
                                        players_bowling_team=self.players[bowling_team])
        self.innings_details.append(current_innings)
        self.__persist_state()
        return current_innings

    def is_match_end(self):
        """When match ends naturally, all innings have ended.
        When match is abandoned, current inning responds True 
        on is_match_end
        """
        # pdb.set_trace()
        if self.max_innings == len(self.innings_details):
            last_inning = self.innings_details[-1]
            return last_inning.is_inning_end()

        if len(self.innings_details) > 0:
            last_inning = self.innings_details[-1]
            return last_inning.is_match_end()

        return False

    def get_match_result(self):
        if not self.is_match_end():
            return None
        if len(self.innings_details) > 0:
            last_inning = self.innings_details[-1]
            last_match_state = last_inning.match_state
            if last_match_state.target_score is not None and \
                    last_match_state.score >= last_match_state.target_score:
                return MatchResult.HOME_TEAM_WON if last_match_state.batting_team == self.home_team else MatchResult.AWAY_TEAM_WON

        return MatchResult.GAME_ABANDONED

    def to_json(self):
        match_details = {k: v for k, v in self.__dict__.items()}
        innings_details = match_details.pop('innings_details', [])
        # We don't update the innings because the innings is self-saved everytime any edit
        # is made to the innings.
        inning_ids = [inning_detail.inning_id for inning_detail in innings_details]
        match_details['innings_details'] = inning_ids
        return json.dumps(match_details, sort_keys=True, indent=4)

    def update(self, **kwargs):
        """
        Update a match using new attributes 
        """
        for k, v in kwargs.items():
            if hasattr(self, k):
                setattr(self, k, v)
        self.persist()

    @classmethod
    def from_json(cls, json_str):
        json_dict = json.loads(json_str)
        match_id = json_dict['match_id']
        inning_ids = json_dict.pop('innings_details', [])
        innings_details = [
            store_manager.StoreManager.get_inning_detail(match_id, i) for i in inning_ids]
        json_dict['innings_details'] = innings_details
        match = MatchDetails(**json_dict)
        return match

    @classmethod
    def max_overs_per_bowler(cls, max_overs_in_game):
        return max_overs_in_game / cls.MINIMUN_NUMBER_OF_BOWLERS

    def persist(self):
        self.__persist_state()

    def __persist_state(self):
        store_manager.StoreManager.put_match_details(self.match_id, self)


class MarshalHelper:

    def team_to_object(self, team_id):
        return store_manager.StoreManager.get_team_details(team_id)

    def get_players_info(self, obj, player_ids):
        players = [store_manager.StoreManager.get_or_make_player_for_match_inning(player_id,
                                                                                  make_player=False)
                   for player_id in player_ids]
        return [p.get_player_info().to_dict() for p in players]

    def get_player_ids(self, players_list):
        return [player_data["player_id"] for player_data in players_list]

    def get_home_team_players(self, obj):
        players = self.get_players_info(obj, obj.players[obj.home_team])
        return players

    def load_players(self, players_data):
        return self.get_player_ids(players_data)

    def load_player(self, player):
        return self.get_player_ids([player])[0]

    def get_away_team_players(self, obj):
        players = self.get_players_info(obj, obj.players[obj.away_team])
        return players

    def get_home_team(self, obj):
        team_dct = self.team_to_object(obj.home_team).to_dict()
        del team_dct["roster"]
        return team_dct

    def get_away_team(self, obj):
        team_dct = self.team_to_object(obj.away_team).to_dict()
        del team_dct["roster"]
        return team_dct

    def get_umpiring_team_1(self, obj):
        team_dct = self.team_to_object(obj.umpiring_team_1).to_dict()
        del team_dct["roster"]
        return team_dct

    def get_umpiring_team_2(self, obj):
        team_dct = self.team_to_object(obj.umpiring_team_2).to_dict()
        del team_dct["roster"]
        return team_dct

    def get_captain_1(self, obj):
        captain = self.get_players_info(obj, [obj.captain_1])[0]
        return captain

    def get_captain_2(self, obj):
        captain = self.get_players_info(obj, [obj.captain_2])[0]
        return captain

    def get_keeper_1(self, obj):
        captain = self.get_players_info(obj, [obj.keeper_1])[0]
        return captain

    def get_keeper_2(self, obj):
        captain = self.get_players_info(obj, [obj.keeper_2])[0]
        return captain

    def get_umpire_1(self, obj):
        umpire = self.get_players_info(obj, [obj.umpire_1])[0]
        return umpire

    def get_umpire_2(self, obj):
        umpire = self.get_players_info(obj, [obj.umpire_2])[0]
        return umpire

    def get_short_inning_details(self, obj):
        innings_details = obj.innings_details
        schema = InningDetailsUISchema(only=("score", "overs", "ball_number",
                                             "wickets", "batting_team",
                                             "bowling_team"), many=True)
        short_innings_data = schema.dump(innings_details).data
        return short_innings_data

    def get_team_batting_first(self, obj):
        team_dct = self.team_to_object(obj.team_batting_first).to_dict()
        del team_dct["roster"]
        return team_dct

    def get_toss_won_by(self, obj):
        team_dct = self.team_to_object(obj.toss_won_by).to_dict()
        del team_dct["roster"]
        return team_dct

    def get_venue(self, obj):
        venue = store_manager.StoreManager.get_venue_details(obj.venue)
        schema = VenueUISchema()
        venue_dct = schema.dump(venue).data
        return venue_dct

    def load_venue(self, venue_data):
        return venue_data["venue_id"]


class ScorecardUISchema(MarshalHelper, BaseSchema):
    """
    Returns the entire match scorecard which is essentially the teams, toss details
    and the innings summary for each batsman/bowler.
    """
    innings_details = fields.Nested(InningDetailsUISchema, many=True)
    home_team = fields.Method("get_home_team", dump_only=True)
    away_team = fields.Method("get_away_team", dump_only=True)
    umpiring_team1 = fields.Method("get_umpiring_team_1", dump_only=True)
    umpiring_team2 = fields.Method("get_umpiring_team_2", dump_only=True)
    umpire1 = fields.Method("get_umpire_1", deserialize="load_player")
    umpire2 = fields.Method("get_umpire_2", deserialize="load_player")
    captain1 = fields.Method("get_captain_1", deserialize="load_player")
    captain2 = fields.Method("get_captain_2", deserialize="load_player")
    keeper1 = fields.Method("get_keeper_1", deserialize="load_player")
    keeper2 = fields.Method("get_keeper_2", deserialize="load_player")
    toss_won_by = fields.Method("get_toss_won_by", dump_only=True)
    team_batting_first = fields.Method("get_team_batting_first", dump_only=True)
    venue = fields.Method("get_venue", deserialize="load_venue")

    class Meta:
        additional = (
            "match_id",
            "max_overs",
            "match_date",
            "match_time",
            "game_group"
        )


class MatchDetailsUISchema(MarshalHelper, BaseSchema):
    """Most of the fields used only for dumping
    While loading, read match_id and use StoreManager to get the details
    Fill in `players` and toss details from the loaded object
    """
    inning_ids = fields.List(fields.Str, attribute="inning_details", dump_only=True)
    home_team = fields.Method("get_home_team", dump_only=True)
    away_team = fields.Method("get_away_team", dump_only=True)
    umpiring_team1 = fields.Method("get_umpiring_team_1", dump_only=True)
    umpiring_team2 = fields.Method("get_umpiring_team_2", dump_only=True)
    umpire1 = fields.Method("get_umpire_1", deserialize="load_player")
    umpire2 = fields.Method("get_umpire_2", deserialize="load_player")
    captain1 = fields.Method("get_captain_1", deserialize="load_player")
    captain2 = fields.Method("get_captain_2", deserialize="load_player")
    keeper1 = fields.Method("get_keeper_1", deserialize="load_player")
    keeper2 = fields.Method("get_keeper_2", deserialize="load_player")
    players_home_team = fields.Method("get_home_team_players",
                                      deserialize="load_players")
    players_away_team = fields.Method("get_away_team_players",
                                      deserialize="load_players")
    short_inning_details = fields.Method("get_short_inning_details", dump_only=True)
    venue = fields.Method("get_venue", deserialize="load_venue")
    group_order = fields.Method("get_group_order", dump_only=True)
    time_epoch = fields.Method("get_epoch", dump_only=True)
    scoring_enabled = fields.Function(lambda x: x.is_scoring_enabled())

    class Meta:
        additional = ("match_id", "max_overs",
                      "toss_won_by", "team_batting_first",
                      "match_date", "match_time", "game_group")

    def get_group_order(self, obj):
        order = {
            "platinum": "0",
            "gold": "1",
            "silver": "2",
            "bronze": "3"
        }
        for k in order:
            if obj.game_group.lower() == k:
                return order[k]
        return "zz"

    def get_epoch(self, obj):
        dt = obj.get_datetime()
        if dt:
            return str(dt.timestamp())

    @post_load
    def make_match_detail(self, data):
        match_from_store = store_manager.StoreManager.get_match_details(data['match_id'])
        match_from_store.add_players_to_team(match_from_store.home_team,
                                             data['players_home_team'])
        match_from_store.add_players_to_team(match_from_store.away_team,
                                             data['players_away_team'])
        match_from_store.add_toss_details(
            data['toss_won_by']["team_id"], data['team_batting_first']["team_id"])
        match_from_store.captain_1 = data['captain1']
        match_from_store.captain_2 = data['captain2']
        match_from_store.keeper_1 = data['keeper1']
        match_from_store.keeper_2 = data['keeper2']
        match_from_store.umpire_1 = data['umpire1']
        match_from_store.umpire_2 = data['umpire2']
        return match_from_store
