import json

from core_utils.schema_utils import BaseSchema
from models import ball_details, dismissal

from marshmallow import post_load


__author__ = 'arbaz'


class PlayerInfo:

    def __init__(self, player_id, name, team_id):
        self.player_id = player_id
        self.name = name
        self.team_id = team_id

    def get_user_id(self):
        return self.player_id

    def get_user_name(self):
        return self.name

    def get_team_id(self):
        return self.team_id

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def to_dict(self):
        return {k: v for k, v in self.__dict__.items()}

    @staticmethod
    def player_info_from_map(attribution_fields):
        return PlayerInfo(**attribution_fields)

    @staticmethod
    def player_info_from_json(dct):
        return PlayerInfo.player_info_from_map(json.loads(dct))


class PlayerInfoUISchema(BaseSchema):

    class Meta:
        fields = ("player_id", "name", "team_id")

    @post_load
    def make_object(self, data):
        return PlayerInfo(**data)
