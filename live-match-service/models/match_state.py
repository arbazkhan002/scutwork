import json
from models import store_manager, inning_details, match_details
from models import player, ball_details
import inspect
from core_utils.dict_utils import map_keys
from core_utils import string_utils
import logging
import pdb
from core_utils.schema_utils import BaseSchema
from marshmallow import fields, pre_load, pre_dump
from models.player import PlayerUISchema
from models.batsman_details import BatsmanDetailsUISchema
from models.bowler_details import BowlerDetailsUISchema
from models.player_info import PlayerInfoUISchema
from marshmallow.decorators import post_dump
from pprint import pprint
from copy import deepcopy, copy
from models.ball_details import BallDetailsUISchema, BallDetails
from models.team_details import TeamDetailsUISchema
import functools
from models.match_over import MatchOver, MatchOverUISchema
from models import match_details as match_details_m


__author__ = 'arbaz'

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class MatchResult:
    HOME_TEAM_WON = 1
    AWAY_TEAM_WON = 2
    GAME_ABANDONED = 3
    HOME_TEAM_FORFEITED = 4
    AWAY_TEAM_FORFEIT = 5


class MatchState:

    def __init__(self, match_id, inning_id, batting_team, bowling_team,
                 batsmen_to_bat, bowlers_to_bowl, players_bowling_team,
                 max_overs, score=0, wickets=0, wides=0, byes=0, nbs=0,
                 batsmen=None, bowlers=None,
                 over_number=0, ball_number=0, power_play_details=None,
                 ball_sequence=0, player_striker=None,  player_non_striker=None,
                 player_current_bowler=None, player_previous_bowler=None,
                 over_complete=True, inning_end=False, match_end=False,
                 last_dismissed_batsman=None, is_strike_switched=False,
                 target_score=None, **kwargs):
        """
        Parameters
        __________
        candidate_batsmen: list(str)

        """
        self.inning_id = inning_id
        self.match_id = match_id
        self.batting_team = batting_team
        self.bowling_team = bowling_team
        self.batsmen = [] if batsmen is None else batsmen
        self.bowlers = [] if bowlers is None else bowlers
        self.batsmen_to_bat = batsmen_to_bat
        self.bowlers_to_bowl = bowlers_to_bowl
        self.max_overs = max_overs
        self.players_bowling_team = players_bowling_team
        self.player_striker = player_striker
        self.player_non_striker = player_non_striker
        self.player_current_bowler = player_current_bowler
        self.player_previous_bowler = player_previous_bowler
        self.is_strike_switched = is_strike_switched
        self.last_dismissed_batsman = last_dismissed_batsman
        self.score = score
        self.wickets = wickets
        self.wides = wides
        self.byes = byes
        self.nbs = nbs
        self.over_number = over_number
        self.ball_number = ball_number
        self.power_play_details = (
            {} if power_play_details is None else power_play_details)
        self.ball_sequence = ball_sequence
        self.over_complete = over_complete
        self.inning_end = inning_end
        self.match_end = match_end
        self.target_score = target_score

    def set_inning_end(self, status):
        self.inning_end = status
        self.persist()

    def set_match_end(self, status):
        self.match_end = status
        self.persist()

    def __update_candidate_bowlers(self, new_bowler_id, curr_bowler):
        """Update candidate bowlers for `over_# + 2` assuming `over_# + 1`
        is going to be bowled by `player_id` and `curr_bowler` bowled `over_#`
        - where `over_#` is the over last bowled

        Also handles the case where curr_bowler retired in middle
        of an over.
        """
        try:
            # new bowler won't bowl the over after
            # hence removing from candidates
            self.bowlers_to_bowl.remove(new_bowler_id)
        except ValueError:
            # bowler not in list
            pass

        # Check if the bowler getting swapped can still bowl more overs
        if (curr_bowler is not None
                and self.can_bowl(curr_bowler)
                and curr_bowler.get_user_id() != new_bowler_id
                and curr_bowler not in self.bowlers_to_bowl):
            self.bowlers_to_bowl.insert(0, curr_bowler.get_user_id())

    def new_bowler(self, player_id, persist=True):
        self.bowlers.append(player_id)
        self.bowlers = self.bowlers[-2:]
        who_was_bowling = (self.player_current_bowler
                           if self.player_current_bowler
                           else self.player_previous_bowler)
        self.__update_candidate_bowlers(player_id, who_was_bowling)
        bowler_created = self._get_player_from_store(player_id)
        self.player_current_bowler = bowler_created
        return bowler_created

    def new_batsman(self, player_id, on_strike, persist=True):
        batsman = self._get_player_from_store(player_id)

        # New batsman check
        if player_id not in self.batsmen:
            # This should be immediately after wicket fall
            assert len(self.batsmen) < 2
            # if batsman came back in after being retired
            batsman.get_batting_details().clear_dismissal()

            self.batsmen_to_bat.remove(player_id)
            self.batsmen.append(player.Player.get_user_id_or_none(batsman))
        if on_strike:
            self.player_striker = batsman
        else:
            self.player_non_striker = batsman
        return batsman

    def undo_new_batsman(self, player_id):
        assert player_id in self.batsmen, "Found: {}, Expected (one of): {}".format(
            player_id, self.batsmen)
        self.batsmen.remove(player_id)
        if player.Player.get_user_id_or_none(self.stiker) == player_id:
            self.player_striker = None
        else:
            self.non_stiker = None

    def _is_same_player(self, player_object, player_id):
        return player_object.get_user_id() == player_id

    def update_players_in_state(self, striker_id, non_striker_id, bowler_id):
        self.player_striker = self.new_batsman(striker_id, on_strike=True)
        self.player_non_striker = self.new_batsman(non_striker_id, on_strike=False)

        if self.player_current_bowler is not None and (
                self.player_current_bowler.get_user_id() != bowler_id):
            self.player_previous_bowler = self.player_current_bowler
        self.player_current_bowler = self.new_bowler(bowler_id)

    def can_bowl(self, bowler_with_details):
        bowling_details = bowler_with_details.get_bowling_details()
        overs_bowled = bowling_details.get_over_count()
        max_bowler_overs = match_details.MatchDetails.max_overs_per_bowler(
            self.max_overs)
        return overs_bowled < max_bowler_overs

    def update_over(self, ball):
        over = store_manager.StoreManager.get_match_over(
            self.match_id, self.inning_id, self.over_number)
        if over is None:
            over = MatchOver(match_id=self.match_id,
                             inning_id=self.inning_id,
                             over_number=self.over_number)
        over.add_ball(ball, self)

    def is_inning_end(self):
        return ((len(self.batsmen) < 2 and len(self.batsmen_to_bat) == 0) or
                self.over_number == self.max_overs or
                self.inning_end == True or
                (self.target_score is not None
                 and self.score >= self.target_score)
                )

    def is_match_end(self):
        return self.match_end == True

    def update_inning_progress(self):
        if self.is_inning_end():
            self.inning_end = True

    def update_score(self, ball):
        self.ball_sequence = ball.ball_sequence
        striker = ball.get_striker()
        non_striker = ball.get_non_striker()
        current_bowler = ball.get_bowler()
        over_number = ball.get_over_number()
        self.update_players_in_state(striker, non_striker, current_bowler)
        self.score += ball.get_totalruns_scored()
        self.wides += ball.get_run_info().get_wide_runs()
        self.byes += ball.get_run_info().get_byes_runs()
        self.nbs += 1 if ball.get_run_info().is_nb else 0
        self.player_striker.add_batting_ball(ball)
        # If non-striker was dismissed
        self.player_non_striker.add_batting_ball(ball)
        current_bowler = self.player_current_bowler
        over_count_before = current_bowler.get_bowling_details().get_over_count()
        current_bowler.add_bowling_ball(ball)
        over_count_after = current_bowler.get_bowling_details().get_over_count()
        bowling_details = current_bowler.get_bowling_details()
        if ball.get_dismissal_info_details() is not None:
            self.wickets += 1
            self.__batsman_dismissed(ball.get_dismissal_info_details())
        if ball.get_run_info().should_switch_batsman():
            self.__switch_striker()
        self.update_over(ball)
        self.ball_number = bowling_details.get_ball_count()
        if ball.is_power_play and over_number not in self.power_play_details:
            self.power_play_details[over_number] = current_bowler.get_user_id()
        if (over_count_after == over_count_before + 1):
            self.__switch_striker()
            self.over_number = self.over_number + 1
            self.player_previous_bowler = self.player_current_bowler
            # No need to explicitly persist here before making None as
            # bowler would be persisted as player_previous_bowler
            self.player_current_bowler = None
            self.over_complete = True
        else:
            self.over_complete = False
        self.update_inning_progress()
        self.persist()
        return self

    def undo_ball(self, ball):
        self.__undo_ball_and_save(ball.get_striker(), ball, True)
        self.__undo_ball_and_save(ball.get_non_striker(), ball, True)
        self.__undo_ball_and_save(ball.get_bowler(), ball, False)
        self.__undo_over(ball)

    def __undo_over(self, ball):
        over_number = ball.get_over_number()
        over = store_manager.StoreManager.get_match_over(
            self.match_id, self.inning_id, over_number)
        match_state_before_ball = store_manager.StoreManager.get_match_state(
            ball.match_id, ball.inning_id, ball.ball_sequence - 1)
        over.undo_ball(ball, match_state_before_ball)

    def copy(self):
        json_str = self.to_json()
        return self.match_state_from_json(json_str)

    def persist(self):
        self.__persist_state()
        return self

    def __debug(self):
        logger.debug(self.batsmen)
        if self.player_striker is not None:
            logger.debug(self.player_striker.to_json())
        if self.player_non_striker is not None:
            logger.debug(self.player_non_striker.to_json())
        if self.player_current_bowler is not None:
            logger.debug(self.player_current_bowler.to_json())
        if self.player_previous_bowler is not None:
            logger.debug(self.player_previous_bowler.to_json())

    def __undo_ball_and_save(self, player_id, ball, is_batsman):
        player_details = store_manager.StoreManager.get_player_details(ball.match_id,
                                                                       ball.inning_id, player_id)
        if is_batsman:
            player_details.undo_batting_ball(ball)
        else:
            player_details.undo_bowling_ball(ball)
        store_manager.StoreManager.put_player_details(ball.match_id, ball.inning_id,
                                                      player_details)

    def get_score(self):
        return self.score

    def set_players_from_store(self, player_striker, player_non_striker, player_current_bowler, player_previous_bowler):
        self.player_striker = self._get_player_from_store(player_striker)
        self.player_non_striker = self._get_player_from_store(player_non_striker)
        self.player_current_bowler = self._get_player_from_store(player_current_bowler)
        self.player_previous_bowler = self._get_player_from_store(
            player_previous_bowler)

    def _serialize_power_play_details(self, pp_dct):
        dct = {}
        for over, bowler_id in pp_dct.items():
            dct[over] = bowler_id

        return json.dumps(dct)

    def _deserialize_power_play_details(self, json_str):
        dct = {}
        for over, bowler_id in json.loads(json_str).items():
            # integer json key gets serialized as string
            # {1: "foo"} on json dumps becomes '{"1": "foo"}'
            dct[int(over)] = bowler_id

        return dct

    def power_play_over_details(self, over):
        if over in self.power_play_details:
            bowler = self._get_player_from_store(self.power_play_details[over])
            return bowler.get_bowling_details().get_over_details(over)
        return None

    def is_empty(self):
        """match_state is empty if its the first ball of an inning"""
        return (self.player_striker is None and
                self.player_non_striker is None and
                self.player_current_bowler is None)

    def to_json(self):
        match_state_dict = {k: v for k, v in self.__dict__.items()}
        for key in filter(lambda key: key.startswith('player_'), match_state_dict.keys()):
            match_state_dict[key] = player.Player.get_user_id_or_none(getattr(self, key))

        match_state_dict["power_play_details"] = self._serialize_power_play_details(
            match_state_dict["power_play_details"])

        json_str = json.dumps(
            match_state_dict, default=lambda x: x.__dict__,
            sort_keys=True, indent=4, cls=inning_details.InningsEncoder)
        return json_str

    @staticmethod
    def match_state_from_json(dct):
        attribution_fields = json.loads(dct)
        player_map = dict()
        non_player_attributes = {
            k: v for k, v in attribution_fields.items() if not k.startswith('player_')}
        key_map = filter(
            lambda key: key.startswith('player_'), attribution_fields.keys())
        for key in key_map:
            player_map[key] = attribution_fields.get(key, None)
        match_state = MatchState(**non_player_attributes)
        match_state.power_play_details = match_state._deserialize_power_play_details(
            match_state.power_play_details)
        match_state.set_players_from_store(**player_map)
        return match_state

    @classmethod
    def from_inning(cls, inning_detail_object):
        match_state = MatchState(
            match_id=inning_detail_object.match_id,
            inning_id=inning_detail_object.inning_id,
            batting_team=inning_detail_object.batting_team,
            bowling_team=inning_detail_object.bowling_team,
            batsmen=[],
            target_score=inning_detail_object.target_score,
            batsmen_to_bat=copy(inning_detail_object.players_batting_team),
            bowlers_to_bowl=copy(inning_detail_object.players_bowling_team),
            players_bowling_team=copy(inning_detail_object.players_bowling_team),
            max_overs=inning_detail_object.max_overs)
        return match_state

    @classmethod
    def from_inning_and_ball(cls, inning_detail_object, ball_detail_object):

        if inning_detail_object.match_state is not None:
            match_state_object = inning_detail_object.match_state
            return match_state_object.copy()
        match_state_object = MatchState.from_inning(inning_detail_object)
        match_state_object.new_batsman(ball_detail_object.get_striker(), True)
        match_state_object.new_batsman(ball_detail_object.get_non_striker(), False)
        match_state_object.new_bowler(ball_detail_object.get_bowler())
        return match_state_object

    def __switch_striker(self):
        logger.debug("Switched strike")
        self.player_non_striker, self.player_striker = self.player_striker, self.player_non_striker
        self.is_strike_switched = not self.is_strike_switched

    def __batsman_dismissed(self, dismissal_details):
        batsman_id = dismissal_details.get_batsman_dismissed()
        assert batsman_id in self.batsmen, "Found: {}, Expected (one of): {}".format(
            batsman_id, self.batsmen)
        if dismissal_details.get_strike_changed():
            self.__switch_striker()

        # Explicitly persist here before making None
        if self.player_striker.get_user_id() == batsman_id:
            self.__put_player_in_store(self.player_striker)
            self.last_dismissed_batsman = self.player_striker
            self.player_striker = None
        else:
            self.__put_player_in_store(self.player_non_striker)
            self.last_dismissed_batsman = self.player_non_striker
            self.player_non_striker = None

        self.batsmen.remove(batsman_id)

        # add retired batsman back to batsmen_to_bat
        if dismissal_details.is_retired():
            self.batsmen_to_bat.append(batsman_id)

    def __undo_batsman_dismissed(self, dismissal_details, batsman):
        batsman_id = batsman.get_user_id()
        assert batsman_id not in self.batsmen
        self.batsmen.append(batsman_id)
        assert len(self.batsmen) == 2
        if dismissal_details.get_strike_changed():
            self.__switch_striker()

    def __persist_state(self):
        players = filter(lambda key: key.startswith('player_'), self.__dict__.keys())
        for key in players:
            self.__put_player_in_store(getattr(self, key))
        store_manager.StoreManager.put_match_state(
            self.match_id, self.inning_id, self.ball_sequence, self)

    def _get_player_from_store(self, player_id):
        store_player = store_manager.StoreManager.get_or_make_player_for_match_inning(
            player_id, self.match_id, self.inning_id, make_player=True)
        return store_player

    def __put_player_in_store(self, player):
        if player is None:
            return None
        store_manager.StoreManager.put_player_details(
            self.match_id, self.inning_id, player)


class MatchStateUISchema(BaseSchema):
    """
    Modifies player_* fields to return full data in one field
    TODO: When extending to Redis or DB schema, override post_load to 
    return data as-is
    """
    batsmen = fields.List(fields.Str)
    bowlers = fields.List(fields.Str)
    player_striker = fields.Nested(PlayerUISchema)
    player_non_striker = fields.Nested(PlayerUISchema)
    player_current_bowler = fields.Nested(PlayerUISchema)
    last_dismissed_batsman = fields.Nested(PlayerUISchema)
    batsmen_to_bat = fields.Method("get_batsmen_to_bat",
                                   deserialize="load_batsmen_to_bat")
    bowlers_to_bowl = fields.Method("get_bowlers_to_bowl",
                                    deserialize="load_bowlers_to_bowl")
    players_bowling_team = fields.Method("get_players_bowling_team",
                                         deserialize="load_players_bowling_team")
    batting_team = fields.Method("make_batting_team",
                                 deserialize=lambda data: data["team_id"])
    bowling_team = fields.Method("make_bowling_team",
                                 deserialize=lambda data: data["team_id"])
    power_play_details = fields.Method("make_power_play_details",
                                       deserialize="load_power_play_details")

    match_details = fields.Method("get_match_details", dump_only=True)

    over_history = fields.Method("make_over_history", dump_only=True)

    def make_over_history(self, obj):
        schema = MatchOverUISchema()
        over_number = obj.over_number
        over_history = []
        for ov in range(over_number, max(-1, over_number - 5), -1):
            over_details = store_manager.StoreManager.get_match_over(
                obj.match_id, obj.inning_id, ov)
            if over_details:
                over_history.append(schema.dump(over_details).data)
        return over_history

    def make_team(self, team_id):
        team_obj = store_manager.StoreManager.get_team_details(team_id)
        team_dict = TeamDetailsUISchema().dump(team_obj).data
        del team_dict["roster"]
        return team_dict

    def make_batting_team(self, obj):
        return self.make_team(obj.batting_team)

    def make_bowling_team(self, obj):
        return self.make_team(obj.bowling_team)

    def get_players_info(self, obj, player_ids):
        return [obj._get_player_from_store(player_id).get_player_info().to_dict()
                for player_id in player_ids]

    def get_players(self, obj, player_ids):
        return [obj._get_player_from_store(player_id)
                for player_id in player_ids]

    def get_player_ids(self, value, field_name):
        player_field = value[field_name]
        return [player_data["player_id"] for player_data in player_field]

    def get_batsmen_to_bat(self, obj):
        details = self.get_players_info(obj, obj.batsmen_to_bat)
        return details

    def from_nested_to_flat(self, dct):
        return functools.reduce(lambda x, y: {**x, **y}, dct.values())

    def get_bowlers_to_bowl(self, obj):
        bowlers_details = self.get_players(obj, obj.bowlers_to_bowl)
        schema = PlayerUISchema(only=(
            "player_info.player_id",
            "player_info.name",
            "player_info.team_id",
            "bowling_details.bowling_figures"))
        bowler_info_with_figures = list(map(
            lambda x: self.from_nested_to_flat(schema.dump(x).data),
            bowlers_details))
        return bowler_info_with_figures

    def get_players_bowling_team(self, obj):
        return self.get_players_info(obj, obj.players_bowling_team)

    def load_batsmen_to_bat(self, obj):
        return self.get_player_ids(obj, "batsmen_to_bat")

    def load_bowlers_to_bowl(self, obj):
        return self.get_player_ids(obj, "bowlers_to_bowl")

    def load_players_bowling_team(self, obj):
        return self.get_player_ids(obj, "players_bowling_team")

    def make_power_play_details(self, obj):
        schema = PlayerInfoUISchema()

        pp_details = []
        for over, bowler_id in obj.power_play_details.items():
            bowler = obj._get_player_from_store(bowler_id)
            runs, wickets = obj.power_play_over_details(over)
            pp_details.append({
                "runs": runs,
                "wickets": wickets,
                "over_number": int(over),
                "bowler": schema.dump(bowler.get_player_info()).data
            })

        return pp_details

    def load_power_play_details(self, pp_details):
        pp_dct = {}
        for item in pp_details:
            pp_dct[item[over_number]] = pp_dct[bowler]["player_id"]
        return pp_dct

    def get_match_details(self, obj):

        match_id = obj.match_id
        match = store_manager.StoreManager.get_match_details(match_id)
        schema = match_details_m.MatchDetailsUISchema(
            only=("short_inning_details", "venue", "game_group", "match_date",
                  "match_time", "time_epoch"))
        match_data = schema.dump(match).data
        return match_data

    @post_dump
    def merge_player_data(self, data):
        for field in ["player_striker", "player_non_striker"]:
            if data.get(field, None) is not None:
                data[field] = {
                    **data[field]["batting_details"], **data[field]["player_info"]}

        for field in ["player_current_bowler"]:
            if data.get(field, None) is not None:
                data[field] = {
                    **data[field]["bowling_details"], **data[field]["player_info"]}

    class Meta:
        additional = (
            "inning_id",
            "match_id",
            "score",
            "wides",
            "byes",
            "nbs",
            "wickets",
            "over_number",
            "ball_number",
            "ball_sequence",
            "max_overs",
            "is_strike_switched",
            "inning_end",
            "match_end",
            "target_score"
        )
