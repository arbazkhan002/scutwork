import json

from core_utils.schema_utils import BaseSchema
from models import store_manager

from marshmallow import post_load, fields
import hashlib


__author__ = 'arbaz'


class Venue:

    def __init__(self, name, city, type, venue_id=None):
        self.venue_id = venue_id or self.make_id(name)
        self.name = name
        self.city = city
        self.type = type

    def get_venue_id(self):
        return self.venue_id

    def get_venue_name(self):
        return self.name

    @classmethod
    def make_id(cls, s):
        m = hashlib.md5()
        m.update(s.encode('utf-8'))
        return str(int(m.hexdigest(), 16))[0:12]

    def to_json(self):
        return json.dumps(self.to_dict(), sort_keys=True, indent=4)

    def to_dict(self):
        venue = {k: v for k, v in self.__dict__.items()}
        return venue

    @staticmethod
    def from_dict(dct):
        return Venue(**dct)

    @staticmethod
    def from_json(json_str):
        json_dict = json.loads(json_str)
        return Venue.from_dict(json_dict)

    def persist(self):
        return self.__persist_state()

    def __persist_state(self):
        store_manager.StoreManager.put_venue_details(self.venue_id, self)


class VenueUISchema(BaseSchema):

    class Meta:
        additional = ("venue_id", "name", "city", "type")

    @post_load
    def make_object(self, data):
        return Venue(**data)
