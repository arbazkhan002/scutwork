import json
from core_utils.schema_utils import BaseSchema
from marshmallow.decorators import post_load


class RunDetails:

    def __init__(self, batruns_scored=0, is4=0, is6=0, is_nb=False,
                 is_wb=False, wide_runs=0, byes_runs=0):
        self.batruns_scored = batruns_scored
        self.is4 = is4
        self.is6 = is6
        self.is_nb = False if not is_nb else is_nb
        self.wide_runs = wide_runs if wide_runs else 0
        self.byes_runs = byes_runs if byes_runs else 0

    def is_wb(self):
        return self.wide_runs > 0

    def get_wide_runs(self):
        return self.wide_runs

    def get_byes_runs(self):
        return self.byes_runs

    def get_batruns_scored(self):
        return self.batruns_scored

    def get_totalruns_scored(self):
        runs = self.batruns_scored + self.wide_runs + self.byes_runs
        if (self.is_nb):
            runs += 1
        return runs

    def should_switch_batsman(self):
        if self.batruns_scored > 0:
            return self.batruns_scored % 2 == 1
        elif self.wide_runs > 0:
            # wide_runs is 1wd + runs taken
            return self.wide_runs % 2 == 0
        elif self.byes_runs > 0:
            return self.byes_runs % 2 == 1
        else:
            return False

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    @staticmethod
    def run_details_from_map(attribution_fields):
        return RunDetails(**attribution_fields)

    @staticmethod
    def run_details_from_json(dct):
        return RunDetails.run_details_from_map(json.loads(dct))


class RunDetailsSchema(BaseSchema):

    @post_load
    def make_object(self, data):
        return RunDetails(**data)

    class Meta:
        fields = ("batruns_scored",
                  "is4",
                  "is6",
                  "is_nb",
                  "wide_runs",
                  "byes_runs")
