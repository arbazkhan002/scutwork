import match_details
import ball_details
import run_details
import player
import dismissal
import random
import json
from store_manager import StoreManager


class SampleMatch:

    def __init__(self, match_id, team1_id, team2_id, max_overs):
        self.create_players(team1_id, team2_id)
        self.match_id = match_id
        self.match = match_details.MatchDetails(match_id, team1_id, team2_id, max_overs)
        match = self.match
        self.team1_id = team1_id
        self.team2_id = team2_id
        for team, player in self.__create_players_for_teams(team1_id, team2_id):
            match.add_player_to_team(team, player)
        self.sample_legal_deliveries = self.__createSampleLegalDeliveries()

    def create_players(self, team1_id, team2_id):
        for team_id, player_id in self.__create_players_for_teams(team1_id, team2_id):
            StoreManager.put_player_info(
                player.PlayerInfo(player_id, player_id, team_id))

    def __create_players_for_teams(self, team1_id, team2_id):
        players = []
        for team in [team1_id, team2_id]:
            for player in range(int(team) * 1000 + 1, int(team) * 1000 + 12):
                players.append((team, str(player)))
        return players

    def toss(self):
        teams = [self.team1_id, self.team2_id]
        toss_won_by = random.choice(teams)
        batting_first = random.choice(teams)
        self.match.add_toss_details(toss_won_by, batting_first)
        self.batting_team = batting_first
        if batting_first == self.team1_id:
            self.bowling_team = self.team2_id
        else:
            self.bowling_team = self.team1_id
        return (self.batting_team, self.bowling_team)

    def start_innings(self, innings_id, batting_team, bowling_team):
        self.batting_team = batting_team
        self.bowling_team = bowling_team
        self.inning_id = innings_id
        self.currentInning = self.match.start_innings(
            innings_id, batting_team, bowling_team)
        self.currentInning.new_batsman(str(int(batting_team) * 1000 + 1), False)
        self.currentInning.new_batsman(str(int(batting_team) * 1000 + 2), True)
        self.current_striker = str(int(batting_team) * 1000 + 2)
        self.non_striker = str(int(batting_team) * 1000 + 1)
        self.nextBatsman = str(int(batting_team) * 1000 + 3)

    def __switch_striker(self):
        self.current_striker, self.non_striker = self.non_striker, self.current_striker

    def __update_inning(self):
        self.currentInning = StoreManager.get_inning_detail(
            self.match_id, self.inning_id)

    def playRandomOver(self, over_number):
        over_balls = self.__getOverList()
        inning_id = self.inning_id
        bowler_id = (int(self.bowling_team) * 1000) + random.randint(1, 11)
        self.__update_inning()
        self.currentInning.new_bowler(str(bowler_id), over_number)
        self.__update_inning()
        ball_number = 1
        for run, dismissal_details in over_balls:
            print("Ball : " + str(over_number) + "." + str(ball_number))
            print("Striker : " + self.current_striker)
            print("Non-striker : " + self.non_striker)
            print(run.to_json())
            new_dismissal = None
            if dismissal_details is not None:
                new_dismissal = dismissal.Dismissal(
                    dismissal_details.get_dismissal_type, self.current_striker, False, str(bowler_id - 4))
                print(new_dismissal.to_json())
            current_ball = ball_details.BallDetails(over_number, ball_number, run, new_dismissal,
                                                    striker=self.current_striker, non_striker=self.non_striker, bowler=str(bowler_id))
            ball_number += 1
            self.currentInning.update_score(current_ball)
            if new_dismissal is not None:
                print("Setting new batsman")
                self.currentInning.new_batsman(self.nextBatsman, True)
                self.current_striker = self.nextBatsman
                self.nextBatsman = str(int(self.nextBatsman) + 1)
            elif run.should_switch_batsman():
                self.__switch_striker()
            self.__update_inning()
            print(self.currentInning.match_state.to_json())
        print(self.currentInning.to_json())
        print(StoreManager.get_player_details(
            self.match_id, self.inning_id, self.current_striker).to_json())
        if int(self.current_striker) - int(self.batting_team) * 1000 != 1:
            print(StoreManager.get_player_details(
                self.match_id, self.inning_id, str(int(self.current_striker) - 1)).to_json())
        self.__switch_striker()

    def __getOverList(self):
        ball = ["wide", "no_ball"]
        for i in range(0, 9):
            ball.append("legal")
        runs = [0, 1, 2, 3, 4, 5, 6]
        legal_delivery_count = 0
        deliveries = []
        over_deliveries = []

        while legal_delivery_count != ball_details.BallDetails.NUMBER_OF_BALLS_IN_OVER:
            deliveries_left = ball_details.BallDetails.NUMBER_OF_BALLS_IN_OVER - \
                legal_delivery_count
            deliveries.append(random.choice(ball))
            legal_delivery_count = deliveries.count("legal")

        for ball in deliveries:
            if (ball == "legal"):
                over_deliveries.append(random.choice(self.sample_legal_deliveries))
            elif (ball == "wide"):
                over_deliveries.append(self.__createSampleWideDelivery())
            elif (ball == "no_ball"):
                over_deliveries.append(self.__createSampleNoBbDelivery())

        return over_deliveries

    def __createSampleLegalDeliveries(self):
        event = ["wicket"]
        for i in range(0, 9):
            event.append("run")
        runs = []
        for i in range(0, 6):
            for j in range(0, 7 - i):
                runs.append(i)
        legal_delivery = []

        for i in range(0, 1000):
            ball_details = random.choice(event)
            no_runs = random.choice(runs)
            run = run_details.RunDetails(batruns_scored=no_runs)
            if (ball_details == "wicket"):
                legal_delivery.append(self.__createSampleDismissalWithRuns(run))
            else:
                legal_delivery.append((run, None))

        return legal_delivery

    def __createSampleDismissalWithRuns(self, run):
        if run.get_batruns_scored() == 0:
            dismissal_type = random.choice(list(dismissal.DismissalType))
        else:
            dismissal_type = dismissal.DismissalType.RUN_OUT
        return (run, dismissal.Dismissal(dismissal_type, random.randint(1, 11)))

    def __createSampleWideDelivery(self):
        run = run_details.RunDetails(is_wb=True)
        return (run, None)

    def __createSampleNoBbDelivery(self):
        run = run_details.RunDetails(is_nb=True)
        return (run, None)

match = SampleMatch('1', '1', '2', 5)
(batting_team, bowling_team) = match.toss()
match.start_innings(1, batting_team, bowling_team)
match.playRandomOver(0)
print("Over DONE")
match.playRandomOver(1)
print("Over DONE")
match.playRandomOver(2)
print("Over DONE")
match.playRandomOver(3)
print("Over DONE")
match.playRandomOver(4)
print("Over DONE")
match.start_innings(2, bowling_team, batting_team)
match.playRandomOver(0)
print("Over DONE")
match.playRandomOver(1)
print("Over DONE")
match.playRandomOver(2)
print("Over DONE")
match.playRandomOver(3)
print("Over DONE")
match.playRandomOver(4)
print(match.match.to_json())
