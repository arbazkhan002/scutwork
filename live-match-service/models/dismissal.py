"""
Dismissial is the information related to the dismissal of the batsman.
This class will contain all the different styles of dismissal and the 
fields associated with the dismissal.
"""
import uuid
import json
from enum import Enum
from core_utils.schema_utils import BaseSchema
from marshmallow import fields
from marshmallow.decorators import post_load
from models import store_manager


class DismissalType(Enum):
    CAUGHT = 1
    CAUGHT_BEHIND = 2
    CAUGHT_AND_BOWLED = 3
    BOWLED = 4
    HIT_WICKET = 5
    STUMPED = 6
    RUN_OUT = 7
    RETIRED = 8


class Dismissal:

    def __init__(self, dismissal_type, batsman_dismissed, bowler,
                 strike_changed=False, fielder=None):
        self.dismissal_type = dismissal_type
        self.batsman_dismissed = batsman_dismissed
        self.strike_changed = strike_changed
        self.fielder = fielder
        self.bowler = bowler

    def get_dismissal_type(self):
        return self.dismissal_type

    def get_batsman_dismissed(self):
        return self.batsman_dismissed

    def get_strike_changed(self):
        return self.strike_changed

    def get_fielder(self):
        return self.fielder

    def get_bowler(self):
        return self.bowler

    def is_retired(self):
        return True if self.dismissal_type == DismissalType.RETIRED else False

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    @staticmethod
    def dismissal_from_map(attribution_fields):
        if attribution_fields is None:
            return None
        return Dismissal(**attribution_fields)

    @staticmethod
    def dismissal_from_json(dct):
        return Dismissal.dismissal_from_map(json.loads(dct))

    def __eq__(self, other):
        if isinstance(self, other.__class__):
            return self.dismissal_type == other.dismissal_type and self.fielder == other.fielder and self.batsman_dismissed == other.batsman_dismissed and self.strike_changed == other.strike_changed
        return False


class DismissalInfoSchema(BaseSchema):
    fielder_name = fields.Method("get_fielder_name", dump_only=True)
    bowler_name = fields.Method("get_bowler_name", dump_only=True)

    def get_player_name(self, player_id):
        player_info = store_manager.StoreManager.get_player_info(player_id)
        return player_info.get_user_name()

    def get_fielder_name(self, obj):
        return self.get_player_name(obj.fielder)

    def get_bowler_name(self, obj):
        return self.get_player_name(obj.bowler)

    @post_load
    def make_object(self, data):
        return Dismissal(**data)

    class Meta:
        additional = ("dismissal_type",
                      "batsman_dismissed",
                      "strike_changed",
                      "fielder",
                      "bowler")
