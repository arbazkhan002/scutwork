import json
import logging
import pdb


from models import ball_details, dismissal
from marshmallow.schema import Schema
from marshmallow.fields import Nested
from models.ball_details import BallDetailsUISchema
from models.dismissal import DismissalInfoSchema
from marshmallow import fields
logger = logging.getLogger(__name__)

__author__ = 'arbaz'


class BowlerDetails:

    def __init__(self, user_id):
        self.user_id = user_id
        self.runs = 0
        self.wides = 0
        self.nbs = 0
        self.maidens = 0
        self.dismissals = []
        self.over_balls = dict()
        self.over = 0
        self.legal_balls_count = 0
        self.dot_balls_count = 0
        self.power_play_bowled_at = None

    def add_ball(self, ball):
        assert self.legal_balls_count < ball_details.BallDetails.NUMBER_OF_BALLS_IN_OVER
        self.over_balls.setdefault(ball.get_over_number(), []).append(ball)
        run_scored = ball.get_run_info().get_batruns_scored()
        self.runs += run_scored
        if (ball.get_run_info().is_wb()):
            wide_runs = ball.get_run_info().get_wide_runs()
            self.runs += wide_runs
            self.wides += wide_runs
        elif (ball.get_run_info().is_nb):
            self.nbs += 1
            self.runs += 1
        else:
            self.legal_balls_count += 1
            self.dot_balls_count += 1 if not run_scored else 0

        dismissal_details = ball.get_dismissal_info_details()
        if dismissal_details is not None and not dismissal_details.is_retired():
            self.dismissals.append(ball.get_dismissal_info_details())

        if ball.is_power_play:
            self.power_play_bowled_at = ball.get_over_number()

        if self.legal_balls_count == ball_details.BallDetails.NUMBER_OF_BALLS_IN_OVER:
            self.complete_over(ball.get_over_number())

    def complete_over(self, over_number):
        self.legal_balls_count = 0
        self.over += 1
        runs_over = sum(item.get_totalruns_scored()
                        for item in self.over_balls[over_number])
        if runs_over == 0:
            self.maidens += 1

    def __undo_complete_over(self, over_number):
        self.legal_balls_count = ball_details.BallDetails.NUMBER_OF_BALLS_IN_OVER
        self.over -= 1
        runs_over = sum(item.get_totalruns_scored()
                        for item in self.over_balls[over_number])
        if runs_over == 0:
            self.maidens -= 1

    def undo_ball(self, ball):
        saved_ball = self.over_balls[ball.get_over_number()][-1]
        assert saved_ball == ball, ("expecting ball: {}, \n\nfound:{}".format(
            saved_ball.to_json(), ball.to_json()))

        if self.legal_balls_count == 0:
            self.__undo_complete_over(ball.get_over_number())

        saved_ball = self.over_balls[ball.get_over_number()].pop()
        run_scored = ball.get_run_info().get_batruns_scored()
        self.runs -= run_scored
        if (ball.get_run_info().is_wb()):
            wide_runs = ball.get_run_info().get_wide_runs()
            self.runs -= wide_runs
            self.wides -= wide_runs
        elif (ball.get_run_info().is_nb):
            self.nbs -= 1
            self.runs -= 1
        else:
            self.legal_balls_count -= 1
            self.dot_balls_count -= 1 if not run_scored else 0

        dismissal_details = ball.get_dismissal_info_details()
        if dismissal_details is not None and not dismissal_details.is_retired():
            saved_dismissal = self.dismissals.pop()
            assert saved_dismissal == dismissal_details

        if ball.is_power_play and self.legal_balls_count == 0:
            self.power_play_bowled_at = None

    def get_over_count(self):
        return self.over

    def get_over_details(self, over_number):
        if over_number not in self.over_balls:
            return None
        over_balls = self.over_balls[over_number]
        runs = sum(b.get_totalruns_scored() for b in over_balls)
        wickets = sum(1 for b in over_balls
                      if b.get_dismissal_info_details() is not None)
        return (runs, wickets)

    def get_ball_count(self):
        return self.legal_balls_count

    def get_short_figures(self):
        legal = self.legal_balls_count
        over = str(self.over) + (".{}".format(legal) if legal > 0 else "")
        return "-".join(list(map(str, [over, self.maidens,
                                       self.runs, len(self.dismissals)])))

    def set_values(self, runs, wides, nbs, maidens, dismissals, over_balls, over, legal_balls_count,
                   power_play_bowled_at, dot_balls_count=None):
        self.runs = runs
        self.wides = wides
        self.nbs = nbs
        self.maidens = maidens
        self.dismissals = dismissals
        self.over_balls = over_balls
        self.over = over
        self.legal_balls_count = legal_balls_count
        self.power_play_bowled_at = power_play_bowled_at
        self.dot_balls_count = dot_balls_count

    def get_bowling_figures(self):
        is_over_running = 0 < self.legal_balls_count < 6
        if self.over == 0 and self.legal_balls_count == 0:
            return None

        over = str(self.over)
        wickets = str(len(self.dismissals))
        maidens = str(self.maidens)
        runs = str(self.runs)
        legal_balls_count = str(self.legal_balls_count)

        return "-".join([over + (("." + legal_balls_count) if is_over_running else ""),
                         maidens, runs, wickets])

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def to_dict(self):
        return {k: v for k, v in self.__dict__.items()}

    @staticmethod
    def bowling_details_from_map(attribution_fields):
        bowl_dismissals = []
        over_balls = dict()
        if "dismissals" in attribution_fields:
            all_dismissals = attribution_fields.pop("dismissals", [])
            for bat_dismissal in all_dismissals:
                bowl_dismissals.append(
                    dismissal.Dismissal.dismissal_from_map(bat_dismissal))
        if "over_balls" in attribution_fields:
            saved_balls = attribution_fields.pop("over_balls", dict())
            for over_number in saved_balls.keys():
                over_balls[int(over_number)] = []
                for ball in saved_balls[over_number]:
                    over_balls[int(over_number)].append(
                        ball_details.BallDetails.ball_from_map(ball))
        ball_info = BowlerDetails(user_id=attribution_fields.pop("user_id"))
        ball_info.set_values(
            **attribution_fields, dismissals=bowl_dismissals, over_balls=over_balls)
        return ball_info


class BowlerDetailsUISchema(Schema):
    dismissals = fields.Function(lambda obj: len(obj.dismissals))
    bowling_figures = fields.Function(lambda obj: obj.get_bowling_figures())
    dot_balls_count = fields.Int(missing=None)

    class Meta:
        additional = ("user_id",
                      "runs",
                      "wides",
                      "nbs",
                      "maidens",
                      "over",
                      "legal_balls_count")
