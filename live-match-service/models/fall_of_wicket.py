import json

from core_utils.schema_utils import BaseSchema
from models import store_manager

from marshmallow import post_load, fields
from models import store_manager
from models.player_info import PlayerInfoUISchema
import pdb


__author__ = 'arbaz'


class FallOfWicket:

    def __init__(self, score, over, ball_number, batsman_id, timestamp=None):
        self.__batsman_id = batsman_id
        self.__score = score
        self.__over = over
        self.__ball_number = ball_number
        self.__timestamp = timestamp

    def get_batsman_id(self):
        return self.__batsman_id

    def get_score(self):
        return self.__score

    def get_over(self):
        return self.__over

    def get_ball_number(self):
        return self.__ball_number

    def set_batsman_id(self, value):
        self.__batsman_id = value

    def set_score(self, value):
        self.__score = value

    def set_over(self, value):
        self.__over = value

    def set_ball_number(self, value):
        self.__ball_number = value

    def del_batsman_id(self):
        del self.__batsman_id

    def del_score(self):
        del self.__score

    def del_over(self):
        del self.__over

    def del_ball_number(self):
        del self.__ball_number

    def get_timestamp(self):
        return self.__timestamp

    def to_json(self):
        return json.dumps(self.to_dict(), sort_keys=True, indent=4)

    def to_dict(self):
        schema = FallOfWicketUISchema()
        dct = schema.dump(self).data
        dct["batsman_id"] = dct["batsman"]["player_id"]
        del dct["batsman"]
        return dct

    @staticmethod
    def from_dict(dct):
        return FallOfWicket(**dct)

    @staticmethod
    def from_json(json_str):
        json_dict = json.loads(json_str)
        return FallOfWicket.from_dict(json_dict)
    batsman_id = property(
        get_batsman_id, set_batsman_id, del_batsman_id, "batsman_id's docstring")
    score = property(get_score, set_score, del_score, "score's docstring")
    over = property(get_over, set_over, del_over, "over's docstring")
    ball_number = property(
        get_ball_number, set_ball_number, del_ball_number, "ball_number's docstring")
    timestamp = property(get_timestamp, "read only timestamp integer")


class FallOfWicketUISchema(BaseSchema):
    batsman = fields.Function(
        lambda x: store_manager.StoreManager.get_player_info(x.batsman_id).to_dict(),
        deserialize=lambda d: d)

    class Meta:
        additional = ("score", "over", "ball_number", "timestamp")

    @post_load
    def make_object(self, data):
        data["batsman_id"] = data["batsman"]["player_id"]
        del data["batsman"]
        return FallOfWicket(**data)
