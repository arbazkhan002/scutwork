import json
from models import store_manager, match_details
from models import player, ball_details
from models import match_state
import inspect
from core_utils.schema_utils import BaseSchema
from core_utils.dict_utils import map_keys
from core_utils import string_utils
from marshmallow import fields, pre_load, pre_dump
from marshmallow.decorators import post_dump
import logging
import pdb
from models.fall_of_wicket import FallOfWicket, FallOfWicketUISchema
from models.team_details import TeamDetailsUISchema
from models.match_state import MatchStateUISchema

__author__ = 'arbaz'

logger = logging.getLogger(__name__)


class InningsEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, ball_details.BallDetails):
            return obj.to_json()
        elif isinstance(obj, player.Player):
            return obj.to_json()
        return json.JSONEncoder.default(self, obj)


class InningDetails:

    def __init__(self, match_id, inning_id, batting_team, bowling_team, max_overs,
                 players_batting_team, players_bowling_team,
                 match_state=None, curr_ball_sequence=0,
                 batsman_ids=None, bowler_ids=None, fow=None,
                 target_score=None, pp=None):
        self.match_id = match_id
        self.inning_id = inning_id
        self.bowling_team = bowling_team
        self.batting_team = batting_team
        self.players_batting_team = players_batting_team
        self.players_bowling_team = players_bowling_team
        self.max_overs = max_overs
        self.target_score = target_score
        # Batting order
        self.batsman_ids = batsman_ids if batsman_ids is not None else []

        # Bowling order by over
        self.bowler_ids = bowler_ids if bowler_ids is not None else []

        # List of tuples (position, score): [(1, 20), (2, 22), ... ]
        self.fow = fow if fow is not None else []
        self.curr_ball_sequence = curr_ball_sequence

        self.pp = pp if pp is not None else []
        self.__match_state = None

        self.__persist_state()

    @property
    def match_state(self):
        return self.__match_state

    @match_state.setter
    def match_state(self, new_state):
        "Initializes match_state"
        self.__match_state = new_state

    def update_match_state(self, new_state):
        """Replaces match state
        TODO: More book-keeping can be done when state is updated
        """
        self.__update_score(prev_state=self.match_state, new_state=new_state)
        self.match_state = new_state
        self.curr_ball_sequence = new_state.ball_sequence
        self.__persist_state()
        self.update_match_progress()

    def undo_match_state(self, prev_state):
        """
        Revert match state back to the state provided
        """
        self.__undo_score(prev_state=prev_state, current_state=self.match_state)
        self.match_state = prev_state
        if prev_state is None:
            self.curr_ball_sequence = 0
        else:
            self.curr_ball_sequence = prev_state.ball_sequence
        self.__persist_state()

    def initialize_match_state_players(self, striker_id, non_striker_id, bowler_id):
        if not self.__match_state.player_striker:
            self.__match_state.new_batsman(striker_id, on_strike=True)
            self.__ingest_batsman(striker_id)
        if not self.__match_state.player_non_striker:
            self.__match_state.new_batsman(non_striker_id, on_strike=False)
            self.__ingest_batsman(non_striker_id)
        if not self.__match_state.bowler:
            self.__match_state.new_bowler(bowler_id)
            self.__ingest_bowler(bowler_id)

    def __ingest_batsman(self, batsman):
        if not batsman:
            return
        batsman_id = batsman.get_user_id()
        if batsman_id is not None and batsman_id not in self.batsman_ids:
            # TODO: Keep in mind about retired batsman coming to play again
            self.batsman_ids.append(batsman_id)

    def __undo_ingest_batsman(self, batsman):
        if not batsman:
            return
        batsman_id = batsman.get_user_id()
        self.batsman_ids.remove(batsman_id)

    def __ingest_bowler(self, bowler):
        if not bowler:
            return
        bowler_id = bowler.get_user_id()
        if bowler_id not in self.bowler_ids[-1:]:
            self.bowler_ids.append(bowler_id)

    def __undo_ingest_bowler(self, bowler):
        if not bowler:
            return
        bowler_id = bowler.get_user_id()
        logger.debug("bowler_ids:{}".format(self.bowler_ids))
        assert bowler_id in self.bowler_ids[-1:],\
            ("Last bowler expected to be same. Got: {}".format(self.bowler_ids))
        del self.bowler_ids[-1]

    def __ingest_batsmen_using_state(self, at_match_state):
        striker = at_match_state.player_striker
        non_striker = at_match_state.player_non_striker
        last_dismissed_batsman = at_match_state.last_dismissed_batsman
        batting_order = [striker, non_striker]
        batting_order = (reversed(batting_order)
                         if at_match_state.is_strike_switched
                         else batting_order)
        batting_order = map(lambda x: last_dismissed_batsman if not x else x,
                            batting_order)
        for b in batting_order:
            self.__ingest_batsman(b)

    def set_match_end(self, status):
        """API for ending the match"""
        self.match_state.set_match_end(status)
        self.match_state.persist()

    def is_inning_end(self):
        """Inning progress is saved in `match_state`"""
        if self.match_state is not None:
            return self.match_state.is_inning_end()
        else:
            return False

    def is_match_end(self):
        """Inning progress is saved in `match_state`"""
        if self.match_state is not None:
            return self.match_state.is_match_end()
        else:
            return False

    def update_match_progress(self):
        """Updating match progress should be done after 
        self is persisted.
        """
        match = store_manager.StoreManager.get_match_details(self.match_id)
        if match.is_match_end():
            self.set_match_end(True)

    def __update_score(self, prev_state, new_state):
        bowler = new_state.player_current_bowler
        striker = new_state.player_striker
        non_striker = new_state.player_non_striker
        self.__ingest_batsmen_using_state(at_match_state=new_state)
        self.__ingest_bowler(bowler)
        if striker is None or non_striker is None:
            # i.e. wicket fall
            assert striker is not None or non_striker is not None, (
                "Both batsmen can not be `None` on `update_score` i.e. after a match ball")
            last_dismissed_batsman = new_state.last_dismissed_batsman
            batsman_position = self.batsman_ids.index(
                last_dismissed_batsman.get_user_id()) + 1
            ball_detail = store_manager.StoreManager.get_match_ball(new_state.match_id,
                                                                    new_state.inning_id,
                                                                    new_state.ball_sequence)
            self.fow.append(FallOfWicket(score=new_state.get_score(),
                                         over=new_state.over_number,
                                         ball_number=new_state.ball_number,
                                         batsman_id=last_dismissed_batsman.get_user_id(),
                                         timestamp=ball_detail.timestamp))
        self.__persist_state()

    def __undo_score(self, prev_state, current_state):
        bowler = current_state.player_current_bowler
        striker = current_state.player_striker
        non_striker = current_state.player_non_striker
        over_bowled = current_state.over_number
        # Undo any new batsman if added in the current ball. If its the first
        # ball, clear all the batsmen_ids
        if prev_state is None or prev_state.is_empty():
            batsmen_ids = []
        else:
            if striker is None or non_striker is None:
                # Undo wicket fall if it happened
                # NOTE: striker/non_striker can be None if its
                # just before 1st ball of second innings
                if len(self.fow) > 0:
                    self.fow.pop()

            new_batsmen = []
            old_batsmen = [x.get_user_id() for x in
                           [prev_state.player_striker, prev_state.player_non_striker]
                           if x]

            current_batsmen = [x for x in [striker, non_striker] if x]
            for batsman in current_batsmen:
                if batsman.get_user_id() not in old_batsmen:
                    self.__undo_ingest_batsman(batsman)

        # TODO Undo bowler if bowl is the first of the over.
        if current_state.ball_sequence % ball_details.BallDetails.NUMBER_OF_BALLS_IN_OVER == 1:
            self.__undo_ingest_bowler(bowler)

    def set_inning_end(self, status):
        self.match_state.set_inning_end(status)

    def __persist_state(self):
        store_manager.StoreManager.put_inning_details(
            self.match_id, self.inning_id, self)

    def to_json(self):
        # Get every attribute except private variables
        inning_details = {
            k: v for k, v in self.__dict__.items() if not k.startswith('_' + InningDetails.__name__)}
        inning_details["fow"] = [x.to_dict() for x in inning_details["fow"]]
        return json.dumps(inning_details, sort_keys=True, indent=4)

    @staticmethod
    def inning_detail_from_json(dct):
        attribution_fields = json.loads(dct)
        attribution_fields['fow'] = [
            FallOfWicket.from_dict(x) for x in attribution_fields['fow']]
        inning_details_object = InningDetails(**attribution_fields)
        match_state_object = store_manager.StoreManager.get_match_state(
            inning_details_object.match_id, inning_details_object.inning_id,
            inning_details_object.curr_ball_sequence)
        inning_details_object.match_state = match_state_object
        return inning_details_object


class InningDetailsUISchema(BaseSchema):
    """
    Returns the entire information for each batsman and bowler for this innings.
    This is ideally going to be used to generate the scorecard for a given game.
    """
    score = fields.Function(lambda x: x.match_state.score)
    wides = fields.Function(lambda x: x.match_state.wides)
    byes = fields.Function(lambda x: x.match_state.byes)
    nbs = fields.Function(lambda x: x.match_state.nbs)
    wickets = fields.Function(lambda x: x.match_state.wickets)
    overs = fields.Function(lambda x: x.match_state.over_number)
    ball_number = fields.Function(lambda x: x.match_state.ball_number)
    batsman_ids = fields.List(fields.Str)
    bowler_ids = fields.List(fields.Str)
    power_play_details = fields.Method("get_power_play_details")
    batting_team = fields.Method("make_batting_team",
                                 deserialize=lambda data: data["team_id"])
    bowling_team = fields.Method("make_bowling_team",
                                 deserialize=lambda data: data["team_id"])
    batsmen_to_bat = fields.Method("get_batsmen_to_bat")
    fow = fields.Nested(FallOfWicketUISchema, many=True)

    # TODO : Handle pp
    def make_team(self, team_id):
        team_obj = store_manager.StoreManager.get_team_details(team_id)
        team_dict = TeamDetailsUISchema().dump(team_obj).data
        del team_dict["roster"]
        return team_dict

    def make_batting_team(self, obj):
        return self.make_team(obj.batting_team)

    def make_bowling_team(self, obj):
        return self.make_team(obj.bowling_team)

    def get_innings_score(self, obj):
        return obj.match_state.score

    def get_innings_wickets(self, obj):
        return obj.match_state.wickets

    def get_power_play_details(self, obj):
        schema = MatchStateUISchema(only=("power_play_details",))
        match_state_data = schema.dump(obj.match_state).data
        return match_state_data["power_play_details"]

    def get_batsmen_to_bat(self, obj):
        schema = MatchStateUISchema(only=("batsmen_to_bat",))
        match_state_data = schema.dump(obj.match_state).data
        return match_state_data["batsmen_to_bat"]

    @post_dump
    def merge_player_data(self, data):

        match_id = data.get("match_id", None)
        inning_id = data.get("inning_id", None)
        batsmen = data.get("batsman_ids", [])
        data["batsman_details"] = []
        for batsman in batsmen:
            batsman_full = store_manager.StoreManager.get_player_details(
                match_id, inning_id, batsman)
            # TODO : Move this to get_player_or_new once merged with Arbaz change.
            if batsman_full is None:
                player_info = store_manager.StoreManager.get_player_info(batsman)
                batsman_full = player.Player(player_info)
            batsman_json = player.PlayerUISchema().dump(batsman_full)
            data["batsman_details"].append(
                {**batsman_json.data["batting_details"], **batsman_json.data["player_info"]})
        # Get list of all bowlers in the same order as they bowled. Remove duplicates
        bowlers = list(dict.fromkeys(data.get("bowler_ids", [])))
        data["bowler_details"] = []
        for bowler in bowlers:
            bowler_full = store_manager.StoreManager.get_player_details(
                match_id, inning_id, bowler)
            assert bowler_full is not None, "Cannot have a bowler with no details"
            bowler_json = player.PlayerUISchema().dump(bowler_full)
            data["bowler_details"].append(
                {**bowler_json.data["bowling_details"], **bowler_json.data["player_info"]})

    class Meta:
        additional = (
            "match_id",
            "inning_id",
            "max_overs",
        )
