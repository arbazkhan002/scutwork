import requests
import json
import logging
import re
import pdb

from marshmallow import fields
from core_utils.schema_utils import BaseSchema
from marshmallow import pre_load, post_load
from models.venue import Venue
from models.player_info import PlayerInfo
from core_utils.string_utils import to_snake_case
from core_utils.views_utils import to_object_format
from json.decoder import JSONDecodeError
from models.team_details import TeamDetailsUISchema, TeamDetails

__author__ = 'arbaz'

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class CricbayApi:

    DEFAULT_PARAMS = {'appKey': 'C8221BF9-0896-4BCF-9C11-B0D67E61CA3A'}

    ALL_MATCHES_URL = "https://www.cricbay.com/getMatchesjson.asp"
    MATCH_PLAYERS_URL = "https://www.cricbay.com/getPlayersjson.asp"
    SCORER_LOGIN_URL = "https://www.cricbay.com/authUserjson.asp"

    class AllMatchesSchema(BaseSchema):
        match_id = fields.String(load_from='match_id')
        match_date = fields.String(load_from='match_date')
        match_time = fields.String(load_from='match_time')
        team1_id = fields.String(load_from='home_team_id')
        team2_id = fields.String(load_from='away_team_id')
        umpiring_team_1 = fields.String(load_from='umpire1_team_id')
        umpiring_team_2 = fields.String(load_from='umpire2_team_id')
        game_group = fields.String(load_from='game_group')
        venue = fields.String(load_from='ground_name')

        @pre_load
        def convert_to_strings(self, data):
            return {k: str(v) for k, v in data.items()}

    class VenueSchema(BaseSchema):
        city = fields.String()
        type = fields.String(load_from="ground_type")
        name = fields.String(load_from="ground_name")

        @pre_load
        def convert_to_strings(self, data):
            return {k: str(v) for k, v in data.items()}

        @post_load
        def make_object(self, data):
            return Venue(**data)

    class MatchesToTeamsSchema(BaseSchema):
        teams = fields.Nested(TeamDetailsUISchema, many=True)

        @pre_load
        def extract_teams(self, data):
            teams = []
            for m in data["matches"]:
                team = {}
                team["team_id"] = str(m["home_team_id"])
                team["team_name"] = m["home_team_name"]
                teams.append(team)
                team = {}
                team["team_id"] = str(m["away_team_id"])
                team["team_name"] = m["away_team_name"]
                teams.append(team)
                team = {}
                team["team_id"] = str(m[to_snake_case("umpire1TeamId")])
                team["team_name"] = m[to_snake_case("umpire1TeamName")]
                teams.append(team)
                team = {}
                team["team_id"] = str(m[to_snake_case("umpire2TeamId")])
                team["team_name"] = m[to_snake_case("umpire2TeamName")]
                teams.append(team)
            return {'teams': teams}

        @post_load
        def make_teams(self, data):
            return data["teams"]

    class MatchPlayersSchema(BaseSchema):

        class PlayerSchema(BaseSchema):
            """Ideally this schema could be defined as sibling class
            to MatchPlayersSchema but for some reason python
            wasn't able to refer to nested sibling class as
            CricbayApi.PlayerSchema or just PlayerSchema
            """
            team_id = fields.Int()
            player_id = fields.Int()
            name = fields.Str(load_from="player_name")

            @post_load
            def make_object(self, data):
                return PlayerInfo(**data)

        player_list = fields.Nested(PlayerSchema, many=True,
                                    load_from=to_snake_case("Player List"))

    class LoggedUser(BaseSchema):
        user_id = fields.Function(deserialize=lambda x: str(x))
        user_name = fields.Str()
        team_id = fields.Function(deserialize=lambda x: str(x))
        auth_token = fields.Str()

    class LoginRequestSchema(BaseSchema):
        email = fields.Str()
        appKey = fields.Str()
        pw = fields.Str(load_from="password")

    class LoginResponseSchema(BaseSchema):
        user = fields.Method(
            deserialize="get_user", load_from=to_snake_case("User Details"))

        def get_user(self, data):
            user_detail = data[0]
            return user_detail

        @post_load
        def extract_user(self, data):
            # Just a dummy schema check
            schema = CricbayApi.LoggedUser()
            user, error = schema.load(data['user'])
            if error:
                raise Exception(error)
            return user

    class DataPullObject(BaseSchema):

        def __init__(self, auth_token):
            self.auth_token = auth_token

    class DataPullSchema(BaseSchema):
        auth_token = fields.Str(load_from='remote_api_token')

        @post_load
        def make_object(self, data):
            return CricbayApi.DataPullObject(**data)


class ApiManager:
    api_stub = CricbayApi

    def _update_payload(payload):
        payload.update(ApiManager.api_stub.DEFAULT_PARAMS)

    @staticmethod
    def login(payload):
        """Returns dict if login is successful else returns error
        as string"""
        url = ApiManager.api_stub.SCORER_LOGIN_URL
        ApiManager._update_payload(payload)
        schema = ApiManager.api_stub.LoginRequestSchema()
        formatted_payload, errors = schema.load(payload)
        if errors:
            raise Exception("LoginRequestSchema Error:{}".format(errors))
        response = requests.get(url, params=formatted_payload)
        json_str = response.content.decode('utf-8')

        try:
            json_dct = json.loads(json_str)
        except JSONDecodeError:
            fixed_str = re.sub("authToken\":([a-zA-Z0-9-]+)", r'authToken":"\1"}',
                               json_str)

            # Return error as string if it is not a json-encoded user
            try:
                json_dct = json.loads(fixed_str)
            except JSONDecodeError:
                return json_str

        data = to_object_format(json_dct)
        schema = ApiManager.api_stub.LoginResponseSchema()
        dct, error = schema.load(data)
        return dct

    @staticmethod
    def get(url, **kwargs):
        params = {}
        ApiManager._update_payload(params)
        if 'params' in kwargs:
            kwargs['params'].update(params)
        else:
            kwargs['params'] = params
        response = requests.get(url,
                                **kwargs)
        # pdb.set_trace()

        try:
            data = response.json()
            return data
        except json.decoder.JSONDecodeError:
            logger.debug("response json wasn't invalid: "
                         "response-text:{}".format(response.content))

    @staticmethod
    def get_stub():
        return ApiManager.api_stub
