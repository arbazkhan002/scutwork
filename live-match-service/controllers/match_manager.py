from models.store_manager import StoreManager
from models.match_state import MatchState
from controllers import admin_manager
from pprint import pprint
from copy import deepcopy
import pdb
from models import store_manager
from models.match_over import MatchOverUISchema
import jwt
from config import Config, bcrypt
import datetime

__author__ = 'arbaz'

SIGNATURE_EXPIRED = 'Signature expired. Please log in again.'
INVALID_TOKEN = 'Invalid token. Please log in again.'
CANDIDATE_MATCH_ID_NOT_CORRECT = 'Found multiple match authorizations. Pass a candidate_match_id that fits'


def start_match(match_details_object):
    if match_details_object.is_setup() and len(match_details_object.innings_details) == 0:
        match_id = match_details_object.match_id
        batting_team = match_details_object.team_batting_first
        home_team = match_details_object.home_team
        bowling_team = (match_details_object.away_team
                        if batting_team == home_team else home_team)
        inning = match_details_object.start_innings(inning_id="1",
                                                    batting_team=batting_team,
                                                    bowling_team=bowling_team)
        match_state_object = MatchState.from_inning(inning).persist()
        return match_state_object


def start_innings(match_id, inning_id, batting_team, bowling_team):
    match = StoreManager.get_match_details(match_id)
    batting_team = match.home_team
    bowling_team = match.away_team
    inning = match.start_innings(inning_id, batting_team, bowling_team)
    return inning


def add_ball(ball):
    StoreManager.put_match_ball(ball.match_id, ball.inning_id, ball)
    match_state_before_ball = StoreManager.get_match_state(
        ball.match_id, ball.inning_id, ball.ball_sequence - 1)
    current_inning_detail = StoreManager.get_inning_detail(ball.match_id,
                                                           ball.inning_id)
    if match_state_before_ball is None:
        # Mostly first ball of an innings
        assert current_inning_detail is not None, ("Inning not yet started. Call `start_innings`"
                                                   "with the relevant details")
        match_state_object = MatchState.from_inning_and_ball(current_inning_detail,
                                                             ball)
        current_inning_detail.match_state = match_state_object
        match_state_before_ball = match_state_object.copy()
    match_state = match_state_before_ball.update_score(ball)
    current_inning_detail.update_match_state(match_state)
    return match_state


def force_match_end(match_id):
    match = get_match(match_id)
    if match is None:
        return None

    if len(match.innings_details) > 0:
        last_inning = match.innings_details[-1]
        last_inning.set_match_end(True)
        return last_inning.match_state


def force_inning_end(match_id, inning_id=None):
    if inning_id is None:
        match_state = get_current_match_state(match_id)
        if match_state is None:
            return None
        match_state.set_inning_end(True)
        return match_state

    inning = StoreManager.get_inning_detail(match_id, inning_id)

    if inning is None:
        return None

    inning.set_inning_end(True)
    return inning.match_state


def undo_ball(ball):
    match_state_at_ball = StoreManager.get_match_state(
        ball.match_id, ball.inning_id, ball.ball_sequence)
    current_inning_detail = StoreManager.get_inning_detail(ball.match_id,
                                                           ball.inning_id)
    # If it was the first ball of the inning, revert the inning
    match = get_match(ball.match_id)
    latest_inning_id = match.innings_details[-1].inning_id
    if int(latest_inning_id) > int(ball.inning_id):
        # this would happen when user undoes ball from last inning
        # when he has just started new inning
        # current inning needs to be deleted before we undo last ball
        # of previous inning
        StoreManager.delete_inning_details(match.match_id, latest_inning_id)
        match.innings_details.pop()
        match.persist()
    match_state_at_ball.undo_ball(ball)
    match_state_before_ball = StoreManager.get_match_state(
        ball.match_id, ball.inning_id, ball.ball_sequence - 1)
    current_inning_detail.undo_match_state(match_state_before_ball)
    return match_state_before_ball


def add_players_to_team(match_id, team_id, players):
    match = StoreManager.get_match_details(match_id)
    match.add_players_to_team(team_id, players)
    return match


def get_match(match_id):
    match = StoreManager.get_match_details(match_id)
    return match


def get_live_matches_for_team(team_id):
    """Get match that is being scored right now for team_id"""
    matches = StoreManager.get_all_matches()
    team_matches = []
    for m in matches:
        if team_id in [m.home_team, m.away_team]:
            team_matches.append(m)
    return team_matches


def get_current_match_state(match_id):
    match = get_match(match_id)
    if match is None:
        return None

    current_match_innings = (None if not match.innings_details
                             else match.innings_details[-1])
    if current_match_innings is None:
        return None

    current_match_state = current_match_innings.match_state
    return current_match_state


def make_next_innings(match_id):
    if get_current_match_state(match_id) is None:
        return None
    match = get_match(match_id)
    if match.innings_details[-1].is_inning_end():
        if not match.is_match_end():
            last_inning = match.innings_details[-1]
            inning = match.start_innings(inning_id=str(int(last_inning.inning_id) + 1),
                                         batting_team=last_inning.bowling_team,
                                         bowling_team=last_inning.batting_team)
            match_state_object = MatchState.from_inning(inning).persist()
            return match_state_object

        else:
            return None
    else:
        return None


def get_over_history(match_id, inning_id, over_till, num_overs):
    """
    Parameters
    __________
    `over_till`: int
        over_number at which to start history from (in reverse order)
        If `over_till` is None, `over_till` is treated as last over of inning
        with inning_id=`inning_id`
    `num_overs`: int
        number of overs to return history for

    Returns
    _______
    dict
        - over_history: list(match_over as dict)
        - next_scroll_at
            - over_till - argument for `get_over_history` for next scroll
            - inning_id - argument for `get_over_history` for next scroll
    """
    if inning_id is not None:
        inning = store_manager.StoreManager.get_inning_detail(match_id, inning_id)
    else:
        match = get_match(match_id)
        if len(match.innings_details) > 0:
            inning = match.innings_details[-1]
            inning_id = inning.inning_id
        else:
            return None
    over_history = []
    curr_inning_id = inning_id
    if inning is None:
        return None
    if over_till is None:
        over_till = inning.match_state.over_number
        # if last over in this inning is blank, omit it
        if inning.match_state.ball_number == 0:
            over_till = over_till - 1

    schema = MatchOverUISchema()
    # initialize ov for if "for loop" isn't executed
    ov = over_till
    if inning.inning_id == inning_id:
        for ov in range(over_till, max(-1, over_till - num_overs), -1):
            over_details = store_manager.StoreManager.get_match_over(
                match_id, inning_id, ov)
            if over_details:
                over_history.append(schema.dump(over_details).data)

    next_scroll_at = {
        "match_id": match_id,
        "inning_id": curr_inning_id,
        "over_till": ov - 1
    }

    if len(over_history) < num_overs:
        curr_inning_id = str(int(curr_inning_id) - 1)
        more_over_history = get_over_history(match_id, curr_inning_id,
                                             over_till=None,
                                             num_overs=num_overs - len(over_history))
        if more_over_history:
            over_history = over_history + more_over_history["over_history"]
            next_scroll_at = more_over_history["next_scroll_at"]

    return {
        "over_history": over_history,
        "next_scroll_at": next_scroll_at
    }


def end_inning(match_id, inning_id):
    if get_current_match_state(match_id) is None:
        return None
    match = get_match(match_id)
    if match.innings_details[-1].inning_id != inning_id:
        return None

    inning = match.innings_details[-1]
    inning.set_inning_end(True)
    return inning


def encode_auth_token(entity_id):
    """Generates the auth token for an entity_id"""

    payload = {
        'exp': datetime.datetime.utcnow() + datetime.timedelta(
            days=0, hours=3),
        'iat': datetime.datetime.utcnow(),
        'sub': entity_id
    }
    return jwt.encode(
        payload,
        Config.config.SECRET_KEY,
        algorithm='HS256'
    )


def decode_auth_token(auth_token):
    """
    Decodes the auth token for a match
    """
    try:
        payload = jwt.decode(
            auth_token, Config.config.SECRET_KEY)
        return payload['sub']
    except jwt.ExpiredSignatureError:
        return SIGNATURE_EXPIRED
    except jwt.InvalidTokenError:
        return INVALID_TOKEN


def decode_match_token(auth_token, candidate_match_id=None):
    """
    Decodes the auth token for a match
    In case of multiple match authorizations, 
    uses candidate_match_id to return corresponding match
    """
    match_id = decode_auth_token(auth_token)
    if match_id in [SIGNATURE_EXPIRED, INVALID_TOKEN]:
        return match_id
    else:
        all_matches = match_id.split(",")
        if len(all_matches) > 1 and candidate_match_id in all_matches:
            return get_match(candidate_match_id)
        if len(all_matches) == 1:
            return get_match(match_id)
        return CANDIDATE_MATCH_ID_NOT_CORRECT


def is_valid_credential(credential, credential_hash):
    return bcrypt.check_password_hash(credential_hash, credential)


def is_valid_scorer(logged_user, match):
    user_team = logged_user['team_id']
    home_team = match.home_team
    away_team = match.away_team
    return user_team in [home_team, away_team]
