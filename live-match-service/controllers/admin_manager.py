import pdb

from config import Config, bcrypt

from models import match_details
from models import player_info
from models import store_manager
from models.player import Player
from models.team_details import TeamDetails
from models.venue import Venue, VenueUISchema
from controllers.api_manager import ApiManager
from core_utils.views_utils import to_object_format
from models.store_manager import StoreManager
from controllers import match_manager
import warnings
from datetime import datetime


__author__ = 'arbaz'


def create_players_for_team(team_id):
    team = TeamDetails(team_id, team_name=str(int(team_id) * 1000))
    for player in range(int(team_id) * 1000 + 1, int(team_id) * 1000 + 12):
        player_info_dict = player_info.PlayerInfo(player_id=str(player),
                                                  name=str(player),
                                                  team_id=team_id)
        store_manager.StoreManager.put_player_info(player_info_dict)
        team.add_player(player_info_dict)
    return team


def create_player(player_info):
    player = Player(player_info)
    player.persist_info()
    team = StoreManager.get_team_details(player_info.team_id)
    if team is not None:
        team.add_player(player_info)
        team.persist()


def make_hash_for(match_pin):
    rounds = Config.config.BCRYPT_LOG_ROUNDS
    pin_hash = bcrypt.generate_password_hash(match_pin, rounds).decode()
    return pin_hash


def prepare_match_args(match_pin=None, venue=None, **kwargs):
    """Using raw arguments read for a match, prepare all arguments
    as required by MatchDetails constructor
    """
    pin_hash = make_hash_for(match_pin)
    venue_id = Venue.make_id(venue)
    venue_object = get_venue(venue_id)
    if venue_object is None:
        create_venue_simple(venue)
    kwargs.update({
        'match_pin': pin_hash,
        'venue': venue_id
    })
    return kwargs


def create_match(match_id, team1_id, team2_id, match_pin, umpiring_team_1,
                 umpiring_team_2, venue, match_date=None, match_time=None,
                 game_group=None, max_overs=20):
    """

    Parameters
    ______________
    venue: str
        Name of the venue
    """
    constructor_args = prepare_match_args(
        match_id=match_id,
        home_team=team1_id,
        away_team=team2_id,
        max_overs=max_overs,
        match_pin=match_pin,
        umpiring_team_1=umpiring_team_1,
        umpiring_team_2=umpiring_team_2,
        venue=venue,
        match_date=match_date,
        match_time=match_time,
        game_group=game_group)
    match = match_details.MatchDetails(**constructor_args)
    store_manager.StoreManager.put_in_match_store(match)
    return match


def create_venue_simple(venue_name):
    """
    Create venue when there is no other information apart from a name
    """
    venue_object = Venue(venue_name, city=None, type=None)
    venue_object.persist()
    return venue_object


def get_players_for_team(team_id):
    for player in range(int(team_id) * 1000 + 1, int(team_id) * 1000 + 12):
        yield str(player)


def get_player(player_id):
    player_object = store_manager.StoreManager.get_player_info(player_id)
    return player_object


def get_team(team_id):
    team_object = store_manager.StoreManager.get_team_details(team_id)
    return team_object


def get_venue(venue_id):
    venue_object = store_manager.StoreManager.get_venue_details(venue_id)
    return venue_object


def get_all_teams():
    teams = store_manager.StoreManager.get_all_teams()
    return teams


def get_all_matches(move_old_to_archive=True):
    matches = store_manager.StoreManager.get_all_matches()

    if move_old_to_archive:
        for m in matches:
            dt = m.get_datetime()
            if not dt:
                continue
            match_week = dt.isocalendar()[1]
            week_now = datetime.now().isocalendar()[1]
            if week_now > match_week:
                store_manager.StoreManager.archive_match(m.match_id)

    return matches


def get_all_venues():
    venues = store_manager.StoreManager.get_all_venues()
    return venues

    return data


def pull_teams(remote_auth_token, data=None):
    """
    Pull venue information from match details
    """
    api_stub = ApiManager.api_stub
    if data is None:
        matches_url = api_stub.ALL_MATCHES_URL
        data_json = ApiManager.get(matches_url,
                                   params={'authToken': remote_auth_token})
        data = to_object_format(data_json)

    schema = api_stub.MatchesToTeamsSchema()
    all_teams, errors = schema.load(data)

    if errors:
        warnings.warn("Error in pulling teams:{}".format(errors))
        return None

    for t in all_teams:
        t.persist()

    return all_teams


def pull_venues(remote_auth_token, data=None):
    """
    Pull venue information from match details
    """
    api_stub = ApiManager.api_stub
    if data is None:
        matches_url = api_stub.ALL_MATCHES_URL
        data_json = ApiManager.get(matches_url,
                                   params={'authToken': remote_auth_token})
        data = to_object_format(matches_data)

    schema = api_stub.VenueSchema(many=True)
    all_venues, errors = schema.load(data['matches'])

    if errors:
        warnings.warn("Error in pulling venues:{}".format(errors))
        return None

    for v in all_venues:
        v.persist()

    return all_venues


def pull_matches(remote_auth_token, match_id=None):
    api_stub = ApiManager.api_stub
    matches_url = api_stub.ALL_MATCHES_URL

    if match_id is not None:
        matches_data = ApiManager.get(matches_url,
                                      params={'authToken': remote_auth_token,
                                              'matchId': match_id})
    else:
        matches_data = ApiManager.get(matches_url,
                                      params={'authToken': remote_auth_token})

    if not matches_data:
        return matches_data

    matches_data_dct = to_object_format(matches_data)
    schema = api_stub.AllMatchesSchema(many=True)
    all_matches_args, errors = schema.load(matches_data_dct['matches'])
    if errors:
        warnings.warn("Pull matches schema-load error: {}".format(errors))
        return None
    for m in all_matches_args:
        match_id = m['match_id']
        if not StoreManager.exists_in_match_store(match_id):
            create_match(**m, match_pin='1000')
        else:
            # if match exists, then update its attributes
            # Avoids deleting the match as the match might already
            # have been started (but venue was updated later for example)
            match = match_manager.get_match(match_id)
            constructor_args = prepare_match_args(**m, match_pin='1000')
            match.update(**constructor_args)
    return matches_data_dct


def pull_match_players(match_id, remote_auth_token):
    api_stub = ApiManager.api_stub
    match_players_url = api_stub.MATCH_PLAYERS_URL
    match_players_data = ApiManager.get(match_players_url,
                                        params={'matchId': match_id,
                                                'authToken': remote_auth_token})
    if not match_players_data:
        return None
    match_players_dct = to_object_format(match_players_data)
    schema = api_stub.MatchPlayersSchema()
    match_players, errors = schema.load(match_players_dct)
    if errors:
        raise Exception("Error in parsing match_players output:{}, error:{}".format(
            match_players_dct, errors))
    for p in match_players['player_list']:
        # also updates team associations of players
        create_player(p)
    return match_players['player_list']
