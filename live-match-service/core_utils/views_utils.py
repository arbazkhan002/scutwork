import json

from core_utils.string_utils import to_snake_case, to_camel_case
from core_utils.dict_utils import map_keys

__author__ = 'arbaz'


def to_object_format(data):
    return map_keys(to_snake_case, data)


def to_ui_format(data):
    return map_keys(to_camel_case, data)


def OkResponse():
    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}


def OkLoginResponse(auth_token, payload=None):
    if payload is None:
        payload = {}
    msg = {'success': True, 'apiAuthToken': auth_token}
    msg.update(payload)
    return json.dumps(msg), 200, {'ContentType': 'application/json'}
