from core_utils import string_utils
import logging
import traceback

__author__ = 'arbaz'

logger = logging.getLogger(__name__)


def map_keys_list(mapper, item_list):
    mapped_list = []
    for v in item_list:
        if isinstance(v, list):
            mapped_list.append(map_keys_list(mapper, v))
        elif isinstance(v, dict):
            mapped_list.append(map_keys(mapper, v))
        else:
            mapped_list.append(v)
    return mapped_list


def map_keys(mapper, item_dict):
    mapped_dict = {}
    try:
        for k, v in item_dict.items():
            mapped_key = mapper(k)
            if isinstance(v, dict):
                mapped_dict[mapped_key] = map_keys(mapper, v)
            elif isinstance(v, list):
                mapped_dict[mapped_key] = map_keys_list(mapper, v)
            else:
                mapped_dict[mapped_key] = v
        return mapped_dict
    except:
        raise Exception(item_dict)
