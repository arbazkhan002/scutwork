from marshmallow.schema import Schema
from marshmallow.utils import missing as MISSING_TYPE
from marshmallow.decorators import post_dump
from core_utils.dict_utils import map_keys
from core_utils.string_utils import to_camel_case

__author__ = 'arbaz'


class BaseSchema(Schema):

    def on_bind_field(self, field_name, field_obj):
        # Override default missing attribute so
        # that missing values deserialize to None
        if field_obj.missing == MISSING_TYPE:
            field_obj.missing = None
            field_obj.allow_none = True


class BaseSchemaWithCleaningForNulls(BaseSchema):

    @post_dump
    def clean_missing(self, data):
        ret = data.copy()
        for key in filter(lambda key: data[key] is None, data):
            del ret[key]
        return ret
