import os
from flask_bcrypt import Bcrypt

bcrypt = Bcrypt()


class DevConfig:
    server_config = {
        'host': '0.0.0.0',
        'port': int(os.getenv('APP_PORT', 3208)),
        'threaded': True,
        'debug': True,
    }

    BCRYPT_LOG_ROUNDS = 4
    SECRET_KEY = os.environ["SECRET_KEY"]
    ADMIN_USER = "user"
    ADMIN_PASSWORD = "password"
    ADMIN_HASH = bcrypt.generate_password_hash(ADMIN_PASSWORD,
                                               BCRYPT_LOG_ROUNDS)


class ProdConfig:
    server_config = {
        'debug': False
    }

    BCRYPT_LOG_ROUNDS = 4
    SECRET_KEY = "scoreel"
    ADMIN_USER = "user"
    ADMIN_PASSWORD = "scores"
    ADMIN_HASH = bcrypt.generate_password_hash(ADMIN_PASSWORD,
                                               BCRYPT_LOG_ROUNDS)


class Config:
    config = DevConfig


def get_server_config(env):
    if env.lower() == "production":
        Config.config = ProdConfig
        return ProdConfig.server_config
    if env.lower() == "development":
        Config.config = DevConfig
        return DevConfig.server_config
    return DevConfig.server_config
