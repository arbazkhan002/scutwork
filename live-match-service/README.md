Server Details
===============

The following document is a detailed documentation of the server to be used to
support live scoring of cricket games. The objective is to build a server that 
supports a single person update of the game and live score updates on multiple
clients which may or may not be in the same geographical location.
                           

Server APIs 
------------

Ball update details : To update the score-card on each ball, the client needs to
send the following details for every ball event :

```BallEventRequest 
{
  striker : GUID,
  nonStriker : GUID,
  bowler : GUID,
  matchid : GUID
  over : int,
  ballSequence : int,
  innings : int,
  is_powerplay : string,
  runs
  wickets
}

Runs {
  batsmanRuns : int
  is4 : boolean
  is6 : boolean
  wideRuns : int
  isNb :  boolean
  byesRuns : int
}

Wicket {
  batsmanOut : GUID,
  dismissaType : String,
  fielder : guid
  strike_changed : boolean
```

Match state details: From server to client to show current brief state

```
{
    "striker": {
        "name": "R Sharma",
        "playerId": "ade01df4-88cf-44f1-9535-30d1974a2df2",
        "ballsFaced": "72",
        "runsScored": "53",
        "dots": "20",
        "fours": "5",
        "sixes": "2"
    },
    "nonStriker": {
        "name": "M Pandey",
        "playerId": "5ba460d0-2714-49e0-9ea5-3a4a5f4cd9cb",
        "ballsFaced": "20",
        "runsScored": "10",
        "dots": "10",
        "fours": "-",
        "sixes": "-"
    },
    "bowler": {
        "name": "J Faulkner",
        "playerId": "791c576b-50b4-4d3e-87f3-5b186518b603",
        "bowlingFigures": "1.5-0-10-2",
        "ballsBowled": [".", "1", "1", ".", ".", "1"]
    },
    "matchId": "c6ab8127-7d54-46a8-bd55-98cf31757cfa",
    "over": "19",
    "ballSequence": "4",
    "innings": "1",
    "isPowerplay": "null"
}
```

Storage Details
---------------

The in-memory store (key-value store), stores a bunch of information related to
the match with specific keys. For a given match, we will specify the following
keys :

`
match_id#meta_information : Meta information related to the match that might not
                            be updated very often.
match_id#bat_player_guid (x22) : Information related to the batting of each
  player in the match.
match_id#bowl_player_guid (x22) : Information related to the bowling of each 
                                  player in the match.
match_id#inning#over#bowl : Event object passed in to the server. Used for undo
                                  purposes.
`
