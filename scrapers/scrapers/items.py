# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Player(scrapy.Item):
    player_id = scrapy.Field()
    name = scrapy.Field()
    team_id = scrapy.Field()
    player_url = scrapy.Field()


class Team(scrapy.Item):
    team_id = scrapy.Field()
    team_name = scrapy.Field()
    team_url = scrapy.Field()
