# -*- coding: utf-8 -*-
from scrapy.exporters import CsvItemExporter
from scrapers.items import Team, Player
import pdb

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


class ScrapersPipeline(object):

    def process_item(self, item, spider):
        return item


class CSVPipeline(object):

    def __init__(self):
        self.file = open(self.FILENAME, 'wb')
        self.exporter = CsvItemExporter(self.file)
        self.exporter.start_exporting()

    def close_spider(self, spider):
        self.exporter.finish_exporting()
        self.file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item


class TeamPipeline(CSVPipeline):
    FILENAME = "teams.csv"

    def process_item(self, item, spider):
        if isinstance(item, Team):
            return super().process_item(item, spider)
        return item


class PlayerPipeline(CSVPipeline):
    FILENAME = "players.csv"

    def process_item(self, item, spider):
        if isinstance(item, Player):
            return super().process_item(item, spider)
        return item
