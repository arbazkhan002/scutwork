# -*- coding: utf-8 -*-

# To run
# scrapy crawl player

import scrapy

from scrapy.spiders import CrawlSpider
from scrapers.items import Player, Team
from scrapy.shell import inspect_response
from scrapy.spiders.crawl import Rule
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from urllib.parse import urlparse, parse_qs


def get_query_param(param_name, url):
    return parse_qs(urlparse(url).query)[param_name][0]


class PlayerSpider(CrawlSpider):
    name = 'player'
    allowed_domains = ['cricbay.com']
    start_urls = ['http://www.cricbay.com/pointsdashboard.asp?seasoneventid=66']

    rules = (
        Rule(LxmlLinkExtractor(allow=('teamprofile.asp', )), callback='parse_item'),
    )

    def parse_item(self, response):
        items = []
        team_name = response.xpath("//h1/text()")[0].extract().strip()
        team_id = get_query_param("teamid", response.url)
        item = Team()
        item["team_id"] = team_id
        item["team_name"] = team_name
        item["team_url"] = response.url
        items.append(item)
        player_profiles = response.xpath(
            "//div[@id='roster']//div[contains(@class, 'card-body')]"
            "//a[contains(@href, 'playerprofile')]")
        for p in player_profiles:
            item = Player()
            rel_url = p.xpath("@href").extract()[0]
            item["team_id"] = team_id
            item["name"] = p.xpath("text()").extract()[0].strip()
            item["player_id"] = get_query_param("playerid", rel_url)
            item["player_url"] = response.urljoin(rel_url)
            items.append(item)
        return items
