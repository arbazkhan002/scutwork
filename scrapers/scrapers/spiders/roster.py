import scrapy


class Player(scrapy.Item):
    # The source URL
    player_id = scrapy.Field()
    # The destination URL
    name = scrapy.Field()

    team_id = scrapy.Field()
