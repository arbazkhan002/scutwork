import React from 'react';
import {
  Button,
  ButtonGroup,
  Checkbox,
  ListGroup,
  ListGroupItem
} from 'react-bootstrap';
import newId from './newId';
import { GameConstants } from './Constants';
import Icon from './svgIcons';
import { FlipFade } from './Fade';

class PlayingXI extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      matchSetupState: {
        team1: this.props.homeTeam || [],
        team2: this.props.awayTeam || [],
        teamRoster1: this.props.homeTeamRoster,
        teamRoster2: this.props.awayTeamRoster,
        currentTargetTeam: this.props.currentTargetTeam || 'team1'
      }
    };
  }

  componentWillMount() {
    this.id = newId();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      matchSetupState: {
        team1: nextProps.homeTeam || this.state.matchSetupState.team1,
        team2: nextProps.awayTeam || this.state.matchSetupState.team2,
        teamRoster1:
          nextProps.homeTeamRoster || this.state.matchSetupState.homeTeamRoster,
        teamRoster2:
          nextProps.awayTeamRoster || this.state.matchSetupState.awayTeamRoster,
        currentTargetTeam:
          nextProps.currentTargetTeam ||
          this.state.matchSetupState.currentTargetTeam
      }
    });
  }

  handleChange(teamField, player) {
    let matchSetupState = this.state.matchSetupState,
      team = matchSetupState[teamField];
    if (team.indexOf(player) > -1) {
      team = team.filter(x => x !== player);
    } else if (team.size === GameConstants.MAX_PLAYERS_IN_TEAM) {
      team.shift();
    } else {
      team.push(player);
    }
    matchSetupState[teamField] = team;
    this.setState({
      matchSetupState: matchSetupState
    });
  }

  onSave(teamSelected, teamCaptain) {
    let matchSetupState = this.state.matchSetupState,
      targetTeam = matchSetupState.currentTargetTeam,
      progress = null;
    matchSetupState[targetTeam] = teamSelected;
    if (matchSetupState.currentTargetTeam === 'team2') {
      progress = 3;
    } else {
      progress = 2;
      matchSetupState.currentTargetTeam === 'team2';
    }
    this.props.setPlayingXI(
      matchSetupState.team1.length == 0 ? null : matchSetupState.team1,
      matchSetupState.team2.length == 0 ? null : matchSetupState.team2,
      progress
    );

    this.setState({
      matchSetupState: matchSetupState
    });
  }

  onBack() {
    let matchSetupState = this.state.matchSetupState,
      targetTeam = matchSetupState.targetTeam,
      progress = 1;

    if (matchSetupState.currentTargetTeam === 'team1') {
      //clear matchSetupState up, launch modal to not lose changes
    }
    matchSetupState.currentTargetTeam = 'team1';
    this.props.setPlayingXI(
      matchSetupState.team1.length == 0 ? null : matchSetupState.team1,
      matchSetupState.team2.length == 0 ? null : matchSetupState.team2,
      progress
    );
    this.setState({
      matchSetupState: matchSetupState
    });
  }

  render() {
    let shortMatchState = this.props.shortMatchState,
      matchSetupState = this.state.matchSetupState,
      targetTeam = matchSetupState.currentTargetTeam,
      team1 = matchSetupState.team1,
      team2 = matchSetupState.team2,
      teamRoster1 = matchSetupState.teamRoster1,
      teamRoster2 = matchSetupState.teamRoster2;
    return (
      <div className="">
        <div className="row">
          <div className="col-xs-12">
            {targetTeam === 'team1' ? (
              <RosterSelection
                key={this.id + '-home'}
                handleClick={this.handleChange.bind(this, 'team1')}
                teamRoster={teamRoster1}
                team={team1}
                targetTeam={targetTeam}
                onSave={this.onSave.bind(this)}
                onBack={this.onBack.bind(this)}
              />
            ) : (
              <RosterSelection
                key={this.id + '-away'}
                handleClick={this.handleChange.bind(this, 'team2')}
                teamRoster={teamRoster2}
                team={team2}
                targetTeam={targetTeam}
                onSave={this.onSave.bind(this)}
                onBack={this.onBack.bind(this)}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

class RosterSelection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      teamSelected: this.props.team.slice(0),
      captainId: this.props.captainId
    };
  }

  handleClick(player, eventKey) {
    /*    console.log(eventKey.target);
    console.log('before: ' + team);*/
    let team = this.state.teamSelected;
    let captainId = this.state.captainId;
    if (team.indexOf(player) > -1) {
      team = team.filter(x => x != player);
      this.resetCaptain(team);
    } else if (team.length === GameConstants.MAX_PLAYERS_IN_TEAM) {
      team.shift();
      team.push(player);
      this.resetCaptain(team);
    } else {
      team.push(player);
    }

    /*    console.log('after: ' + JSON.stringify(team));*/
    this.setState({
      teamSelected: team
    });
  }

  endSelection(event) {
    this.props.onSave(this.state.teamSelected, this.state.captainId);
  }

  makeCaptain(player) {
    this.setState({ captainId: player.playerId });
  }

  resetCaptain(team) {
    let captainId = this.state.captainId;
    if (team.filter(x => x.playerId === captainId).length == 0) {
      this.setState({ captainId: team.length > 0 && team[0].playerId });
    }
  }

  render() {
    let team = this.state.teamSelected,
      targetTeam = this.props.targetTeam,
      teamRoster = this.props.teamRoster.sort(function(a, b) {
        return a.name.localeCompare(b.name);
      }),
      captainId = this.state.captainId || (team.length > 0 && team[0].playerId);
    return (
      <div>
        <div className="row action-row">
          <div className="col-xs-1 col-md-3" />
          <div className="col-xs-10 col-md-6">
            <ButtonGroup justified className="ballevent-actions">
              <Button
                href="#"
                bsStyle="danger"
                className="dismiss"
                //onClick={this.sendBallEventTest.bind(this)}
                onClick={this.props.onBack}
              >
                Back
              </Button>
              <Button
                href="#"
                bsStyle="success"
                disabled={
                  team
                    ? team.length < GameConstants.MIN_PLAYERS_IN_TEAM
                      ? true
                      : false
                    : true
                }
                onClick={this.endSelection.bind(this)}
              >
                {targetTeam === 'team1' ? 'Next' : 'Toss'}
              </Button>
            </ButtonGroup>
          </div>
          <div className="col-xs-1 col-md-3" />
        </div>
        <div className="row">
          {team.length > 0 || teamRoster.size == 0 ? (
            <ListGroup>
              <div className="selection-list-item">
                <span className="selection-index list-content list-content-10" />
                <span className="selection-index list-content list-content-90" />
                {/* Captain selection label
              <span className="selection-index list-content list-content-10">(c)</span>
            */}
              </div>
              <FlipFade>
                {team.map((player, index) => (
                  <div className="selection-list-item">
                    <span className="selection-index list-content list-content-10">
                      {index + 1}
                    </span>
                    <ListGroupItem
                      key={player.playerId}
                      onClick={this.handleClick.bind(this, player)}
                      active={team.indexOf(player) > -1}
                      className="list-content list-content-90"
                    >
                      <div className="svg-icon-wrapper">
                        {team.indexOf(player) === -1 ? (
                          <Icon size="2rem" icon="blank-checkbox" />
                        ) : (
                          <Icon size="2rem" icon="checked-box" />
                        )}
                      </div>
                      <span className="icon-right-label">{player.name}</span>
                    </ListGroupItem>
                    {/* Captain selection radio buttons
                  {player.playerId === captainId ?
                    (<span className="selection-index list-content list-content-10">
                      <Icon icon="radio-button-on" size="2rem"/>
                    </span>)
                  : (<span className="selection-index list-content list-content-10"
                            onClick={this.makeCaptain.bind(this, player)}>
                      <Icon icon="radio-button-off" size="2rem"/>
                    </span>)
                  }
                */}
                  </div>
                ))}

                {/*  **** Uncomment this to show add new player ***
                <ListGroupItem>
                  <div className="svg-icon-wrapper">
                    <Icon size="2rem" icon="add" />
                  </div>
                  <span className="icon-right-label">{'New Player'}</span>
                </ListGroupItem>
              */}
              </FlipFade>
            </ListGroup>
          ) : null}
          <ListGroup>
            <FlipFade>
              {teamRoster.filter(x => team.indexOf(x) == -1).map(player => (
                <ListGroupItem
                  key={player.playerId}
                  onClick={this.handleClick.bind(this, player)}
                  active={team.indexOf(player) > -1}
                >
                  <div className="svg-icon-wrapper">
                    {team.indexOf(player) === -1 ? (
                      <Icon size="2rem" icon="blank-checkbox" />
                    ) : (
                      <Icon size="2rem" icon="checked-box" />
                    )}
                  </div>
                  <span className="icon-right-label">{player.name}</span>
                </ListGroupItem>
              ))}
            </FlipFade>
          </ListGroup>
        </div>
      </div>
    );
  }
}

export default PlayingXI;
