export const GameConstants = {
  MIN_PLAYERS_IN_TEAM: 1,
  MAX_PLAYERS_IN_TEAM: 11,
  DISMISSAL_TYPES: [
    {
      name: 'Bowled',
      type: 'b',
      fielderRequired: false,
      batsmanRequired: false,
      special: false,
      bowlerRequired: true
    },
    {
      name: 'Caught',
      type: 'c',
      fielderRequired: true,
      batsmanRequired: false,
      special: false,
      bowlerRequired: true
    },
    {
      name: 'Caught Behind',
      type: 'cb',
      fielderRequired: true,
      batsmanRequired: false,
      special: false,
      bowlerRequired: true
    },
    {
      name: 'Runout',
      type: 'runout',
      fielderRequired: true,
      batsmanRequired: true,
      special: false,
      bowlerRequired: false
    },
    {
      name: 'Stumped',
      type: 'st',
      fielderRequired: true,
      batsmanRequired: false,
      special: false,
      bowlerRequired: true
    },
    {
      name: 'Caught Substitute',
      type: 'c-sub',
      fielderRequired: false,
      batsmanRequired: false,
      special: true,
      bowlerRequired: true
    },
    {
      name: 'Hit Wicket',
      type: 'hit-wicket',
      fielderRequired: false,
      batsmanRequired: false,
      special: true,
      bowlerRequired: false
    },
    {
      name: 'Retired Hurt',
      type: 'rtd-hurt',
      fielderRequired: false,
      batsmanRequired: true,
      special: true,
      bowlerRequired: false
    },
    {
      name: 'Timed Out',
      type: 'timedout',
      fielderRequired: false,
      batsmanRequired: true,
      special: true,
      bowlerRequired: false
    },
    {
      name: 'Obstructed Field',
      type: 'obst-field',
      fielderRequired: false,
      batsmanRequired: true,
      special: true,
      bowlerRequired: false
    },
    {
      name: 'Handled the Ball',
      type: 'handled-ball',
      fielderRequired: false,
      batsmanRequired: false,
      special: true,
      bowlerRequired: false
    },
    {
      name: 'Hit Ball Twice',
      type: 'hit-twice',
      fielderRequired: false,
      batsmanRequired: false,
      special: true,
      bowlerRequired: false
    }
  ]
};

export const StyleConstants = {
  REGULAR_BALL: 0,
  BOUNDARY_BALL: 1,
  WICKET_BALL: 2
};
