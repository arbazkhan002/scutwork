export function initializeBallEvent(matchState) {
  let striker = matchState.playerStriker,
    nonStriker = matchState.playerNonStriker,
    playerCurrentBowler = matchState.playerCurrentBowler;

  return {
    playerStriker: !striker
      ? null
      : {
          name: striker.name,
          playerId: striker.playerId
        },
    playerNonStriker: !nonStriker
      ? null
      : {
          name: nonStriker.name,
          playerId: nonStriker.playerId
        },
    playerCurrentBowler: !playerCurrentBowler
      ? null
      : {
          name: playerCurrentBowler.name,
          playerId: playerCurrentBowler.playerId
        },
    matchId: matchState.matchId,
    overNumber: matchState.overNumber,
    ballNumber: matchState.ballNumber,
    ballSequence: matchState.ballSequence,
    inningId: matchState.inningId,
    isPowerPlay: matchState.powerPlayDetails
      .map(x => x.overNumber)
      .includes(matchState.overNumber),
    runs: {
      batrunsScored: 0,
      is4: null,
      is6: null,
      wideRuns: null,
      isNb: null,
      byesRuns: null
    },
    wicket: {
      batsmanDismissed: null,
      dismissalType: null,
      fielder: null
    },
    commentaryText: 'no run',
    commentaryInput: ''
  };
}

export function isSameMatchState(s1, s2) {
  return (
    s1 &&
    s2 &&
    s1.playerStriker === s2.playerStriker &&
    s1.playerNonStriker === s2.playerNonStriker &&
    s1.playerCurrentBowler === s2.playerCurrentBowler &&
    s1.ballSequence === s2.ballSequence
  );
}

// https://github.com/github/fetch/issues/175
export function timeout(ms, promise) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      reject(new Error('timeout'));
    }, ms);
    promise.then(resolve, reject);
  });
}

export function ordinalSuffixOf(i) {
  var j = i % 10,
    k = i % 100;
  if (j == 1 && k != 11) {
    return i + 'st';
  }
  if (j == 2 && k != 12) {
    return i + 'nd';
  }
  if (j == 3 && k != 13) {
    return i + 'rd';
  }
  return i + 'th';
}

export function withQuantSuffix(quantity, label) {
  return label + (quantity > 1 ? 's' : '');
}

export function querify(obj) {
  //https://stackoverflow.com/questions/1714786/query-string-encoding-of-a-javascript-object
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
    }
  return str.join('&');
}

export function waitFor(delay) {
  //https://stackoverflow.com/questions/22707475/how-to-make-a-promise-from-settimeout
  return new Promise(function(resolve) {
    setTimeout(resolve, delay);
  });
}

export function unCamelCase(str) {
  return (
    str
      // insert a space between lower & upper
      .replace(/([a-z])([A-Z])/g, '$1 $2')
      // space before last upper in a sequence followed by lower
      .replace(/\b([A-Z]+)([A-Z])([a-z])/, '$1 $2$3')
      // uppercase the first character
      .replace(/^./, function(str) {
        return str.toUpperCase();
      })
  );
}

export function groupBy(list, keyGetter) {
  const map = new Map();
  list.forEach(item => {
    const key = keyGetter(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [item]);
    } else {
      collection.push(item);
    }
  });
  return map;
}

export function getLocationLastSegment() {
  let locationParts = window.location.href.split('/'),
    endpoint = locationParts.pop() || locationParts.pop();
  return endpoint;
}

export function getNestedKey(obj, path) {
  // get key from an object when path is specified as
  // venue.city.name
  let pathElem = path.split('.');
  if (pathElem.length > 1) {
    return getNestedKey(obj[pathElem[0]], pathElem.slice(1).join('.'));
  } else {
    return obj[pathElem[0]];
  }
}

export function convertTimestamp(timestamp) {
  //https://gist.github.com/kmaida/6045266
  var d = new Date(timestamp * 1000), // Convert the passed timestamp to milliseconds
    yyyy = d.getFullYear(),
    mm = ('0' + (d.getMonth() + 1)).slice(-2), // Months are zero based. Add leading 0.
    dd = ('0' + d.getDate()).slice(-2), // Add leading 0.
    hh = d.getHours(),
    h = hh,
    min = ('0' + d.getMinutes()).slice(-2), // Add leading 0.
    ampm = 'AM',
    time;

  if (hh > 12) {
    h = hh - 12;
    ampm = 'PM';
  } else if (hh === 12) {
    h = 12;
    ampm = 'PM';
  } else if (hh == 0) {
    h = 12;
  }

  // ie: 2018-02-18, 8:35 AM
  time = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;

  return time;
}

export function logoutIfTokenExpired(callback) {
  console.log(callback);
  let currentTS = new Date().getTime(),
    expirationTS = sessionStorage.getItem('tokenExpirationTS');
  if (expirationTS && expirationTS < currentTS) {
    callback();
  }
}
