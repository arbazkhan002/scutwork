import React from 'react';
import Dashboard from './Dashboard';
import GameHeader from './GameHeader';
import MatchSetup from './MatchSetup';
import Container from './Container';
import ToggleSwitch from './ToggleSwitch';
import { BubbleLoader } from 'react-css-loaders';
import { timeout, waitFor, logoutIfTokenExpired } from './utils';
import { Fade } from './Fade';
import AdminPage from './Admin';
import {
  Button,
  Col,
  ControlLabel,
  Form,
  FormGroup,
  FormControl,
  Modal,
  Navbar
} from 'react-bootstrap';
import { Link, Redirect, Route, Switch } from 'react-router-dom';

class Home extends React.Component {
  constructor(props) {
    super(props);
    let adminToken = localStorage.getItem('adminToken'),
      scorerToken = sessionStorage.getItem('authToken'),
      remoteApiToken = sessionStorage.getItem('remoteApiToken'),
      scorerMatchId = sessionStorage.getItem('scorerMatchId');
    this.state = {
      showModal: false,
      showAdminModal: false,
      isScorer: scorerToken ? true : false,
      scorerMatchId: scorerMatchId,
      isAdmin: adminToken ? true : false,
      scorerAuth: scorerToken,
      failedAdminLogin: false,
      failedLogin: false,
      formData: {
        email: '',
        password: ''
      },
      adminFormData: {
        username: '',
        password: ''
      }
    };
  }

  closeModal() {
    this.setState({
      showModal: false,
      failedLogin: false
    });
  }

  closeAdminModal() {
    this.setState({
      showAdminModal: false,
      failedAdminLogin: false
    });
  }

  openModal() {
    this.setState({ showModal: true });
  }

  openAdminModal() {
    this.setState({ showAdminModal: true });
  }

  toggleView() {
    /*TODO: Remember to invalidate scorer token after expiration*/
    if (
      !this.state.isScorer ||
      !this.state.scorerAuth ||
      !this.state.scorerMatchId
    ) {
      this.openModal();
    } else {
      this.logout();
    }
  }

  logout() {
    sessionStorage.removeItem('authToken');
    sessionStorage.removeItem('remoteApiToken');
    sessionStorage.removeItem('scorerMatchId');
    sessionStorage.removeItem('tokenExpirationTS');
    this.setState({
      isScorer: !this.state.isScorer,
      scorerMatchId: null,
      scorerAuth: null
    });
  }

  logoutExpiredToken() {
    window.alert('Logging out! Token expired. Login again to continue.');
    this.logout();
  }

  login(data) {
    sessionStorage.setItem('authToken', data.apiAuthToken);
    sessionStorage.setItem('remoteApiToken', data.authToken);
    sessionStorage.setItem('scorerMatchId', data.matchId);
    let currentTS = new Date().getTime();
    sessionStorage.setItem('tokenExpirationTS', currentTS + 3 * 60 * 60 * 1000);
    this.setState({
      isScorer: true,
      scorerMatchId: data.matchId,
      scorerAuth: data.apiAuthToken,
      formData: {
        email: '',
        password: ''
      }
    });
  }

  isScorer() {
    console.log('token expiry check called');
    logoutIfTokenExpired(this.logoutExpiredToken.bind(this));
    return (
      this.state.isScorer &&
      this.state.scorerMatchId.split(',').includes(this.readMatchFromUrl())
    );
  }

  onFormChange(event) {
    this.state.formData[event.target.id] = event.target.value;
    this.setState({ formData: this.state.formData });
  }

  onAdminFormChange(event) {
    this.state.adminFormData[event.target.id] = event.target.value;
    this.setState({ adminFormData: this.state.adminFormData });
  }

  readMatchFromUrl() {
    /* readMatchFromUrl can be the tick function for expiration based logout*/
    logoutIfTokenExpired(this.logoutExpiredToken.bind(this));
    let location = window.location.href,
      regexp = /match\/(\d+).*/g,
      regexMatch = regexp.exec(location),
      matchId = regexMatch && regexMatch.length > 1 && regexMatch[1];

    return matchId;
  }

  submitLoginDetails() {
    const matchId = this.readMatchFromUrl();
    return fetch('/api/v1/scorer/login', {
      method: 'POST',
      body: JSON.stringify(this.state.formData),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '.*'
      }
    })
      .then(function(response) {
        //response.status     //=> number 100–599
        //response.statusText //=> String
        //response.headers    //=> Headers
        //response.url        //=> String
        return response.json();
      })
      .then(data => {
        this.login(data);
        return true;
      })
      .catch(err => {
        this.setState({
          failedLogin: true
        });
        return false;
      });
  }

  resetAdminAuthorization() {
    this.setState({
      isAdmin: false,
      showAdminModal: true
    });
  }

  submitAdminDetails() {
    return fetch('/admin/api/v1/login', {
      method: 'POST',
      body: JSON.stringify(this.state.adminFormData),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '.*'
      }
    })
      .then(function(response) {
        //response.status     //=> number 100–599
        //response.statusText //=> String
        //response.headers    //=> Headers
        //response.url        //=> String
        return response.json();
      })
      .then(data => {
        localStorage.setItem('adminToken', data.apiAuthToken);
        this.setState({
          isAdmin: true,
          adminFormData: {
            username: '',
            password: ''
          }
        });
        return true;
      })
      .catch(err => {
        this.setState({
          failedAdminLogin: true
        });
        return false;
      });
  }

  render() {
    let matchId = this.readMatchFromUrl(),
      isScorer = this.state.isScorer;
    console.log(isScorer);
    return (
      <div>
        <Navbar id={'brand-header'} inverse collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">Scoreel</Link>
            </Navbar.Brand>
            <Navbar.Brand>
              <div
                onClick={this.toggleView.bind(this)}
                className="scorer-login-toggle"
              >
                {isScorer ? (
                  <div className="scorer-login-text">
                    <span className="glyphicon glyphicon-log-out" />{' '}
                    <span>Logout</span>
                  </div>
                ) : (
                  <div className="scorer-login-text">
                    <span className="glyphicon glyphicon-pencil" />{' '}
                    <span>Score</span>
                  </div>
                )}
              </div>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse />
        </Navbar>
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route
            exact
            path="/match/:matchId"
            render={routeProps => (
              <MatchSetup
                isScorer={this.isScorer()}
                getLogin={this.openModal.bind(this)}
                logoutCallBack={this.logoutExpiredToken.bind(this)}
                {...routeProps}
              />
            )}
          />
          <Route
            path="/match/:matchId/live"
            render={routeProps => (
              <Container
                {...routeProps}
                isScorer={this.isScorer()}
                logoutCallBack={this.logoutExpiredToken.bind(this)}
              />
            )}
          />
          <Route
            path="/league/admin"
            render={routeProps => (
              <AdminPage
                isAdmin={this.state.isAdmin}
                resetAuthorization={this.resetAdminAuthorization.bind(this)}
                getLogin={() => this.setState({ showAdminModal: true })}
                {...routeProps}
              />
            )}
          />
          <Route path="/league/admin" component={AdminPage} />
        </Switch>
        <ScorerLoginModal
          showModal={this.state.showModal}
          close={this.closeModal.bind(this)}
          onSubmitModal={this.submitLoginDetails.bind(this)}
          onFormChange={this.onFormChange.bind(this)}
          formData={this.state.formData}
        />

        <AdminLoginModal
          showModal={this.state.showAdminModal}
          close={this.closeAdminModal.bind(this)}
          onSubmitModal={this.submitAdminDetails.bind(this)}
          onFormChange={this.onAdminFormChange.bind(this)}
          formData={this.state.adminFormData}
        />
      </div>
    );
  }
}

class LoginModal extends React.Component {
  constructor(props) {
    super(props);
    this.NOATTEMPT = 0;
    this.FAILEDATTEMPT = 1;
    this.SUCCESSFULATTEMPT = 2;
    this.state = {
      isLoading: false,
      loginStatus: this.NOATTEMPT
    };
  }

  onSubmit() {
    const self = this;

    self.setState({ isLoading: true });

    this.props.onSubmitModal().then(status => {
      self.setState({ isLoading: false });
      if (status) {
        self.setState({ loginStatus: this.SUCCESSFULATTEMPT });
        waitFor(2000).then(() => {
          self.setState({ loginStatus: this.NOATTEMPT });
          self.props.close();
        });
      } else {
        self.setState({ loginStatus: this.FAILEDATTEMPT });
        waitFor(2000).then(() => {
          self.setState({ loginStatus: this.NOATTEMPT });
        });
      }
    });
  }
}

class ScorerLoginModal extends LoginModal {
  render() {
    return (
      <div>
        <Modal
          dialogClassName="scorer-login-modal"
          show={this.props.showModal}
          onHide={this.props.close}
        >
          <Modal.Body>
            <Fade>
              {this.state.loginStatus == this.SUCCESSFULATTEMPT ? (
                <div class="alert alert-success" role="alert">
                  Login Successful!
                </div>
              ) : this.state.loginStatus == this.FAILEDATTEMPT ? (
                <div class="alert alert-danger" role="alert">
                  Login Failed!
                </div>
              ) : null}
            </Fade>
            <Form horizontal className="scorer-input">
              <FormGroup controlId="email">
                <FormControl
                  className="col-xs-10"
                  type="text"
                  placeholder="Email"
                  onChange={this.props.onFormChange.bind(this)}
                />
                <Col componentClass={ControlLabel} xs={2} />
              </FormGroup>
              <FormGroup controlId="password">
                <FormControl
                  className="col-xs-10"
                  type="password"
                  placeholder="Password"
                  onChange={this.props.onFormChange.bind(this)}
                />
                <Col componentClass={ControlLabel} xs={2} />
              </FormGroup>
              <Button
                bsStyle="primary"
                onClick={this.onSubmit.bind(this)}
                block
                id="scorer-login-button"
              >
                {this.state.isLoading ? (
                  <BubbleLoader
                    className="loader-on-component"
                    color="#fff"
                    size={2}
                  />
                ) : (
                  <div>
                    <span>Start Scoring</span>
                    <span className="glyphicon glyphicon-chevron-right pull-right" />
                  </div>
                )}
              </Button>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

class AdminLoginModal extends LoginModal {
  render() {
    return (
      <div>
        <Modal
          dialogClassName="scorer-login-modal"
          show={this.props.showModal}
          onHide={this.props.close}
        >
          <Modal.Body>
            <Fade>
              {this.state.loginStatus == this.SUCCESSFULATTEMPT ? (
                <div class="alert alert-success" role="alert">
                  Login Successful!
                </div>
              ) : this.state.loginStatus == this.FAILEDATTEMPT ? (
                <div class="alert alert-danger" role="alert">
                  Login Failed!
                </div>
              ) : null}
            </Fade>
            <Form horizontal className="scorer-input">
              <FormGroup controlId="username">
                <FormControl
                  className="col-xs-10"
                  type="text"
                  placeholder="Rohit Sharma"
                  onChange={this.props.onFormChange.bind(this)}
                />
                <Col componentClass={ControlLabel} xs={2} />
              </FormGroup>
              <FormGroup controlId="password">
                <FormControl
                  className="col-xs-10"
                  type="password"
                  placeholder="Password"
                  onChange={this.props.onFormChange.bind(this)}
                />
                <Col componentClass={ControlLabel} xs={2} />
              </FormGroup>
              <Button
                bsStyle="primary"
                onClick={this.onSubmit.bind(this)}
                block
                id="scorer-login-button"
              >
                {this.state.isLoading ? (
                  <BubbleLoader
                    className="loader-on-component"
                    color="#fff"
                    size={2}
                  />
                ) : (
                  <div>
                    <span>Log In</span>
                    <span className="glyphicon glyphicon-chevron-right pull-right" />
                  </div>
                )}
              </Button>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default Home;
