import React from 'react';

export default class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false
    };
  }

  toggle() {
    this.setState({ toggle: !this.state.toggle });
  }

  componentWillReceiveProps(newProps) {
    let { toggleState } = newProps;
    this.setState({ toggle: toggleState });
  }

  render() {
    const className = `toggle-component ${this.state.toggle ? ' active' : ''}`;
    return (
      <div
        className={className}
        onClick={() => {
          this.toggle();
          this.props.onToggle(this.state.toggle);
        }}
      >
        <div className="scorer-toggle-button" />
      </div>
    );
  }
}
