import React from 'react';
import newId from './newId';
import CommentaryHead from './CommentaryHead';
import { wicketCommentary } from './CommentaryHead';
import DialogModal from './DialogModal';
import Select from 'react-select';
import { CSSTransition } from 'react-transition-group';
import ReactCSSTransitionReplace from 'react-css-transition-replace';
import { BubbleLoader } from 'react-css-loaders';
import { Fade } from './Fade';
import {
  initializeBallEvent,
  isSameMatchState,
  waitFor,
  logoutIfTokenExpired
} from './utils';
import Icon from './svgIcons';
import {
  Alert,
  Button,
  ButtonGroup,
  ButtonToolbar,
  Checkbox,
  ControlLabel,
  FormControl,
  FormGroup,
  DropdownButton,
  MenuItem,
  Panel,
  PanelGroup,
  Radio,
  Table
} from 'react-bootstrap';

class BallEvent extends React.Component {
  constructor(props) {
    super(props);
    let matchState = Object.assign({}, this.props.matchState),
      ballEvent = initializeBallEvent(this.props.matchState);

    this.state = {
      matchState: matchState,
      ballEvent: ballEvent,
      inProgress: false,
      justFinished: false,
      dialogShow: false
    };
    this.dismissalTypes = this.props.dismissalTypes;
  }

  addRuns(tag) {
    let ballEvent = Object.assign({}, this.state.ballEvent);
    ballEvent.runs.batrunsScored = tag;
    ballEvent.runs.is4 = tag === 4 ? true : false;
    ballEvent.runs.is6 = tag === 6 ? true : false;
    ballEvent._isFromDownstream = true;
    this.setState({ ballEvent: ballEvent });
  }

  handleChange(nextBallEvent, nextMatchState) {
    let ballEvent = Object.assign({}, nextBallEvent),
      matchState = Object.assign({}, nextMatchState);
    matchState && (matchState._isFromDownstream = true);
    ballEvent && (ballEvent._isFromDownstream = true);
    if (!nextMatchState) {
      this.setState({
        ballEvent: ballEvent
      });
    } else {
      this.setState({
        ballEvent: ballEvent,
        matchState: matchState,
        inProgress: false,
        justFinished: false
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      matchState: Object.assign({}, nextProps.matchState),
      ballEvent: initializeBallEvent(nextProps.matchState),
      justFinished: false,
      inProgress: false
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    // Update BallEvent only if matchState is for next ball
    // or if changes were done downstream i.e. by descendants
    // of BallEvent
    let isUpdated =
      nextState.ballEvent._isFromDownstream ||
      nextState.matchState._isFromDownstream ||
      nextProps.matchState.ballSequence !==
        this.state.matchState.ballSequence ||
      nextProps.sendUndo !== this.props.sendUndo ||
      nextState.justFinished !== this.state.justFinished ||
      nextState.inProgress !== this.state.inProgress ||
      nextState.dialogShow !== this.state.dialogShow ||
      nextState.dialogContent !== this.state.dialogContent;

    console.log(isUpdated);
    return isUpdated;
  }

  saveBall(retainComponent) {
    if (!this.state.inProgress) {
      this.sendBallEvent(retainComponent);
      this.setState({ inProgress: true });
    }
  }

  sendBallEvent(retainComponent) {
    logoutIfTokenExpired(this.props.logoutCallBack);
    const self = this;

    if (this.state.inProgress || this.state.justFinished) {
      // wait till state changes are complete from
      // previous ball
      return;
    }
    let ballEvent = self.state.ballEvent,
      matchId = ballEvent.matchId;
    ballEvent.ballSequence = this.props.matchState.ballSequence + 1;
    this.setState({ inProgress: true });
    fetch('/api/v1/match/' + matchId + '/add_ball_event', {
      method: 'POST',
      body: JSON.stringify(this.state.ballEvent),
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('authToken')}`,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '.*'
      }
    })
      .then(function(response) {
        //response.status     //=> number 100–599
        //response.statusText //=> String
        //response.headers    //=> Headers
        //response.url        //=> String
        return response.json();
      })
      .then(data => {
        self.setState({
          matchState: data,
          ballEvent: initializeBallEvent(data),
          inProgress: false,
          justFinished: true
        });

        waitFor(500)
          .then(() => {
            console.log('resetting values');
            return this.setState({
              inProgress: false,
              justFinished: false
            });
          })
          .then(() => {
            console.log('passing values');
            self.props.handleChange(data, retainComponent);
          });
      })
      .catch(ex => {
        console.log('Parsing Failed', ex);
        this.setState({ inProgress: false });
      });
  }

  sendInningEnd(retainComponent) {
    logoutIfTokenExpired(this.props.logoutCallBack);
    const self = this;
    self.setState({ inProgress: true });
    let matchId = self.state.matchState.matchId;
    fetch('/api/v1/match/' + matchId + '/next', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('authToken')}`,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '.*'
      }
    })
      .then(function(response) {
        if (response.status >= 400) {
          throw new Error('error: ' + response.statusText);
        }
        return response.json();
      })
      .then(data => {
        self.props.handleChange(data, retainComponent);
        self.setState({
          matchState: data,
          ballEvent: initializeBallEvent(data),
          inProgress: false
        });
      })
      .catch(ex => {
        console.log('Parsing Failed', ex);
        this.setState({ inProgress: false });
      });
  }

  forceStateEnd(stateEndInstructions, retainComponent) {
    logoutIfTokenExpired(this.props.logoutCallBack);
    const self = this;
    self.setState({ inProgress: true });
    let matchId = self.state.matchState.matchId;
    fetch('/api/v1/match/' + matchId + '/end', {
      method: 'POST',
      body: JSON.stringify(stateEndInstructions),
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('authToken')}`,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '.*'
      }
    })
      .then(function(response) {
        if (response.status >= 400) {
          throw new Error('error: ' + response.statusText);
        }
        return response.json();
      })
      .then(data => {
        self.props.handleChange(data, retainComponent);
        self.setState({
          matchState: data,
          ballEvent: initializeBallEvent(data),
          inProgress: false
        });
      })
      .catch(ex => {
        console.log('Parsing Failed', ex);
        this.setState({ inProgress: false });
      });
  }

  extrasMetaDisplay() {
    let wideRuns = this.state.ballEvent.runs.wideRuns,
      noBall = this.state.ballEvent.runs.isNb,
      byes = this.state.ballEvent.runs.byesRuns,
      tags = [
        wideRuns > 0 ? (wideRuns > 1 ? wideRuns.toString() + 'Y' : 'Y') : '',
        noBall ? 'N' : '',
        byes ? byes.toString() + 'B' : ''
      ],
      returnval = tags.filter(x => x).join(',');
    return returnval;
  }

  wicketMetaDisplay(dismissalInfo, striker, bowler) {
    let wicket = dismissalInfo,
      wicketData = wicketCommentary(dismissalInfo, striker, bowler);
    return wicket && wicket.dismissalType;
  }

  switchStrike() {
    let ballEvent = Object.assign({}, this.state.ballEvent);
    [ballEvent.playerStriker, ballEvent.playerNonStriker] = [
      ballEvent.playerNonStriker,
      ballEvent.playerStriker
    ];
    this.setState({ ballEvent: ballEvent });
  }

  triggerChangeBowler() {
    let matchState = Object.assign({}, this.state.matchState);
    matchState.playerCurrentBowler = null;
    this.props.handleChange(matchState);
  }

  forceInningEnd() {
    let stateEndInstructions = {
      inningEnd: true
    };
    this.forceStateEnd(stateEndInstructions);
  }

  forceMatchEnd() {
    let stateEndInstructions = {
      matchEnd: true
    };
    this.forceStateEnd(stateEndInstructions);
  }

  clearForm() {
    let ballEvent = initializeBallEvent(this.props.matchState);
    // ensure shouldComponentUpdate is happy
    ballEvent._isFromDownstream = true;
    this.setState({ ballEvent: ballEvent });
  }

  getConfirmation(dialogContent, onConfirm) {
    let onHide = () =>
      this.setState({
        dialogShow: false,
        dialogContent: '',
        dialogOnConfirm: null,
        dialogOnHide: null
      });
    this.setState({
      dialogContent: dialogContent,
      dialogShow: true,
      dialogOnConfirm: onConfirm,
      dialogOnHide: onHide
    });
  }

  render() {
    const scorerButtons = this.props.scorerButtons;
    let matchState = this.state.matchState,
      playerStriker = matchState.playerStriker,
      playerNonStriker = matchState.playerNonStriker,
      playerCurrentBowler = matchState.playerCurrentBowler,
      extrasInBall =
        !this.state.ballEvent.runs.wideRuns &&
        !this.state.ballEvent.runs.byesRuns &&
        !this.state.ballEvent.runs.isNb
          ? false
          : true,
      dialogClose = () => this.setState({ smShow: false });
    return (
      <div className="ballEventWrapper">
        {this.state.matchState.inningEnd || this.state.matchState.matchEnd ? (
          <InningSetup
            matchState={this.state.matchState}
            sendInningEnd={this.sendInningEnd.bind(this, true)}
            inProgress={this.state.inProgress}
          />
        ) : !playerStriker || !playerNonStriker || !playerCurrentBowler ? (
          <BallEventSetup
            ballEvent={this.state.ballEvent}
            matchState={this.state.matchState}
            handleChange={this.handleChange.bind(this)}
          />
        ) : (
          <div id="ball-capture">
            <div className="row">
              <div className="col-xs-4">
                <div className="div-header">Next ball</div>
              </div>
            </div>
            <div className="row" id="commentary-head">
              <div className="col-xs-12">
                <CommentaryHead
                  ballEvent={this.state.ballEvent}
                  dismissalTypes={this.dismissalTypes}
                  isEditable={true}
                  handleChange={this.handleChange.bind(this)}
                />
              </div>
            </div>
            {/* end-recent-over */}
            <div className="row ballevent-collapse" id="ballevent-details">
              <div className="col-xs-12">
                <PanelGroup>
                  <PanelMaker
                    parentId="panel-ballevent-collapse"
                    panelId="runs-scored"
                    panelTitle="Runs to bat"
                    panelBody={RunsPanelElement}
                    expanded={true}
                    scorerButtons={[0, 1, 2, 3, 4, 5, 6]}
                    ballEvent={this.state.ballEvent}
                    matchState={this.state.matchState}
                    addRuns={this.addRuns.bind(this)}
                    metaTag={this.state.ballEvent.runs.batrunsScored}
                    handleChange={this.handleChange.bind(this)}
                  />
                  <PanelMaker
                    parentId="panel-ballevent-collapse"
                    panelId="extras"
                    panelTitle="Extras"
                    panelBody={ExtrasPanelElement}
                    expanded={extrasInBall}
                    ballEvent={this.state.ballEvent}
                    matchState={this.state.matchState}
                    metaTag={this.extrasMetaDisplay()}
                    handleChange={this.handleChange.bind(this)}
                  />
                  <PanelMaker
                    parentId="panel-ballevent-collapse"
                    panelId="wicket"
                    panelTitle="Wicket"
                    panelBody={WicketPanelElement}
                    ballEvent={this.state.ballEvent}
                    matchState={this.state.matchState}
                    dismissalTypes={this.dismissalTypes}
                    metaTag={this.wicketMetaDisplay(
                      this.state.ballEvent.wicket,
                      this.state.matchState.playerStriker.name,
                      this.state.matchState.playerCurrentBowler.name
                    )}
                    handleChange={this.handleChange.bind(this)}
                  />
                </PanelGroup>
              </div>
            </div>
            {/* end-ballevent-collapse*/}
            <div className="row">
              <div className="col-xs-12 col-md-6">
                <ButtonGroup justified className="ballevent-actions">
                  <DropdownButton
                    href="#"
                    className="btn-primary option-button pull-right"
                    bsStyle="default"
                    title={<Icon size="2rem" icon="storage" />}
                    noCaret
                    id="ballevent-options"
                  >
                    <MenuItem eventKey="10" onClick={this.clearForm.bind(this)}>
                      <span className="left-icon left-meta-icon">
                        <Icon size="1.5em" icon="clear" />
                      </span>Clear changes
                    </MenuItem>
                    <MenuItem
                      eventKey="1"
                      onClick={this.switchStrike.bind(this)}
                    >
                      <span className="left-icon left-meta-icon">
                        <Icon size="1.5em" icon="swap-vert" />
                      </span>Swap Strike
                    </MenuItem>
                    <MenuItem
                      eventKey="1"
                      onClick={this.triggerChangeBowler.bind(this)}
                    >
                      <span className="left-icon left-meta-icon">
                        <Icon size="1.5em" icon="undo" />
                      </span>Change Bowler
                    </MenuItem>
                    <MenuItem divider />
                    <MenuItem header>Game Options</MenuItem>
                    <MenuItem
                      eventKey="2"
                      onClick={this.getConfirmation.bind(
                        this,
                        'Are you sure you want to end ' +
                          matchState.battingTeam.teamName +
                          ' innings?',
                        this.forceInningEnd.bind(this)
                      )}
                    >
                      <span className="left-icon left-meta-icon">
                        <Icon size="1.5em" icon="skip-next" />
                      </span>
                      End Inning
                    </MenuItem>
                    <MenuItem
                      eventKey="3"
                      onClick={this.getConfirmation.bind(
                        this,
                        'Are you sure you want to end match?',
                        this.forceMatchEnd.bind(this)
                      )}
                    >
                      <span className="left-icon left-meta-icon">
                        <Icon size="1.5em" icon="stop" />
                      </span>
                      End Match
                    </MenuItem>
                  </DropdownButton>
                  <Button
                    href="#"
                    bsStyle="danger"
                    className="dismiss"
                    //onClick={this.sendBallEventTest.bind(this)}
                    onClick={this.props.sendUndo}
                  >
                    Undo
                  </Button>
                  <Button
                    href="#"
                    className={
                      'save ' +
                      (!this.state.inProgress ? '' : 'loading ') +
                      (!this.state.justFinished ? '' : 'ok')
                    }
                    bsStyle="success"
                    onClick={this.sendBallEvent.bind(this, true)}
                  >
                    {!this.state.inProgress && !this.state.justFinished ? (
                      <div>
                        <span>Save</span>
                        <span className="glyphicon glyphicon-chevron-right pull-right" />
                      </div>
                    ) : !this.state.justFinished ? (
                      <BubbleLoader
                        className="loader-on-component"
                        color="#6f6f6f"
                        size={2}
                      />
                    ) : (
                      <span className="glyphicon glyphicon-ok" />
                    )}
                  </Button>
                </ButtonGroup>
              </div>
            </div>
            <DialogModal
              heading={'Confirmation'}
              content={this.state.dialogContent}
              show={this.state.dialogShow}
              onConfirm={this.state.dialogOnConfirm}
              onHide={
                this.state.dialogOnHide ? this.state.dialogOnHide : dialogClose
              }
            />
          </div>
        ) // - playerStriker and related existence checks
        }
      </div>
    );
  }
}

class InningSetup extends React.Component {
  onSubmit() {
    this.props.sendInningEnd();
  }

  render() {
    return (
      <div className="ball-event-setup">
        <div className="row">
          <div className="col-xs-12 col-md-12">
            <span className="title">
              {!this.props.matchState.matchEnd
                ? 'End of Innings'
                : 'End of Match'}
            </span>
            <div className="col-xs-2 col-md-4" />
            <div className="well col-xs-8 col-md-4">
              <div className="">
                <ul className="list-inline center-block">
                  <li>
                    <span>
                      <b>{this.props.matchState.battingTeam.teamName}</b>
                    </span>
                  </li>
                  <li>
                    <span>-</span>
                  </li>
                  <li>
                    <span>
                      {this.props.matchState.score}/{
                        this.props.matchState.wickets
                      }
                    </span>
                  </li>
                </ul>
                <ul className="list-inline center-block">
                  <li>
                    <span>
                      ({this.props.matchState.overNumber}.{
                        this.props.matchState.ballNumber
                      }{' '}
                      overs / {this.props.matchState.maxOvers})
                    </span>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xs-2 col-md-4" />
          </div>
        </div>
        {this.props.readOnly ? null : (
          <div className="row">
            <div className="col-xs-1" />
            <div className="col-xs-10 ballevent-actions">
              <ButtonGroup justified className="ballevent-actions">
                <Button
                  href="#"
                  className={
                    'save ' + (!this.props.inProgress ? '' : 'loading ')
                  }
                  bsStyle="success"
                  onClick={this.onSubmit.bind(this)}
                >
                  {!this.props.inProgress ? (
                    !this.props.matchState.matchEnd ? (
                      <div>
                        <span>Start Next Innings</span>
                        <span className="glyphicon glyphicon-chevron-right pull-right" />
                      </div>
                    ) : (
                      <div>
                        <span>Close</span>
                      </div>
                    )
                  ) : (
                    <BubbleLoader
                      className="loader-on-component"
                      color="#6f6f6f"
                      size={2}
                    />
                  )}
                </Button>
              </ButtonGroup>
            </div>
            <div className="col-xs-1" />
          </div>
        )}
      </div>
    );
  }
}

class BallEventSetup extends React.Component {
  /* Method to prepare elements required for setting up ball event
  This elements are:
    1) Bowler
    2) Striker
    3) Non-Striker
  If match state has an empty field on either one of these,
  then the corresponding dialog-box will show up.
  */
  constructor(props) {
    super(props);
    this.state = {
      ballEvent: Object.assign({}, this.props.ballEvent),
      matchState: Object.assign({}, this.props.matchState)
    };
  }

  handleChange(ballEvent, matchState) {
    this.setState({
      ballEvent: ballEvent,
      matchState: matchState
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ballEvent: Object.assign({}, nextProps.ballEvent),
      matchState: Object.assign({}, nextProps.matchState)
    });
  }

  onSubmit() {
    this.props.handleChange(this.state.ballEvent, this.state.matchState);
  }

  isPowerPlayBowler(bowler) {
    return (
      this.props.matchState.powerPlayDetails
        .map(x => x.bowler.playerId)
        .indexOf(bowler.playerId) > -1
    );
  }

  render() {
    let ballEvent = this.state.ballEvent,
      playerStriker = ballEvent.playerStriker,
      playerNonStriker = ballEvent.playerNonStriker,
      playerCurrentBowler = ballEvent.playerCurrentBowler,
      bowlersToBowl = this.props.matchState.bowlersToBowl,
      isPowerPlay = ballEvent.isPowerPlay,
      powerPlayDetails = this.props.matchState.powerPlayDetails,
      bowlingOptions = bowlersToBowl,
      batsmenToBatOptions = this.props.matchState.batsmenToBat,
      fielderOptions = this.props.matchState.playersBowlingTeam,
      emptyContent = [
        {
          playerElementInProps: this.props.ballEvent.playerCurrentBowler,
          playerElement: playerCurrentBowler,
          playerElementFieldName: 'playerCurrentBowler',
          playerElementTag: 'Bowler',
          eventTitle: 'Select Bowler',
          options: bowlingOptions,
          statsFunction: x => x.bowlingFigures,
          labelFunction: x => (this.isPowerPlayBowler(x) ? 'P' : ''),
          disableFunction: x => isPowerPlay && this.isPowerPlayBowler(x)
        },
        {
          playerElementInProps: this.props.ballEvent.playerStriker,
          playerElement: playerStriker,
          playerElementFieldName: 'playerStriker',
          playerElementTag: 'Striker',
          eventTitle: 'New Batsman (Striker)',
          options: batsmenToBatOptions.filter(
            x => x.playerId !== (playerNonStriker && playerNonStriker.playerId)
          )
        },
        {
          playerElementInProps: this.props.ballEvent.playerNonStriker,
          playerElement: playerNonStriker,
          playerElementFieldName: 'playerNonStriker',
          playerElementTag: 'Non-Striker',
          eventTitle: 'New Batsman (Non-Striker)',
          options: batsmenToBatOptions.filter(
            x => x.playerId !== (playerStriker && playerStriker.playerId)
          )
        }
      ].filter(x => !x.playerElementInProps),
      steps = emptyContent.length;
    return (
      <div className="ball-event-setup">
        {!steps ? null : (
          <div className="col-xs-12">
            {this.state.ballEvent.ballNumber ? null : (
              <span className="title">
                {this.state.ballEvent.ballSequence
                  ? this.state.ballEvent.ballNumber == 6 ? 'New Over' : ''
                  : 'Start ' +
                    this.state.matchState.battingTeam.teamName +
                    ' Innings'}
              </span>
            )}
            <div className="well">
              {emptyContent.map(content => (
                <PlayerElementChange
                  ballEvent={this.state.ballEvent}
                  matchState={this.state.matchState}
                  handleChange={this.handleChange.bind(this)}
                  {...content}
                />
              ))}

              {/* show powerplay option only 
                if its first ball of over and 
                bowler selected is not a powerplay bowler */
              this.state.ballEvent.ballNumber !== 0 ? null : this.state
                .ballEvent.playerCurrentBowler &&
              this.isPowerPlayBowler(
                this.state.ballEvent.playerCurrentBowler
              ) ? null : (
                <div
                  className="svg-icon-wrapper"
                  onClick={() => {
                    let ballEvent = this.state.ballEvent;
                    ballEvent.isPowerPlay = !ballEvent.isPowerPlay;
                    if (
                      ballEvent.playerCurrentBowler &&
                      this.isPowerPlayBowler(ballEvent.playerCurrentBowler)
                    ) {
                      ballEvent.playerCurrentBowler = null;
                    }
                    this.setState({ ballEvent: ballEvent });
                  }}
                >
                  {!this.state.ballEvent.isPowerPlay ? (
                    <Icon size="2rem" icon="blank-checkbox" />
                  ) : (
                    <Icon size="2rem" icon="checked-box" />
                  )}
                  <span className="icon-right-label">Powerplay</span>
                  <span className="label label-danger">P</span>
                </div>
              )}
              <Button
                type="submit"
                disabled={
                  !(
                    ballEvent.playerStriker &&
                    ballEvent.playerNonStriker &&
                    ballEvent.playerCurrentBowler
                  )
                }
                className="btn btn-primary form-submit"
                onClick={this.onSubmit.bind(this)}
              >
                Continue scoring
              </Button>
            </div>
          </div>
        )}
      </div>
    );
  }
}

class PlayerElementChange extends React.Component {
  handleDropdownChange(field, eventKey, event) {
    let ballEvent = this.props.ballEvent,
      matchState = this.props.matchState,
      masterField = field.includes('Striker')
        ? 'batsmenToBat'
        : 'bowlersToBowl',
      playerObject = this.props.matchState[masterField].filter(
        x => x.playerId === eventKey
      )[0];
    ballEvent[field] = playerObject;
    matchState[field] = playerObject;
    this.props.handleChange(ballEvent, matchState);
  }

  render() {
    let elementValue = this.props.playerElement,
      elementTag = this.props.playerElementTag,
      elementFieldName = this.props.playerElementFieldName,
      playerSelectionOptions = this.props.options,
      statsFunction = this.props.statsFunction,
      labelFunction = this.props.labelFunction,
      disableFunction = this.props.disableFunction;

    return (
      <FormGroup controlId="new-over">
        <ControlLabel>{this.props.eventTitle}</ControlLabel>
        <ButtonToolbar>
          <PlayerSelectionMenu
            title={
              elementValue
                ? elementTag + ': ' + elementValue.name
                : 'Select ' + elementTag
            }
            dropDownOptions={playerSelectionOptions}
            statsFunction={statsFunction}
            labelFunction={labelFunction}
            disableFunction={disableFunction}
            handleDropdownChange={this.handleDropdownChange.bind(
              this,
              elementFieldName
            )}
          />
        </ButtonToolbar>
      </FormGroup>
    );
  }
}

class BallEventCollapse extends React.Component {}

export class PanelMaker extends React.Component {
  makePanelHeader() {
    if (!this.props.metaTag || this.props.metaTag.toString().length <= 6) {
      return (
        <div className="row">
          <div className="col-xs-12">
            <span>{this.props.panelTitle}</span>
            <span className="meta-tag">
              {this.props.metaTag == null || this.props.metaTag.length == 0
                ? ''
                : '(' + this.props.metaTag + ')'}
            </span>
          </div>
        </div>
      );
    } else {
      //* length header needs meta-tag on new line
      return (
        <div>
          <div className="row">
            <div className="col-xs-12">
              <span>{this.props.panelTitle}</span>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12 meta-tag">
              <span className="commentary-body">
                {this.props.metaTag == null || this.props.metaTag.length == 0
                  ? ''
                  : '(' + this.props.metaTag + ')'}
              </span>
            </div>
          </div>
        </div>
      );
    }
  }

  render() {
    const PanelBody = this.props.panelBody,
      PanelHeader = this.props.panelHeader;
    return (
      <Panel
        header={this.makePanelHeader()}
        eventKey={this.props.parentId}
        defaultExpanded={this.props.expanded}
        {...this.props}
      >
        <PanelBody {...this.props} />
      </Panel>
    );
  }
}

class RunsPanelElement extends React.Component {
  addRuns(tag) {
    this.props.addRuns(tag);
  }

  clearWidesAndByes() {
    // in case scorer needs to enable runs off bat
    let ballEvent = this.props.ballEvent;
    // Toggle the presence of runs in `type`
    ballEvent.runs.wideRuns = 0;
    ballEvent.runs.byesRuns = 0;
    this.props.handleChange(ballEvent);
  }

  render() {
    const scorerButtons = this.props.scorerButtons,
      wideRuns = this.props.ballEvent.runs.wideRuns,
      isWide = wideRuns > 0 ? true : false,
      isNb = this.props.ballEvent.runs.isNb || false,
      byes = this.props.ballEvent.runs.byesRuns,
      isBye = byes > 0 ? true : false,
      scoringDisabled = isWide || isBye;

    return (
      <div id="runs-panel">
        {isWide || isBye ? (
          <Alert
            bsStyle="warning"
            className="validation-info"
            onClick={this.clearWidesAndByes.bind(this)}
          >
            <span className="meta-text text-block">
              No runs to bat off {isWide ? 'wides' : 'byes'}.{' '}
            </span>
            <span className="meta-text text-block">
              <span className="bold-meta-text">Tap here</span> to clear{' '}
              {isWide ? 'wides' : 'byes'}
            </span>
          </Alert>
        ) : (
          <ul className="list-inline">
            {scorerButtons.map(tag => (
              <li key={tag}>
                <ScorerButtonComponent
                  buttonTag={tag}
                  addRuns={this.addRuns.bind(this)}
                  buttonActive={this.props.ballEvent.runs.batrunsScored === tag}
                />
              </li>
            ))}
          </ul>
        )}
      </div>
    );
  }
}

class ExtrasPanelElement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ballEvent: this.props.ballEvent
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ ballEvent: nextProps.ballEvent });
  }

  handleChange(type) {
    let ballEvent = this.state.ballEvent;
    // Toggle the presence of runs in `type`
    ballEvent.runs[type] = ballEvent.runs[type] ? false : true;
    if (type !== 'isNb') {
      ballEvent.runs[type] = ballEvent.runs[type] ? 1 : 0;
    }
    this.props.handleChange(ballEvent);
  }

  handleIncrementDecrement(type, addendum) {
    console.log(type + addendum);
    let ballEvent = this.state.ballEvent;
    ballEvent.runs[type] = ballEvent.runs[type] + addendum;
    this.props.handleChange(ballEvent);
  }

  nonEditableButton(tag, source) {
    let isActive = this.state.ballEvent.runs[source];
    return (
      <div
        className="non-editable-btn"
        onClick={this.handleChange.bind(this, source)}
      >
        <span className={'ball-circle ' + (isActive ? 'boundary' : '')}>
          <p>{tag}</p>
        </span>
      </div>
    );
  }

  editableButton(tag, source) {
    let isActive = this.state.ballEvent.runs[source] > 0 ? true : false,
      content = this.state.ballEvent.runs[source];
    return (
      <Fade component="ul" className="list-inline">
        {!isActive ? null : (
          <li
            className="non-editable-btn narrow"
            key="number-down"
            onClick={this.handleIncrementDecrement.bind(this, source, -1)}
          >
            {<button className={'btn btn-danger'}>-</button>}
          </li>
        )}
        <li
          className="non-editable-btn"
          onClick={this.handleChange.bind(this, source)}
        >
          <span className={'ball-circle ' + (isActive ? 'boundary' : '')}>
            <p>
              {content > 1 ? content : ''}
              {tag}
            </p>
          </span>
        </li>
        {!isActive ? null : (
          <li
            className="non-editable-btn narrow "
            key="number-up"
            onClick={this.handleIncrementDecrement.bind(this, source, 1)}
          >
            {<button className={'btn btn-success '}>+</button>}
          </li>
        )}
      </Fade>
    );
  }

  handleDropdownChange(type, eventKey, event) {
    var ballEvent = this.state.ballEvent;
    ballEvent.runs[type] = eventKey + (type === 'wideRuns' ? 1 : 0);
    this.props.handleChange(ballEvent);
  }
  render() {
    const wideRuns = this.props.ballEvent.runs.wideRuns,
      isWide = wideRuns > 0 ? true : false,
      isNb = this.props.ballEvent.runs.isNb || false,
      byes = this.props.ballEvent.runs.byesRuns,
      isBye = byes > 0 ? true : false,
      isBatRunsScored = this.props.ballEvent.runs.batrunsScored > 0;
    return (
      <div id="extras-panel">
        <ul className="list-inline">
          {isNb || isBye || isBatRunsScored ? null : (
            <li key="wide-ball">{this.editableButton('Y', 'wideRuns')}</li>
          )}
          {isWide ? null : (
            <li key="no-ball">{this.nonEditableButton('N', 'isNb')}</li>
          )}
          {isWide || isBatRunsScored ? null : (
            <li key="bye-ball">{this.editableButton('B', 'byesRuns')}</li>
          )}
        </ul>
      </div>
    );
  }
}

class WicketPanelElement extends React.Component {
  constructor(props) {
    super(props);
    this.dismissalOptions = props.dismissalTypes;
    this.specialCases = props.dismissalTypes.filter(x => x.special);
    this.nonSpecialCases = props.dismissalTypes.filter(x => !x.special);
    this.fielderReqdCases = props.dismissalTypes.filter(x => x.fielderRequired);
    this.batsmanReqdCases = props.dismissalTypes.filter(x => x.batsmanRequired);
    this.bowlerReqdCases = props.dismissalTypes.filter(x => x.bowlerRequired);
    this.state = {
      ballEvent: this.props.ballEvent
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ ballEvent: nextProps.ballEvent });
  }

  handleChange(selectedOption) {
    let ballEvent = this.state.ballEvent,
      field = 'dismissalType';
    if (!selectedOption) {
      /* Happens when user clears select bar*/
      ballEvent.wicket.dismissalType = null;
    } else {
      ballEvent.wicket.batsmanDismissed =
        this.batsmanReqdCases.indexOf(selectedOption.type) > -1
          ? null
          : this.props.ballEvent.playerStriker.playerId;
      ballEvent.wicket.fielder = null;
      ballEvent.wicket.bowler =
        this.bowlerReqdCases.map(x => x.type).indexOf(selectedOption.type) > -1
          ? this.props.ballEvent.playerCurrentBowler.playerId
          : null;
      if (selectedOption.type === 'special') {
        ballEvent.wicket[field] = this.specialCases[0].type;
      } else {
        ballEvent.wicket[field] = selectedOption.type;
      }
    }
    this.props.handleChange(ballEvent);
    this.setState({ ballEvent: ballEvent });
  }

  handleDropdownChange(field, selectedOption) {
    var ballEvent = this.state.ballEvent;
    if (!selectedOption) {
      /* Happens when user clears select bar*/
      ballEvent.wicket[field] = null;
      this.props.handleChange(ballEvent);
      return;
    }
    ballEvent.wicket[field] = selectedOption.playerId;
    this.props.handleChange(ballEvent);
  }

  isSpecialSelected() {
    var returnval =
      this.specialCases
        .map(x => x.type)
        .indexOf(this.state.ballEvent.wicket.dismissalType) >= 0;
    return returnval;
  }

  render() {
    const selectedDismissalType = this.state.ballEvent.wicket.dismissalType,
      batsmanDismissed = this.state.ballEvent.wicket.batsmanDismissed,
      dismissalFielder = this.state.ballEvent.wicket.fielder,
      dismissalOptions = this.dismissalOptions,
      dismissalType = !selectedDismissalType
        ? ''
        : dismissalOptions.filter(x => x.type === selectedDismissalType)[0]
            .type;
    return (
      <div>
        <Select
          name={'wicket-options'}
          placeholder={'How Out?'}
          value={dismissalType}
          onChange={this.handleChange.bind(this)}
          options={dismissalOptions}
          valueKey={'type'}
          labelKey={'name'}
          clearable={true}
          searchable={false}
          autoBlur={true}
        />
        {this.batsmanReqdCases.map(x => x.type).indexOf(selectedDismissalType) <
        0 ? null : (
          <Select
            name={'batsman-dismissed'}
            placeholder={"Who's out?"}
            value={batsmanDismissed}
            options={[
              this.props.matchState.playerStriker,
              this.props.matchState.playerNonStriker
            ]}
            valueKey={'playerId'}
            labelKey={'name'}
            onChange={this.handleDropdownChange.bind(this, 'batsmanDismissed')}
            clearable={false}
            autoBlur={true}
          />
        )}
        {this.fielderReqdCases.map(x => x.type).indexOf(selectedDismissalType) <
        0 ? null : (
          <Select
            name={'fielder-selected'}
            placeholder={'Fielder'}
            value={dismissalFielder}
            options={this.props.matchState.playersBowlingTeam}
            valueKey={'playerId'}
            labelKey={'name'}
            onChange={this.handleDropdownChange.bind(this, 'fielder')}
            searchable={true}
            clearable={true}
            autoBlur={true}
          />
        )}
      </div>
    );
  }
}

class WicketItem extends React.Component {
  handleChange(event) {
    this.props.handleChange(this.props.dismissalType, event);
  }
  render() {
    return (
      <FormGroup>
        <Radio inline onClick={this.handleChange.bind(this)} {...this.props}>
          {this.props.dismissalName}
        </Radio>
      </FormGroup>
    );
  }
}

class PlayerSelectionMenu extends React.Component {
  componentWillMount() {
    this.id = newId();
  }
  handleDropdownChange(eventKey, event) {
    this.props.handleDropdownChange(eventKey, event);
  }
  render() {
    let dropDownOptions = JSON.parse(
        JSON.stringify(this.props.dropDownOptions)
      ),
      statsFunction = this.props.statsFunction,
      labelFunction = this.props.labelFunction,
      disableFunction = this.props.disableFunction,
      disabledOptions =
        disableFunction && dropDownOptions.filter(tag => disableFunction(tag)),
      enabledOptions = disableFunction
        ? dropDownOptions.filter(tag => !disableFunction(tag))
        : dropDownOptions;
    return (
      <DropdownButton
        title={this.props.title}
        id={this.id}
        onSelect={this.handleDropdownChange.bind(this)}
      >
        {enabledOptions.map(tag => (
          <MenuItem
            className="dropdown-item"
            eventKey={tag.playerId}
            key={tag.playerId}
          >
            <span>{tag.name}</span>
            <span className="label label-danger pull-right">
              {labelFunction ? labelFunction(tag) : null}
            </span>
            <span>{statsFunction ? statsFunction(tag) : null}</span>
          </MenuItem>
        ))}
        {disabledOptions && disabledOptions.length > 0 ? (
          <MenuItem divider />
        ) : null}
        {disabledOptions && disabledOptions.length > 0 ? (
          <MenuItem header>Can't Bowl</MenuItem>
        ) : null}
        {disabledOptions &&
          disabledOptions.map(tag => (
            <MenuItem className="dropdown-item" disabled>
              <span>{tag.name}</span>
              <span className="label label-danger pull-right">
                {labelFunction ? labelFunction(tag) : null}
              </span>
              <span>{statsFunction ? statsFunction(tag) : null}</span>
            </MenuItem>
          ))}
      </DropdownButton>
    );
  }
}

class ScorerButtonComponent extends React.Component {
  addRuns(event) {
    var runs = this.props.buttonActive ? 0 : this.props.buttonTag;
    this.props.addRuns(runs);
  }

  render() {
    return (
      <div className="run-buttons" onClick={this.addRuns.bind(this)}>
        <span
          className={
            'ball-circle ' + (this.props.buttonActive ? 'boundary' : '')
          }
        >
          <p>{this.props.buttonTag || '.'}</p>
        </span>
      </div>
    );
  }
}

export default BallEvent;
