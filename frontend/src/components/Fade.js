import React from 'react';
import { CSSTransitionGroup } from 'react-transition-group';
import FlipMove from 'react-flip-move';
import { ListGroup } from 'react-bootstrap';

export const Fade = ({ children, ...props }) => (
  <CSSTransitionGroup
    {...props}
    transitionName="fade"
    transitionEnterTimeout={700}
    transitionLeaveTimeout={500}
  >
    {children}
  </CSSTransitionGroup>
);

export const FadeWait = ({ children, ...props }) => (
  <CSSTransitionGroup
    {...props}
    transitionName="fade"
    transitionEnterTimeout={700}
    transitionLeaveTimeout={500}
  >
    {children}
  </CSSTransitionGroup>
);

export const FlipFade = ({ children, ...props }) => (
  <FlipMove
    duration={500}
    easing="ease-out"
    enterAnimation="fade"
    leaveAnimation="elevator"
    appearAnimation="fade"
    appearDuration={500}
    staggerDelayBy={40}
    {...props}
  >
    {children}
  </FlipMove>
);
