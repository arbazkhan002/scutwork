import React from 'react';
import CommentaryHead from './CommentaryHead';
import {
  getBallStringFontClass,
  wicketCommentary,
  getStringForm
} from './CommentaryHead';
import BallEvent from './BallEvent';
import { PanelMaker } from './BallEvent';
import { GameConstants } from './Constants';
import { Fade } from './Fade';
import { BubbleLoader } from 'react-css-loaders';
import ReactCSSTransitionReplace from 'react-css-transition-replace';
import { Button, Panel, PanelGroup } from 'react-bootstrap';
import {
  initializeBallEvent,
  withQuantSuffix,
  timeout,
  querify,
  logoutIfTokenExpired
} from './utils';
import { StyleConstants } from './Constants';

class Summary extends React.Component {
  constructor(props) {
    super(props);
    let matchState = this.props.matchState;
    this.matchId = matchState.matchId;
    this.overRequestBatchSize = 3;
    this.lastScroll = null;
    let overTill =
        matchState.overHistory.length > 0
          ? matchState.overHistory.slice(-1)[0].overNumber - 1
          : -1,
      nextScrollAt = {
        matchId: this.matchId,
        inningId: matchState.inningId,
        overTill: overTill
      };
    this.state = {
      addNewBallMode: this.props.isScorer,
      undoInProgress: false,
      addingOversInProgress: false,
      addedOverHistory: [],
      nextScrollAt: nextScrollAt
    };
    this.dismissalTypes = GameConstants.DISMISSAL_TYPES;
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleOnScroll.bind(this));
    if (this.isShorterThanPage()) {
      this.getMoreOvers();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleOnScroll);
  }

  isShorterThanPage() {
    let body = document.body,
      html = document.documentElement;
    return body.clientHeight <= html.clientHeight;
  }

  isBlankScroll() {
    let nextScrollAt = this.state.nextScrollAt;
    return nextScrollAt
      ? nextScrollAt.inningId == '1' && nextScrollAt.overTill < 0
      : false;
  }

  getMoreOvers() {
    const self = this,
      nextScrollAt = self.state.nextScrollAt;

    // Don't send obsolete requests
    if (self.isBlankScroll(nextScrollAt) || self.state.addingOversInProgress) {
      console.log(self.state.addingOversInProgress);
      return;
    }

    self.setState({ addingOversInProgress: true });
    this.fetchOverHistory(nextScrollAt)
      .then(data => {
        //Ignore redundant requests
        if (
          data.nextScrollAt.inningId == this.state.nextScrollAt.inningId &&
          data.nextScrollAt.overTill == this.state.nextScrollAt.overTill
        ) {
          return;
        }

        let overHistory = data.overHistory;
        self.setState({
          addedOverHistory: this.state.addedOverHistory.concat(overHistory),
          nextScrollAt: data.nextScrollAt,
          addingOversInProgress: false
        });
      })
      .catch(err => {
        self.setState({ addingOversInProgress: false });
      });
  }

  fetchOverHistory(nextScrollAt) {
    /* Check if match-state is already available
    which would mean set-up is not required */
    const self = this;
    const url = '/api/v1/match/' + nextScrollAt.matchId + '/overs';

    return timeout(
      5000,
      fetch(url + '?' + querify(nextScrollAt), {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '.*'
        }
      })
    )
      .then(function(response) {
        if (response.status === 404) {
          // redirect to match-setup
          return null;
        } else if (response.status > 400) {
          const error = new Error('Bad response from server');
          error.response = response;
          throw error;
        }
        return response.json();
      })
      .then(data => {
        return data;
      })
      .catch(err => {
        console.log(err);
        throw new Error(JSON.stringify(err));
      });
  }

  addNewBallEvent() {
    this.setState({ addNewBallMode: !this.state.addNewBallMode });
  }

  sendUndo(pastBallEvent) {
    logoutIfTokenExpired(this.props.logoutCallBack);
    if (!pastBallEvent) {
      return;
    }
    const self = this;
    let ballEvent = pastBallEvent,
      matchId = ballEvent.matchId;
    this.setState({ undoInProgress: true });
    fetch('/api/v1/match/' + matchId + '/undo_ball_event', {
      method: 'POST',
      body: JSON.stringify(ballEvent),
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('authToken')}`,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '.*'
      }
    })
      .then(function(response) {
        //response.status     //=> number 100–599
        //response.statusText //=> String
        //response.headers    //=> Headers
        //response.url        //=> String
        return response.json();
      })
      .then(data => {
        self.props.handleChange(data);
        self.setState({
          matchState: data,
          ballEvent: initializeBallEvent(data),
          undoInProgress: false
        });
      })
      .catch(ex => {
        console.log('Parsing Failed', ex);
        this.setState({ undoInProgress: false });
      });
  }

  handleChange(matchState, retainComponent) {
    this.props.handleChange(matchState);
    this.setState({
      addNewBallMode: retainComponent || this.props.isScorer
    });
  }

  handleOnScroll() {
    // http://stackoverflow.com/questions/9439725/javascript-how-to-detect-if-browser-window-is-scrolled-to-bottom
    let scrollTop =
        (document.documentElement && document.documentElement.scrollTop) ||
        document.body.scrollTop,
      scrollHeight =
        (document.documentElement && document.documentElement.scrollHeight) ||
        document.body.scrollHeight,
      clientHeight =
        document.documentElement.clientHeight || window.innerHeight,
      scrolledToBottom =
        Math.ceil(scrollTop + clientHeight + 64) >= scrollHeight;

    if (scrolledToBottom) {
      this.getMoreOvers();
    }
  }

  render() {
    const scorerButtons = this.props.scorerButtons,
      matchState = this.props.matchState,
      ballEvent = initializeBallEvent(matchState),
      playerStriker = ballEvent.playerStriker,
      playerNonStriker = ballEvent.playerNonStriker,
      playerCurrentBowler = ballEvent.playerCurrentBowler,
      playerPreviousBowler = ballEvent.playerPreviousBowler,
      matchStateOverHistory = this.props.matchState.overHistory,
      overHistory = matchStateOverHistory.concat(this.state.addedOverHistory),
      lastBallSequenceForUndo =
        overHistory &&
        overHistory.length > 0 &&
        Math.max.apply(Math, overHistory[0].ballSequences),
      lastBallForUndo =
        overHistory &&
        overHistory.length > 0 &&
        overHistory[0].balls.filter(
          x => x.ballSequence === lastBallSequenceForUndo
        )[0];
    //overHistory = this.state.overHistory && this.state.overHistory.overHistory;
    console.log(lastBallForUndo);
    return (
      <div className="summary">
        <div id="score-summary">
          <div className="row bg-secondary animate-in-down my-2">
            <div className="col-xs-12">
              <BattingSummary
                batsmen={[
                  matchState.playerStriker,
                  matchState.playerNonStriker
                ]}
                onStrike={
                  matchState.playerStriker && matchState.playerStriker.playerId
                }
              />
            </div>
          </div>
          <div className="row bg-secondary animate-in-down my-2">
            <div className="col-xs-12">
              <BowlingSummary
                playerCurrentBowler={matchState.playerCurrentBowler}
                playerPreviousBowler={matchState.playerPreviousBowler}
              />
            </div>
          </div>
        </div>

        {matchStateOverHistory && matchStateOverHistory.length > 0 ? (
          <div className="row ballevent-collapse thin-panel">
            <div className="col-xs-12">
              <RecentOversPanel recentOvers={matchStateOverHistory} />
            </div>
          </div>
        ) : null}

        <div id="ball-event-commentary-heads" className="row">
          <div className="col-xs-12">
            <ReactCSSTransitionReplace
              transitionName="roll-up"
              transitionEnterTimeout={800}
              transitionLeaveTimeout={800}
            >
              {!this.props.isScorer ? null : (
                <BallEvent
                  key="ball-scoring"
                  matchState={matchState}
                  handleChange={this.handleChange.bind(this)}
                  dismissalTypes={this.dismissalTypes}
                  sendUndo={this.sendUndo.bind(this, lastBallForUndo)}
                  logoutCallBack={this.props.logoutCallBack}
                />
              )}
            </ReactCSSTransitionReplace>
          </div>

          {overHistory &&
            [...new Set(overHistory.map(item => item.inningId))].map(
              (inningId, inningIndex) => {
                let inningOverHistory = overHistory.filter(
                  x => x.inningId == inningId
                );
                return (
                  <div>
                    {inningOverHistory.map((over, overIndex) => {
                      // Going top to down in over history,
                      // first over in the list might be a blank.
                      // In that case, second over was last bowled
                      // If its first ball of this inning, then
                      // first over of previous Inning is undoable
                      let isLastBowledOver =
                          overIndex == 0 ||
                          (overIndex == 1 &&
                            inningOverHistory[0].ballSequences.length == 0),
                        isUndoableMatchOver =
                          (inningIndex == 0 && isLastBowledOver) ||
                          (inningIndex == 1 &&
                            matchState.ballSequence == 0 &&
                            isLastBowledOver);

                      return (
                        <div>
                          {inningIndex === 0 ? null : overIndex !== 0 ? null : (
                            <InningEndSnapshot over={over} />
                          )}
                          <OverSnapshot
                            sendUndo={this.sendUndo.bind(this)}
                            over={over}
                            undoInProgress={this.state.undoInProgress}
                            dismissalTypes={this.dismissalTypes}
                            allowUndo={
                              this.props.isScorer && isUndoableMatchOver
                            }
                            showOverEnd={
                              overIndex !== 0 ||
                              inningIndex !== 0 ||
                              matchState.inningEnd
                            }
                          />
                        </div>
                      );
                    })}
                  </div>
                );
              }
            )}
        </div>

        {!this.state.addingOversInProgress ? null : (
          <div className="row loader-wrapper">
            <BubbleLoader
              className="loader-on-component"
              color="#6f6f6f"
              size={4}
            />
          </div>
        )}

        <div id="page-bottom" />
      </div>
    );
  }
}

class OverSnapshot extends React.Component {
  render() {
    let over = this.props.over;

    return (
      <div className="over-history">
        {!this.props.showOverEnd ? null : (
          <div className="panel panel-default panel-body commentary-head over-end col-xs-12">
            <div className="panel-heading">
              <ul className="list-inline">
                <li>
                  <span className="bold-meta-text">
                    END OF OVER {over.overNumber + 1}
                  </span>
                </li>
                <li>
                  <span className="light-meta-text">|</span>
                </li>
                <li>
                  <span className="light-meta-text">
                    {over.runs} {withQuantSuffix(over.runs, 'run')}{' '}
                    {over.wickets} {withQuantSuffix(over.wickets, 'wkt')}
                  </span>
                </li>
                <li>
                  <span className="light-meta-text">|</span>
                </li>
                <li>
                  <span className="bold-meta-text">
                    {over.battingTeam.teamName} - {over.inningScore}/{
                      over.inningWickets
                    }
                  </span>
                </li>
                <li>
                  <span className="light-meta-text">|</span>
                </li>
                <li>
                  <span className="light-meta-text">
                    RR - {(over.inningScore / (over.overNumber + 1)).toFixed(2)}
                  </span>
                </li>
              </ul>
            </div>
            <div className="panel-body no-padding">
              <div className="col-xs-6 thin-border stat-cell">
                {[over.playerStriker, over.playerNonStriker]
                  .filter(x => x)
                  .map(batsman => (
                    <div>
                      <span className="col-xs-8">{batsman.name}</span>
                      <span className="col-xs-4">{batsman.score}</span>
                    </div>
                  ))}
              </div>
              <div className="col-xs-6 thin-border stat-cell">
                {[over.playerCurrentBowler].filter(x => x).map(bowler => (
                  <div>
                    <span className="col-xs-6">{bowler.name}</span>
                    <span className="col-xs-6">{bowler.score}</span>
                  </div>
                ))}
              </div>
            </div>
          </div>
        )}
        <Fade component="div" className="over-balls">
          {over.balls &&
            over.balls
              .slice()
              .reverse()
              .map((pastBallEvent, index) => (
                <div
                  className="col-xs-12"
                  key={'past-ball-' + pastBallEvent.ballSequence}
                >
                  <CommentaryHead
                    ballEvent={pastBallEvent}
                    dismissalTypes={this.props.dismissalTypes}
                  >
                    <div
                      onClick={() => this.props.sendUndo(pastBallEvent)}
                      className="action-symbol grey500 pull-right"
                    >
                      {index === 0 && this.props.allowUndo
                        ? (this.props.undoInProgress ? (
                            <BubbleLoader
                              className="loader-on-component"
                              color="#6f6f6f"
                              size={2}
                            />
                          ) : (
                            <div>
                              <span
                                className="glyphicon glyphicon-undo block-display"
                                title="Undo"
                              />Undo
                            </div>
                          ): null)
                        : null}
                    </div>
                  </CommentaryHead>
                </div>
              ))}
        </Fade>
      </div>
    );
  }
}

class InningEndSnapshot extends React.Component {
  render() {
    return (
      <div className="ball-event-setup">
        <div className="row">
          <div className="col-xs-12 col-md-12">
            <span className="title">End of Innings</span>
            <div className="col-xs-2 col-md-4" />
            <div className="well col-xs-8 col-md-4">
              <div className="">
                <ul className="list-inline center-block">
                  <li>
                    <span>
                      <b>{this.props.over.battingTeam.teamName}</b>
                    </span>
                  </li>
                  <li>
                    <span>-</span>
                  </li>
                  <li>
                    <span>
                      {this.props.over.inningScore}/{
                        this.props.over.inningWickets
                      }
                    </span>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xs-2 col-md-4" />
          </div>
        </div>
      </div>
    );
  }
}

class RecentOversPanel extends React.Component {
  render() {
    return (
      <PanelMaker
        panelId="recent-overs"
        panelTitle="Recent"
        panelBody={RecentOversBody}
        expanded={true}
        recentOvers={this.props.recentOvers}
      />
    );
  }
}

class RecentOversBody extends React.Component {
  getOverFraction(over, numBalls) {
    let fractionBalls = over.balls
        .slice()
        .reverse()
        .slice(0, numBalls),
      fractionOver = Object.assign({ balls: over.balls }, over);

    fractionOver.balls = fractionBalls;

    return fractionOver;
  }

  limitOvers(overs, numBalls) {
    // does reduce operation on overs
    // with accumulator being a
    // tuple of (aggregated overs so far, # balls left to add)
    // and initial value being (empty list, # balls to limit to)
    let limitedOvers = [],
      initialValueTuple = [limitedOvers, numBalls],
      reduceOperationResult = overs.reduce((acc, curr) => {
        return acc[1] == 0
          ? acc
          : acc[1] > curr.balls.length
            ? [acc[0].concat(curr), acc[1] - curr.balls.length]
            : [acc[0].concat(this.getOverFraction(curr, acc[1])), 0];
      }, initialValueTuple);
    return reduceOperationResult[0];
  }

  render() {
    let recentOvers = this.props.recentOvers,
      limitedRecent = this.limitOvers(recentOvers, 10);
    return (
      <div>
        {limitedRecent.map(over => (
          <span className="over-boundary inline-list">
            {over.balls
              .slice()
              .reverse()
              .map((ball, index) => {
                let ballString = getStringForm(ball);
                return (
                  <span
                    className={
                      ballString.color === StyleConstants.REGULAR_BALL
                        ? ballString.content === '.'
                          ? 'ball-no-circle'
                          : 'ball-circle'
                        : ballString.color === StyleConstants.WICKET_BALL
                          ? 'ball-circle wicket'
                          : 'ball-circle boundary'
                    }
                  >
                    <p className={getBallStringFontClass(ballString)}>
                      {ballString.content}
                    </p>
                  </span>
                );
              })}
          </span>
        ))}
      </div>
    );
  }
}

class AddBallModeSwapper extends React.Component {
  render() {
    const content = React.Children.toArray(this.props.children);
    return (
      <ReactCSSTransitionReplace {...this.props}>
        {content[this.props.addNewBallMode ? 1 : 0]}
      </ReactCSSTransitionReplace>
    );
  }
}

export class BattingSummary extends React.Component {
  wicketText(batsman) {
    if (!batsman.batDismissal) {
      return null;
    } else {
      return (
        <span className="center-block">
          {wicketCommentary(batsman.batDismissal)
            .filter(x => x)
            .splice(1)
            .join(' ')}
        </span>
      );
    }
  }
  render() {
    const batsmen = this.props.batsmen.filter(x => x),
      onStrike = this.props.onStrike;

    return (
      <table className="table stats player-match-stats batting-match-stats">
        <thead>
          <tr>
            <th>Batting</th>
            <th className="hidden-xs" />
            <th />
            <th>4s</th>
            <th>6s</th>
            <th>SR</th>
          </tr>
        </thead>
        {batsmen.map(batsman => (
          <tbody key={batsman.name}>
            <tr>
              <td>
                {batsman.name}
                {onStrike && onStrike === batsman.playerId ? '*' : ''}
              </td>
              <td className="hidden-xs">{this.wicketText(batsman)}</td>
              <td>
                <b>{batsman.runs || 0}</b>({batsman.ballsFaced || 0})
              </td>
              <td>{batsman.fours || 0}</td>
              <td>{batsman.sixes || 0}</td>
              <td>
                {(
                  (batsman.runs || 0) *
                  100 /
                  (batsman.ballsFaced || 1)
                ).toFixed(1)}
              </td>
            </tr>
            <tr className="visible-xs hidden-md">
              <td colSpan={'10'}>{this.wicketText(batsman)}</td>
            </tr>
          </tbody>
        ))}
      </table>
    );
  }
}

export class BowlingSummary extends React.Component {
  render() {
    let playerCurrentBowler = this.props.playerCurrentBowler,
      playerPreviousBowler = this.props.playerPreviousBowler;
    return (
      <table className="table">
        <thead>
          <tr>
            <th> Bowling</th>
            <th />
            <th>Y</th>
            <th>N</th>
          </tr>
        </thead>
        <tbody>
          {[playerCurrentBowler].filter(x => x).map(bowler => {
            return (
              <tr key={bowler.name}>
                <td>{bowler.name}</td>
                <td>
                  {[
                    (bowler.over || '0') +
                      (bowler.legalBallsCount > 0
                        ? '.' + (bowler.legalBallsCount || '0')
                        : ''),
                    bowler.maidens || '0',
                    bowler.runs || '0',
                    bowler.dismissals || '0'
                  ].join('-')}
                </td>
                <td>{bowler.wides}</td>
                <td>{bowler.nbs}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default Summary;
