import React from 'react';
import { Panel } from 'react-bootstrap';

class GameHeader extends React.Component {
  render() {
    const shortMatchState = this.props.shortMatchState,
      team1 = shortMatchState.battingTeam
        ? shortMatchState.battingTeam
        : shortMatchState.homeTeam,
      team2 = shortMatchState.bowlingTeam
        ? shortMatchState.bowlingTeam
        : shortMatchState.awayTeam,
      venue = shortMatchState.venue,
      date = shortMatchState.matchDate,
      time = shortMatchState.matchTime,
      gameGrp = shortMatchState.gameGroup;
    if (!(team1 && team2)) {
      return null;
    } else {
      return (
        <div>
          <Panel className="game-header">
            <div className="row">
              <div className="col-xs-9 text-left">
                <span className="header">
                  {[
                    gameGrp && gameGrp + ' Division',
                    ((venue && venue.name) || '') +
                      (venue &&
                        ((venue.type && ' (' + venue.type + ')') || '')),
                    venue && venue.city
                  ]
                    .filter(x => x)
                    .join(', ')}
                </span>
              </div>
              <div className="col-xs-3 text-right">
                <span className="header">
                  {date && time && date + ', ' + time}
                </span>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-4 col-md-6 text-left">
                {team1.teamName || '10x00'}
              </div>
              <div className="col-xs-8 col-md-6 text-right">
                {shortMatchState &&
                  shortMatchState.shortInningDetails
                    .filter(
                      inning => inning.battingTeam.teamName == team1.teamName
                    )
                    .map((inning, index) => {
                      let numBalls = inning.overs * 6 + inning.ballNumber;
                      return (
                        <div key={inning.inningId}>
                          <span className="team-score text-success">
                            {inning.score}/{inning.wickets}{' '}
                          </span>
                          <span>(</span>
                          <span className="team-score text-success">
                            {inning.overs}.{inning.ballNumber} ov
                          </span>
                          <span>, </span>
                          <span className="team-score text-success">
                            RR{' '}
                            {numBalls
                              ? (inning.score * 6 / numBalls).toFixed(2)
                              : '-'}
                          </span>
                          <span>)</span>
                        </div>
                      );
                    })}
              </div>
            </div>
            <div className="row">
              <div className="col-xs-6 text-left">
                {team2.teamName || '10x00'}
              </div>
              <div className="col-xs-6 text-right">
                {shortMatchState &&
                  shortMatchState.shortInningDetails
                    .filter(
                      inning => inning.battingTeam.teamName == team2.teamName
                    )
                    .map(inning => (
                      <div>
                        <span className="team-score">
                          {inning.score}/{inning.wickets}{' '}
                        </span>
                        <span className="ball-text">
                          ({inning.overs}.{inning.ballNumber} ov)
                        </span>
                      </div>
                    ))}
              </div>
            </div>
          </Panel>
        </div>
      );
    }
  }
}

export default GameHeader;
