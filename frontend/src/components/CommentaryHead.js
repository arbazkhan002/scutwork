import React from 'react';
import { withQuantSuffix, convertTimestamp } from './utils';
import {
  Button,
  Col,
  ControlLabel,
  Form,
  FormGroup,
  FormControl
} from 'react-bootstrap';
import ReactCSSTransitionReplace from 'react-css-transition-replace';
import Icon from './svgIcons';
import { StyleConstants } from './Constants';

export function getBallStringFontClass(ballString) {
  return ballString.content.length / 2 > 2
    ? 'fs-5'
    : ballString.content.length / 2 > 1
      ? 'fs-7'
      : ballString.content.length > 1 ? 'fs-12' : '';
}

export function getStringForm(ballEvent) {
  let ballString = [],
    regularBall = StyleConstants.REGULAR_BALL,
    boundaryBall = StyleConstants.BOUNDARY_BALL,
    wicketBall = StyleConstants.WICKET_BALL,
    ballColor = regularBall;

  if (ballEvent.runs.isNb) {
    ballString.push('N');
  }

  if (ballEvent.runs.wideRuns) {
    let text =
      ballEvent.runs.wideRuns > 1 ? ballEvent.runs.wideRuns + 'Y' : 'Y';
    ballString.push(text);
  }

  if (ballEvent.runs.byesRuns) {
    let text =
      ballEvent.runs.byesRuns > 1 ? ballEvent.runs.byesRuns + 'B' : 'B';
    ballString.push(text);
  }

  if (ballEvent.runs.batrunsScored > 0) {
    ballString.push(ballEvent.runs.batrunsScored.toString());
    if (ballEvent.runs.is4 || ballEvent.runs.is6) {
      ballColor = boundaryBall;
    }
  }

  if (ballEvent.wicket.dismissalType) {
    ballString.push('W');
    ballColor = wicketBall;
  }

  let ballContent = ballString.filter(x => x).join('+');
  return {
    color: ballColor,
    content: ballContent ? ballContent : '.'
  };
}

class CommentaryHead extends React.Component {
  constructor(props) {
    super(props);
    // Used to determine string representation in commentary head
    this.regularBall = StyleConstants.REGULAR_BALL;
    this.wicketBall = StyleConstants.WICKET_BALL;
    this.boundaryBall = StyleConstants.BOUNDARY_BALL;
    this.isEditable = props.isEditable;
    this.state = {
      addComment: false
    };
  }

  getDismissedBatsman(ballEvent) {
    let wicketData = ballEvent.wicket;
    return [ballEvent.playerStriker, ballEvent.playerNonStriker].filter(
      batsman => batsman.playerId === wicketData.batsmanDismissed
    )[0];
  }

  getWicketText(ballEvent) {
    let wicketData = ballEvent.wicket;
    if (!wicketData.dismissalType) {
      return '';
    }

    let dismissalData = this.props.dismissalTypes.filter(
        x => x.type === wicketData.dismissalType
      )[0],
      batsmanDismissed = this.getDismissedBatsman(ballEvent),
      batsmanRequired = dismissalData.batsmanRequired,
      fielderRequired = dismissalData.fielderRequired;

    return (
      'out! ' +
      (batsmanRequired ? batsmanDismissed.name + ' is ' : '') +
      dismissalData.name.toLowerCase() +
      (fielderRequired && wicketData.fielderName
        ? ' by ' + wicketData.fielderName
        : '') +
      '.'
    );
  }

  getExtrasText(ballEvent) {
    let extrasData = ballEvent.runs;

    if (!(extrasData.wideRuns || extrasData.isNb || extrasData.byesRuns)) {
      return '';
    }

    let extrasTextBuffer = [],
      wideBall =
        (extrasData.wideRuns > 1 ? extrasData.wideRuns + ' ' : '') +
        withQuantSuffix(extrasData.wideRuns, 'wide'),
      noBall = 'no ball',
      byesBall =
        (extrasData.byesRuns > 1 ? extrasData.byesRuns + ' ' : '') +
        withQuantSuffix(extrasData.byesRuns, 'bye');

    if (extrasData.isNb) {
      extrasTextBuffer.push(noBall);
    }
    if ((extrasData.byesRuns || 0) > 0) {
      extrasTextBuffer.push(byesBall);
    }
    if ((extrasData.wideRuns || 0) > 0) {
      extrasTextBuffer.push(wideBall);
    }

    // There would either by a byesBall or a wideBall, it can't be both so
    // picking first extra type that matches runsText type works!
    return extrasTextBuffer.join(' and ');
  }

  getRunsText(ballEvent) {
    let batRunsText = '',
      batRuns = ballEvent.runs.batrunsScored,
      isBoundary = ballEvent.runs.is4 || ballEvent.runs.is6;
    switch (true) {
      case batRuns === 4 && isBoundary:
        batRunsText = 'four!';
        break;
      case batRuns === 6 && isBoundary:
        batRunsText = 'six!';
        break;
      case batRuns === 1:
        batRunsText = 'one run';
        break;
      case batRuns > 0:
        batRunsText = batRuns + ' runs';
        break;
      default:
        break;
    }
    return batRunsText;
  }

  getCommentaryText(ballEvent) {
    let textForWicket = this.getWicketText(ballEvent),
      textForExtras = this.getExtrasText(ballEvent),
      textForRuns = this.getRunsText(ballEvent),
      commentText = [textForWicket, textForExtras, textForRuns]
        .filter(x => x)
        .join(', '),
      commentNormalized = commentText ? commentText : 'no run';

    // Boundaries or wicket get transformed to uppercase
    let parts = commentNormalized.split(/(\bout|four|six\b)/gi);
    for (var i = 1; i < parts.length; i += 2) {
      parts[i] = <b key={i}>{parts[i].toUpperCase()}</b>;
    }
    return <span className="commentary-text">{parts}</span>;
  }

  handleChange(event) {
    let ballEvent = Object.assign({}, this.props.ballEvent),
      inputValue = event.target.value;
    ballEvent.commentaryInput = inputValue;
    this.props.handleChange(ballEvent);
  }

  switchStrike() {
    let ballEvent = Object.assign({}, this.props.ballEvent);
    [ballEvent.playerStriker, ballEvent.playerNonStriker] = [
      ballEvent.playerNonStriker,
      ballEvent.playerStriker
    ];
    this.props.handleChange(ballEvent);
  }

  render() {
    const ballEvent = this.props.ballEvent,
      playerStriker = ballEvent.playerStriker,
      playerNonStriker = ballEvent.playerNonStriker,
      playerCurrentBowler = ballEvent.playerCurrentBowler,
      playerPreviousBowler = ballEvent.playerPreviousBowler,
      ballString = getStringForm(ballEvent),
      timeString = convertTimestamp(ballEvent.timestamp);
    return (
      <div className="panel panel-default panel-body commentary-head">
        <div>
          {this.props.dummy ? (
            <div class="center-block" onClick={this.props.addNewBallEvent}>
              <table className="commentary-body">
                <tbody>
                  <tr>
                    <td>
                      <span className="glyphicon glyphicon-plus-sign white" />
                    </td>
                    <td>Add a ball</td>
                  </tr>
                </tbody>
              </table>
            </div>
          ) : (
            <div>
              {!ballEvent.isPowerPlay ? null : (
                <div className="powerplay-sidestrip">
                  <span className="label">POWERPLAY</span>
                </div>
              )}
              {this.props.isEditable ? null : (
                <div>
                  <span className="pull-right meta-text small-text">
                    {timeString.split(',').slice(-1)[0]}
                  </span>
                </div>
              )}
              <div
                className={
                  'commentary-body ' +
                  (this.props.isEditable ? 'col-xs-10' : 'col-xs-12 no-padding')
                }
              >
                <tr className="list-inline">
                  <td>
                    <div className="over-meta-wrapper">
                      <ul className="list-inline">
                        <li>
                          <span className="match-over-text">
                            {ballEvent.overNumber +
                              '.' +
                              (ballEvent.ballNumber + 1)}{' '}
                          </span>
                        </li>
                        <li>
                          <span
                            className={
                              'ball-circle ' +
                              (ballString.color === this.regularBall
                                ? ''
                                : ballString.color === this.wicketBall
                                  ? 'wicket'
                                  : 'boundary')
                            }
                          >
                            <p className={getBallStringFontClass(ballString)}>
                              {ballString.content}
                            </p>
                          </span>
                        </li>
                      </ul>
                    </div>
                  </td>
                  <td>
                    <span className="match-over-encounter">
                      {playerCurrentBowler.name +
                        ' to ' +
                        playerStriker.name +
                        ', '}
                    </span>
                    <span className="match-over-encounter">
                      {this.getCommentaryText(ballEvent)}
                    </span>
                    {ballEvent && this.isEditable ? (
                      !this.state.addComment ? null : (
                        <Form
                          key="commentary-input"
                          horizontal
                          className="scorer-input"
                        >
                          <FormGroup controlId="commentary-input">
                            <FormControl
                              componentClass="textarea"
                              className="col-xs-10"
                              type="textarea"
                              placeholder="Add commentary"
                              value={ballEvent.commentaryInput}
                              onChange={this.handleChange.bind(this)}
                            />
                            <Col componentClass={ControlLabel} xs={2} />
                          </FormGroup>
                        </Form>
                      )
                    ) : (
                      <div className="commentary-input">
                        <span className="meta-text">
                          {ballEvent.commentaryInput}
                        </span>
                        <Col xs={2} />
                      </div>
                    )}
                  </td>
                </tr>
              </div>
              {!this.isEditable ? null : (
                <div className="col-xs-2 action-symbol pull-right">
                  <Button
                    className="btn-success btn-xs blank-icon"
                    onClick={() =>
                      this.setState({ addComment: !this.state.addComment })
                    }
                  >
                    {!ballEvent.commentaryInput ? (
                      <Icon size="1.5rem" icon="blank-comment" />
                    ) : (
                      <Icon size="1.5rem" icon="filled-comment" />
                    )}
                  </Button>
                  <span className="pull-right small-text meta-text">
                    Comment
                  </span>
                </div>
              )}
              {this.props.children}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export function wicketCommentary(wicket, striker, bowler) {
  if (wicket.dismissalType) {
    let batsmanDismissed = wicket.batsmanDismissed || striker,
      playerCurrentBowler = wicket.bowlerName || wicket.bowler || bowler,
      fielder = wicket.fielderName || wicket.fielder,
      bowlerTag = wicket.bowler ? 'b' : '',
      // Handle case of 'Bowled' so that it doesn't show as 'Rohit b b Faulkner'
      dismissal =
        wicket.dismissalType === bowlerTag ? '' : wicket.dismissalType;
    return [
      batsmanDismissed,
      dismissal,
      fielder,
      bowlerTag,
      playerCurrentBowler
    ];
  }
}

export default CommentaryHead;
