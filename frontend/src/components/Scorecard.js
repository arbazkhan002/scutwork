import React from 'react';
import { wicketCommentary } from './CommentaryHead';
import { Panel, PanelGroup } from 'react-bootstrap';
import { BubbleLoader } from 'react-css-loaders';
import { BattingSummary } from './Summary';
import { ordinalSuffixOf } from './utils';

class BattingScores extends BattingSummary {
  wicketText(batsman) {
    return (
      <span>
        {!batsman.batDismissal
          ? 'not out'
          : wicketCommentary(batsman.batDismissal)
              .filter(x => x)
              .splice(1)
              .join(' ')}
      </span>
    );
  }
}

class YetToBat extends React.Component {
  render() {
    return (
      <div className="row-display">
        <span className="bold-meta-text">Yet to Bat </span>
        <span className="meta-text">
          {this.props.batsmenToBat.map(batsman => batsman.name).join(', ')}
        </span>
      </div>
    );
  }
}

class FoW extends React.Component {
  render() {
    return (
      <div className="row-display">
        <span className="bold-meta-text">Fall of Wickets </span>
        {this.props.fow
          .map(
            (wicket, index) =>
              wicket.score +
              '/' +
              (index + 1) +
              ' (' +
              wicket.batsman.name +
              ', ' +
              wicket.over +
              '.' +
              wicket.ballNumber +
              ' overs)'
          )
          .join(' ')}
      </div>
    );
  }
}

class ExtrasScores extends React.Component {
  render() {
    let { wides, noBalls, byes } = this.props,
      extras = wides + noBalls + byes;
    return (
      <div className="row-display">
        <span>Extras</span>
        <p className="pull-right">
          {!extras
            ? '0'
            : extras +
              ' ( ' +
              (wides > 0 ? wides + 'Y ' : '') +
              (noBalls > 0 ? noBalls + 'N ' : '') +
              (byes > 0 ? byes + ' byes' : '') +
              ')'}
        </p>
      </div>
    );
  }
}

class Total extends React.Component {
  render() {
    let { score, wickets, ballNumber, over } = this.props;
    return (
      <div className="row-display">
        <b>
          <span>TOTAL </span>
          <p className="pull-right">
            {score + '/' + wickets + ' (' + over + '.' + ballNumber + ' overs)'}
          </p>
        </b>
      </div>
    );
  }
}

class BowlingScores extends React.Component {
  render() {
    let bowlers = this.props.bowlers;
    return (
      <div>
        <table className="table stats player-match-stats bowling-match-stats">
          <thead>
            <tr>
              <th> Bowling</th>
              <th>O</th>
              <th>M</th>
              <th>R</th>
              <th>W</th>
              <th>ECON</th>
              <th>0s</th>
              <th>Y</th>
              <th>N</th>
            </tr>
          </thead>
          <tbody>
            {bowlers.filter(x => x).map(bowler => (
              <tr>
                <td>{bowler.name}</td>
                <td>
                  {bowler.over || '0' + '.' + (bowler.legalBallsCount || '0')}
                </td>
                <td>{bowler.maidens || '0'}</td>
                <td>{bowler.runs || '0'}</td>
                <td>{bowler.dismissals || '0'}</td>
                <td>
                  {bowler.over * 6 + bowler.legalBallsCount !== 0
                    ? (
                        6.0 *
                        bowler.runs /
                        (bowler.over * 6.0 + bowler.legalBallsCount)
                      ).toFixed(2)
                    : '-'}
                </td>
                <td>{bowler.dotBallsCount}</td>
                <td>{bowler.wides}</td>
                <td>{bowler.nbs}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

class PowerPlayDetails extends React.Component {
  render() {
    let powerPlayDetails = this.props.powerPlayDetails;
    return (
      <div className="row-display col-xs-12 col-md-4 pull-right">
        <span className="center-block">
          Powerplay Overs <span className="label label-danger">P</span>{' '}
        </span>
        <table className="table">
          <thead>
            <tr>
              <th>O#</th>
              <th>Bowler</th>
              <th>R</th>
              <th>W</th>
            </tr>
          </thead>
          <tbody>
            {powerPlayDetails
              .sort((a, b) => a.overNumber - b.overNumber)
              .map(x => (
                <tr key={x.overNumber + 1}>
                  <td>{x.overNumber + 1}</td>
                  <td>{x.bowler.name}</td>
                  <td>{x.runs}</td>
                  <td>{x.wickets}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default class Scorecard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scorecard: null
    };
  }

  componentDidMount() {
    const self = this;
    self.fetchScorecard(this.props.matchId).then(scorecard => {
      this.setState({
        scorecard: scorecard
      });
    });
  }

  fetchScorecard(matchId) {
    return fetch('/api/v1/match/' + matchId + '/scorecard', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '.*'
      }
    })
      .then(function(response) {
        if (response.status >= 400) {
          throw new Error('error: ' + response.statusText);
        }
        return response.json();
      })
      .then(data => {
        return data;
      });
  }

  makePanelHeader(inning, index) {
    return (
      // Header should span entire width so that
      // complete heading is clickable
      <div className="row">
        <div className="col-xs-12">
          <span>
            {inning.battingTeam.teamName +
              ' ' +
              ordinalSuffixOf(index + 1) +
              ' innings'}
          </span>
          <span className="pull-right chevron-up-down" />
        </div>
      </div>
    );
  }

  render() {
    let scorecard = this.state.scorecard,
      homeTeam = scorecard && scorecard.homeTeam.teamName,
      awayTeam = scorecard && scorecard.awayTeam.teamName,
      content = scorecard && [
        {
          dropdownTag: homeTeam + ' Captain',
          targetField: 'captain1',
          labelKey: 'name',
          valueKey: 'playerId'
        },
        {
          dropdownTag: homeTeam + ' WicketKeeper',
          targetField: 'keeper1',
          labelKey: 'name',
          valueKey: 'playerId'
        },
        {
          dropdownTag: awayTeam + ' Captain',
          targetField: 'captain2',
          labelKey: 'name',
          valueKey: 'playerId'
        },
        {
          dropdownTag: awayTeam + ' WicketKeeper',
          targetField: 'keeper2',
          labelKey: 'name',
          valueKey: 'playerId'
        },
        {
          dropdownTag: 'Umpire 1',
          presetValue:
            scorecard.umpire1 &&
            scorecard.umpiringTeam1 &&
            scorecard.umpire1.name +
              ' (' +
              scorecard.umpiringTeam1.teamName +
              ')'
        },
        {
          dropdownTag: 'Umpire 2',
          presetValue:
            scorecard.umpire2 &&
            scorecard.umpiringTeam2 &&
            scorecard.umpire2.name +
              ' (' +
              scorecard.umpiringTeam2.teamName +
              ')'
        },
        {
          dropdownTag: 'Toss',
          presetValue:
            scorecard.tossWonBy.teamName +
            ' won the toss and decided to ' +
            (scorecard.teamBattingFirst.teamName == scorecard.tossWonBy.teamName
              ? ' bat'
              : ' bowl') +
            ' first'
        }
      ],
      filledContent =
        (content &&
          content.filter(
            formContent =>
              formContent.presetValue || scorecard[formContent.targetField]
          )) ||
        [];
    return (
      <div>
        {!scorecard ? (
          <BubbleLoader color="#6f6f6f" size={5} />
        ) : (
          <PanelGroup>
            {scorecard.inningsDetails.map((inning, index) => {
              return (
                <Panel
                  collapsible
                  header={this.makePanelHeader(inning, index)}
                  key={scorecard.matchId + '-' + inning.inningId}
                  eventKey={
                    scorecard.matchId + '-' + inning.inningId + '-collapse'
                  }
                  defaultExpanded={
                    index === scorecard.inningsDetails.length - 1
                  }
                >
                  <InningScorecard inning={inning} />
                </Panel>
              );
            })}
          </PanelGroup>
        )}
        {!filledContent.length ? null : (
          <div className="row">
            <div className="col-xs-10">
              <h4>Match Details</h4>
              <table className="table property-table">
                <tbody>
                  {filledContent.map(formContent => {
                    return (
                      <tr key={formContent.dropdownTag}>
                        <td className="property-label">
                          {formContent.dropdownTag}
                        </td>
                        <td className="property-value">
                          {formContent.presetValue
                            ? formContent.presetValue
                            : scorecard[formContent.targetField][
                                formContent.labelKey
                              ]}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        )}
      </div>
    );
  }
}

class InningScorecard extends React.Component {
  render() {
    let inning = this.props.inning,
      bowlerDetails = inning.bowlerDetails,
      bowlingOrder = inning.bowlerIds.map(x => x.toString()),
      orderedBowlerDetails = bowlerDetails.sort(
        (a, b) =>
          bowlingOrder.indexOf(a.playerId.toString()) -
          bowlingOrder.indexOf(b.playerId.toString())
      );
    return (
      <div>
        <BattingScores batsmen={inning.batsmanDetails} />
        <YetToBat batsmenToBat={inning.batsmenToBat} />
        <ExtrasScores
          wides={inning.wides}
          noBalls={inning.nbs}
          byes={inning.byes}
        />
        <Total
          score={inning.score}
          over={inning.overs}
          ballNumber={inning.ballNumber}
          wickets={inning.fow.length}
        />
        {inning.fow ? <FoW fow={inning.fow} /> : null}
        {inning.powerPlayDetails && inning.powerPlayDetails.length > 0 ? (
          <PowerPlayDetails powerPlayDetails={inning.powerPlayDetails} />
        ) : null}
        <BowlingScores bowlers={orderedBowlerDetails} />
      </div>
    );
  }
}
