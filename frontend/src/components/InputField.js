import React from 'react';

export default class InputField extends React.Component {
  render() {
    return (
      <div className="Input">
        <input
          id={this.props.name}
          autocomplete="false"
          required
          type={this.props.type}
          placeholder={this.props.placeholder}
        />
        <label for={this.props.name} />
      </div>
    );
  }
}
