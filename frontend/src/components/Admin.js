import React from 'react';
import { Button, FormControl, FormGroup } from 'react-bootstrap';
import { timeout, waitFor, getLocationLastSegment } from './utils';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Link, Redirect, Route, Switch } from 'react-router-dom';
import { BubbleLoader } from 'react-css-loaders';

function getProducts() {
  const products = [];
  const startId = products.length;
  for (let i = 0; i < 12; i++) {
    const id = startId + i;
    products.push({
      id: id,
      name: 'Item name ' + id,
      price: Math.floor(Math.random() * 2000 + 1)
    });
  }
  return products;
}

export default class AdminPage extends React.Component {
  constructor(props) {
    super(props);
    let MATCH_SERVICE_URL = '/admin/api/v1/match',
      TEAM_SERVICE_URL = '/admin/api/v1/team';
    this.MATCH_SERVICE = {
      getAll: MATCH_SERVICE_URL,
      get: id => MATCH_SERVICE_URL + '/' + id,
      create: MATCH_SERVICE_URL,
      update: id => MATCH_SERVICE_URL + '/' + id,
      delete: id => MATCH_SERVICE_URL + '/' + id
    };

    this.state = {
      showSideBar: true,
      loading: false,
      pageData: null
    };
  }

  toggleSideBar() {
    this.setState({
      showSideBar: !this.state.showSideBar
    });
  }

  componentWillMount() {
    if (!this.props.isAdmin) {
      this.props.getLogin();
    }
  }

  resetAuthorization() {
    this.props.resetAuthorization();
  }

  render() {
    if (!this.props.isAdmin) return null;
    return (
      <div id="wrapper" className={this.state.showSideBar ? '' : 'toggled'}>
        <div id="sidebar-wrapper">
          <ul className="sidebar-nav">
            <li className="sidebar-brand">
              <a href="#">League Admin</a>
            </li>
            <li>
              <Link to={this.props.match.path + '/match'}>Matches</Link>
            </li>
            <li>
              <Link to={this.props.match.path + '/team'}>Teams</Link>
            </li>
            <li>
              <Link to={this.props.match.path + '/venue'}>Venues</Link>
            </li>
          </ul>
        </div>
        <div id="page-content-wrapper">
          <div className="container-fluid">
            {['team', 'players'].indexOf(getLocationLastSegment()) ==
            -1 ? null : (
              <div className="row">
                <FileUpload />
              </div>
            )}
            <div className="row">
              <div className="col-lg-12">
                <h1>Table</h1>
                {!this.state.loading ? (
                  <div>
                    <AdminTable
                      routeMatch={this.props.match}
                      resetAuthorization={this.resetAuthorization.bind(this)}
                      data={this.state.pageData}
                    />
                    <Button
                      bsStyle="info"
                      onClick={this.toggleSideBar.bind(this)}
                    >
                      {this.state.showSideBar ? 'Hide' : 'Show'} Menu
                    </Button>
                  </div>
                ) : (
                  <div className="row loader-wrapper">
                    <BubbleLoader
                      className="loader-on-component"
                      color="#6f6f6f"
                      size={4}
                    />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class AdminTable extends React.Component {
  render() {
    return (
      <Switch>
        <Route
          path={this.props.routeMatch.path + '/match'}
          render={routeProps => (
            <MatchesTable
              resetAuthorization={this.props.resetAuthorization}
              {...routeProps}
            />
          )}
        />
        <Route
          exact
          path={this.props.routeMatch.path + '/team'}
          render={routeProps => (
            <TeamsTable
              resetAuthorization={this.props.resetAuthorization}
              {...routeProps}
            />
          )}
        />
        <Route
          exact
          path={this.props.routeMatch.path + '/team/:teamId/players'}
          render={routeProps => (
            <PlayersTable
              resetAuthorization={this.props.resetAuthorization}
              {...routeProps}
            />
          )}
        />
        <Route
          path={this.props.routeMatch.path + '/venue'}
          render={routeProps => (
            <VenueTable
              resetAuthorization={this.props.resetAuthorization}
              {...routeProps}
            />
          )}
        />
      </Switch>
    );
  }
}

class EntityTable extends React.Component {
  componentDidMount() {
    this.refreshState();
  }

  refreshState() {
    const self = this;
    self.setState({ loading: true, invalidRequest: false });
    self
      .fetchData(self.SERVICE.getAll)
      .then(data => {
        self.setState({
          data: data,
          loading: false
        });
      })
      .catch(err => {
        self.setState({ loading: false });
      });
  }

  nameFormatter(data, cell) {
    return cell ? `<p>${cell[data]}</p>` : null;
  }

  fetchData(url) {
    const self = this;
    return timeout(
      10000,
      fetch(url, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '.*',
          Authorization: `Bearer ${localStorage.getItem('adminToken')}`
        }
      })
    )
      .then(function(response) {
        if (response.status === 401) {
          // redirect to login
          self.props.resetAuthorization();
          return null;
        } else if (response.status > 400) {
          const error = new Error('Bad response from server');
          error.response = response;
          throw error;
        }
        return response.json();
      })
      .then(data => {
        return data;
      })
      .catch(err => {
        console.log(err);
        throw new Error(JSON.stringify(err));
      });
  }

  sendData(url, method, data) {
    const self = this;
    self.setState({ invalidRequest: false });
    return timeout(
      10000,
      fetch(url, {
        method: method,
        body: JSON.stringify(data),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '.*',
          Authorization: `Bearer ${localStorage.getItem('adminToken')}`
        }
      })
    )
      .then(function(response) {
        if (response.status === 404) {
          // redirect to match-setup
          return null;
        } else if (response.status > 400) {
          const error = new Error('Bad response from server');
          error.response = response;
          throw error;
        }
        return response.json();
      })
      .then(respdata => {
        return respdata;
      })
      .catch(err => {
        self.setState({ invalidRequest: true });
        waitFor(2000).then(() => {
          self.setState({ invalidRequest: false });
        });
        throw new Error(err);
      });
  }
}

class MatchesTable extends EntityTable {
  constructor(props) {
    super(props);
    let SERVICE_URL = '/admin/api/v1/match';
    this.SERVICE = {
      getAll: SERVICE_URL,
      get: id => SERVICE_URL + '/' + id,
      create: SERVICE_URL,
      update: id => SERVICE_URL + '/' + id,
      delete: id => SERVICE_URL + '/' + id
    };
    this.state = {
      data: null,
      loading: false
    };
  }

  pinChange(row, cell, event) {
    //
  }

  componentWillReceiveProps(nextProps) {
    // extract team Ids from nested object
    let flattenedData = this.flattenedData(nextProps.data);
    if (flattenedData) {
      this.setState({ data: flattenedData });
    }
  }

  flattenedData(stateData) {
    let data = Object.assign({}, stateData);
    if (data && data['matches'] && data['matches'].length > 0) {
      let i;
      for (i = 0; i < data['matches'].length; i++) {
        let match = data['matches'][i];
        if (match['homeTeam'] && match['awayTeam']) {
          data['matches'][i]['team1Id'] = match['homeTeam']['teamId'];
          data['matches'][i]['team2Id'] = match['awayTeam']['teamId'];
        }
        if (match['umpiringTeam1'] && match['umpiringTeam2']) {
          data['matches'][i]['umpiringTeam1Id'] =
            match['umpiringTeam1']['teamId'];
          data['matches'][i]['umpiringTeam2Id'] =
            match['umpiringTeam2']['teamId'];
        }
      }

      return data;
    } else {
      return stateData;
    }
  }

  inputFormatter(cell, row) {
    return (
      <FormGroup controlId="matchPin">
        <FormControl
          className="col-xs-10"
          type="text"
          placeholder="New PIN"
          onChange={this.pinChange.bind(this, row, cell)}
        />
      </FormGroup>
    );
  }

  onCellEdit(row, fieldName, value) {
    // row -- raw record
    // fieldName -- name of dataField
    // value -- value put on cell
    const data = this.state.data;
    let rowIdx;
    const targetRow = data['matches'].find((record, i) => {
      if (record.matchId === row.matchId) {
        rowIdx = i;
        return true;
      }
      return false;
    });
    if (targetRow) {
      targetRow[fieldName] = value;
      let tgt = Object.assign({}, targetRow);
      data['matches'][rowIdx] = tgt;
      this.sendData(this.SERVICE.update(targetRow.matchId), 'PUT', targetRow)
        .then(x => this.setState({ data }))
        .catch(err => {
          //
        });
    }
  }

  onAddRow(row) {
    let data = this.state.data;
    this.sendData(this.SERVICE.create, 'POST', row)
      .then(x => {
        data['matches'].push(row);
        this.setState({ data });
      })
      .catch(err => {
        //
      });
  }

  onDeleteRow(ids, rows) {
    let data = this.state.data;
    this.sendData(this.SERVICE.delete(rows[0].matchId), 'DELETE', rows[0])
      .then(x => {
        data['matches'] = data['matches'].filter(match => {
          return match.matchId !== rows[0].matchId;
        });
        this.setState({ data });
      })
      .catch(err => {
        //
      });
  }

  render() {
    const cellEditProp = {
      mode: 'click'
    };
    const selectRow = {
      mode: 'checkbox',
      clickToSelect: true
    };
    let componentData = this.flattenedData(this.state.data);

    return !componentData ? null : (
      <div>
        <div>
          {this.state.invalidRequest == false ? null : (
            <div class="alert alert-danger" role="alert">
              Action Failed!
            </div>
          )}
        </div>
        <BootstrapTable
          data={componentData['matches']}
          selectRow={selectRow}
          remote={this.remote}
          insertRow
          deleteRow
          search
          pagination
          cellEdit={cellEditProp}
          options={{
            onCellEdit: this.onCellEdit.bind(this),
            onDeleteRow: this.onDeleteRow.bind(this),
            onAddRow: this.onAddRow.bind(this),
            sizePerPage: 50
          }}
        >
          <TableHeaderColumn
            dataField="matchId"
            isKey={true}
            search={true}
            multiColumnSearch={true}
          >
            Match ID
          </TableHeaderColumn>
          <TableHeaderColumn dataField="team1Id">Home (ID)</TableHeaderColumn>
          <TableHeaderColumn
            dataField="homeTeam"
            dataFormat={this.nameFormatter.bind(this, 'teamName')}
            hiddenOnInsert
          >
            Home (Name)
          </TableHeaderColumn>
          <TableHeaderColumn dataField="team2Id">Away (ID)</TableHeaderColumn>
          <TableHeaderColumn
            dataField="awayTeam"
            dataFormat={this.nameFormatter.bind(this, 'teamName')}
            hiddenOnInsert
          >
            Away (Name)
          </TableHeaderColumn>
          <TableHeaderColumn dataField="umpiringTeam1Id">
            Umpire1 (ID)
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="umpiringTeam1"
            dataFormat={this.nameFormatter.bind(this, 'teamName')}
            hiddenOnInsert
          >
            Umpire1 (Name)
          </TableHeaderColumn>
          <TableHeaderColumn dataField="umpiringTeam2Id">
            Umpire2 (ID)
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="umpiringTeam2"
            dataFormat={this.nameFormatter.bind(this, 'teamName')}
            hiddenOnInsert
          >
            Umpire2 (Name)
          </TableHeaderColumn>
          <TableHeaderColumn dataField="venue">Venue</TableHeaderColumn>
          <TableHeaderColumn dataField="maxOvers">Max Overs</TableHeaderColumn>
          <TableHeaderColumn
            dataField="matchPin"
            dataFormat={this.inputFormatter.bind(this)}
          >
            Match PIN
          </TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

class TeamsTable extends EntityTable {
  constructor(props) {
    super(props);
    let SERVICE_URL = '/admin/api/v1/team';
    this.SERVICE = {
      getAll: SERVICE_URL,
      get: id => SERVICE_URL + '/' + id,
      create: SERVICE_URL,
      update: id => SERVICE_URL + '/' + id,
      delete: id => SERVICE_URL + '/' + id
    };
    this.state = {
      data: null,
      loading: false
    };
  }

  rosterFormatter(cell, row) {
    return (
      <Button className="col-xs-12">
        <Link to={this.props.match.path + '/' + row.teamId + '/players'}>
          <span class="glyphicon glyphicon-edit" aria-hidden="true" />
          <span className="icon-right-label">Edit Roster</span>
        </Link>
      </Button>
    );
  }

  onCellEdit(row, fieldName, value) {
    // row -- raw record
    // fieldName -- name of dataField
    // value -- value put on cell
    const data = Object.assign({}, this.state.data);

    let rowIdx;
    const targetRow = data['teams'].find((record, i) => {
      if (record.teamId === row.teamId) {
        rowIdx = i;
        return true;
      }
      return false;
    });
    if (targetRow) {
      targetRow[fieldName] = value;
      let id = targetRow.teamId,
        tgt = Object.assign({}, targetRow);
      this.sendData(this.SERVICE.update(id), 'PUT', targetRow)
        .then(x => {
          data['teams'][rowIdx] = tgt;
          this.setState({ data: data });
        })
        .catch(err => {
          //
        });
    }
  }

  onAddRow(row) {
    let data = this.state.data;

    this.sendData(this.SERVICE.create, 'POST', row)
      .then(x => {
        data['teams'].push(row);
        this.setState({ data });
      })
      .catch(err => {
        //
      });
  }

  onDeleteRow(ids, rows) {
    let data = this.state.data;
    this.sendData(this.SERVICE.delete(rows[0].teamId), 'DELETE', rows[0]).then(
      x => {
        data['teams'] = data['teams'].filter(team => {
          return team.teamId !== rows[0].teamId;
        });
        this.setState({ data });
      }
    );
  }

  render() {
    const cellEditProp = {
      mode: 'click'
    };
    const selectRow = {
      mode: 'checkbox',
      clickToSelect: true
    };
    let componentData = this.state.data;
    return !componentData ? null : (
      <div>
        <div>
          {this.state.invalidRequest == false ? null : (
            <div class="alert alert-danger" role="alert">
              Action Failed!
            </div>
          )}
        </div>
        <BootstrapTable
          data={this.state.data['teams']}
          selectRow={selectRow}
          remote={this.remote}
          insertRow
          deleteRow
          search
          pagination
          cellEdit={cellEditProp}
          options={{
            onCellEdit: this.onCellEdit.bind(this),
            onDeleteRow: this.onDeleteRow.bind(this),
            onAddRow: this.onAddRow.bind(this),
            sizePerPage: 50
          }}
        >
          <TableHeaderColumn
            dataField="teamId"
            isKey={true}
            search={true}
            multiColumnSearch={true}
          >
            ID
          </TableHeaderColumn>
          <TableHeaderColumn dataField="teamName">Name</TableHeaderColumn>
          <TableHeaderColumn
            dataField="rosterView"
            dataFormat={this.rosterFormatter.bind(this)}
            hiddenOnInsert
          />
        </BootstrapTable>
      </div>
    );
  }
}

class VenueTable extends EntityTable {
  constructor(props) {
    super(props);
    let SERVICE_URL = '/admin/api/v1/venue';
    this.SERVICE = {
      getAll: SERVICE_URL,
      get: id => SERVICE_URL + '/' + id,
      create: SERVICE_URL,
      update: id => SERVICE_URL + '/' + id,
      delete: id => SERVICE_URL + '/' + id
    };
    this.state = {
      data: null,
      loading: false
    };
  }

  onCellEdit(row, fieldName, value) {
    // row -- raw record
    // fieldName -- name of dataField
    // value -- value put on cell
    const data = Object.assign({}, this.state.data);

    let rowIdx;
    const targetRow = data['venues'].find((record, i) => {
      if (record.venueId === row.venueId) {
        rowIdx = i;
        return true;
      }
      return false;
    });
    if (targetRow) {
      targetRow[fieldName] = value;
      let id = targetRow.venueId,
        tgt = Object.assign({}, targetRow);
      this.sendData(this.SERVICE.update(id), 'PUT', targetRow)
        .then(x => {
          data['venues'][rowIdx] = tgt;
          this.setState({ data: data });
        })
        .catch(err => {
          //
        });
    }
  }

  onAddRow(row) {
    let data = this.state.data;

    this.sendData(this.SERVICE.create, 'POST', row)
      .then(x => {
        data['venues'].push(row);
        this.setState({ data });
      })
      .catch(err => {
        //
      });
  }

  onDeleteRow(ids, rows) {
    let data = this.state.data;
    this.sendData(this.SERVICE.delete(rows[0].venueId), 'DELETE', rows[0]).then(
      x => {
        data['venues'] = data['venues'].filter(venue => {
          return venue.venueId !== rows[0].venueId;
        });
        this.setState({ data });
      }
    );
  }

  render() {
    const cellEditProp = {
      mode: 'click'
    };
    const selectRow = {
      mode: 'checkbox',
      clickToSelect: true
    };
    let componentData = this.state.data;
    return !componentData ? null : (
      <div>
        <div>
          {this.state.invalidRequest == false ? null : (
            <div class="alert alert-danger" role="alert">
              Action Failed!
            </div>
          )}
        </div>
        <BootstrapTable
          data={this.state.data['venues']}
          selectRow={selectRow}
          remote={this.remote}
          insertRow
          deleteRow
          search
          pagination
          cellEdit={cellEditProp}
          options={{
            onCellEdit: this.onCellEdit.bind(this),
            onDeleteRow: this.onDeleteRow.bind(this),
            onAddRow: this.onAddRow.bind(this),
            sizePerPage: 50
          }}
        >
          <TableHeaderColumn
            dataField="venueId"
            isKey={true}
            search={true}
            multiColumnSearch={true}
            hiddenOnInsert
          >
            ID
          </TableHeaderColumn>
          <TableHeaderColumn dataField="name">Name</TableHeaderColumn>
          <TableHeaderColumn dataField="city">City</TableHeaderColumn>
          <TableHeaderColumn dataField="type">Type</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}
class PlayersTable extends EntityTable {
  constructor(props) {
    super(props);
    let teamId = this.props.match.params.teamId,
      SERVICE_URL = '/admin/api/v1/player';
    this.SERVICE = {
      getAll: '/admin/api/v1/team/' + teamId,
      get: id => SERVICE_URL + '/' + id,
      create: SERVICE_URL,
      update: id => SERVICE_URL + '/' + id,
      delete: id => SERVICE_URL + '/' + id
    };
    this.state = {
      data: null,
      loading: false
    };
  }

  onCellEdit(row, fieldName, value) {
    // row -- raw record
    // fieldName -- name of dataField
    // value -- value put on cell
    const data = Object.assign({}, this.state.data);
    let rowIdx;
    const targetRow = data['roster'].find((record, i) => {
      if (record.playerId === row.playerId) {
        rowIdx = i;
        return true;
      }
      return false;
    });
    if (targetRow) {
      targetRow[fieldName] = value;
      let tgt = Object.assign({}, targetRow);
      data['roster'][rowIdx] = tgt;
      this.sendData(this.SERVICE.update(targetRow.playerId), 'PUT', targetRow)
        .then(x => this.setState({ data }))
        .catch(err => {
          //
        });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.data) {
      this.setState({ data: { roster: [] } });
    } else {
      this.setState({ data: nextProps.data });
    }
  }

  onAddRow(row) {
    let data = Object.assign({}, this.state.data);
    this.sendData(this.SERVICE.create, 'POST', row)
      .then(x => {
        data['roster'].push(row);
        this.setState({ data });
      })
      .catch(err => {
        // Player already exists, so update team of this player
        this.sendData(this.SERVICE.update(row.playerId), 'PUT', row)
          .then(x => {
            data['roster'].push(row);
            this.setState({ data });
          })
          .catch(err => {
            //
          });
      });
  }

  onDeleteRow(ids, rows) {
    let data = this.state.data;
    this.sendData(this.SERVICE.delete(rows[0].playerId), 'DELETE', rows[0])
      .then(x => {
        data['roster'] = data['roster'].filter(player => {
          return player.playerId !== rows[0].playerId;
        });
        this.setState({ data });
      })
      .catch(err => {
        console.log('err');
        //
      });
  }

  render() {
    const cellEditProp = {
      mode: 'click'
    };
    const selectRow = {
      mode: 'checkbox',
      clickToSelect: true
    };
    let data = this.state.data,
      componentData = data ? data : { roster: [] };

    return !componentData ? null : (
      <div>
        <div>
          {this.state.invalidRequest == false ? null : (
            <div class="alert alert-danger" role="alert">
              Action Failed!
            </div>
          )}
        </div>
        <BootstrapTable
          data={componentData['roster']}
          selectRow={selectRow}
          remote={this.remote}
          insertRow
          deleteRow
          search
          pagination
          cellEdit={cellEditProp}
          options={{
            onCellEdit: this.onCellEdit.bind(this),
            onDeleteRow: this.onDeleteRow.bind(this),
            onAddRow: this.onAddRow.bind(this),
            sizePerPage: 50
          }}
        >
          <TableHeaderColumn
            dataField="playerId"
            isKey={true}
            search={true}
            multiColumnSearch={true}
          >
            ID
          </TableHeaderColumn>
          <TableHeaderColumn dataField="name">Name</TableHeaderColumn>
          <TableHeaderColumn dataField="teamId">Team (ID)</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

class FileUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      fileUploadInProgress: false,
      invalidRequest: false
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.fileUpload = this.fileUpload.bind(this);
  }
  onFormSubmit(e) {
    e.preventDefault(); // Stop form submit
    this.fileUpload(this.state.file).then(response => {
      //
    });
  }
  onChange(e) {
    this.setState({ file: e.target.files[0] });
  }

  sendData(url, data) {
    const self = this;
    self.setState({ fileUploadInProgress: true });
    return timeout(
      10000,
      fetch(url, {
        method: 'POST',
        body: data,
        headers: {
          Accept: 'application/json',
          'Access-Control-Allow-Origin': '.*',
          Authorization: `Bearer ${localStorage.getItem('adminToken')}`
        }
      })
    )
      .then(function(response) {
        if (response.status === 404) {
          // redirect to match-setup
          return null;
        } else if (response.status > 400) {
          const error = new Error('Bad response from server');
          error.response = response;
          throw error;
        }
        return response.json();
      })
      .then(respdata => {
        return respdata;
      })
      .catch(err => {
        self.setState({ invalidRequest: true });
        waitFor(2000).then(() => {
          self.setState({ invalidRequest: false });
        });
      });
  }

  fileUpload(file) {
    let apiEndpoint = getLocationLastSegment();

    const url = '/admin/api/v1/upload/' + apiEndpoint;
    const formData = new FormData();
    formData.append('file', file);
    return this.sendData(url, formData);
  }

  render() {
    return (
      <div>
        {this.state.invalidRequest == false ? null : (
          <div class="alert alert-danger" role="alert">
            Upload Failed!
          </div>
        )}
        <form className="form-inline" onSubmit={this.onFormSubmit}>
          <h3>File Upload</h3>
          <div className="form-group">
            <input
              className="form-control"
              type="file"
              onChange={this.onChange}
            />
            <button className="form-control" type="submit">
              Upload
            </button>
          </div>
        </form>
      </div>
    );
  }
}
