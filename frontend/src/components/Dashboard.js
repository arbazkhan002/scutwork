import React from 'react';
import GameHeader from './GameHeader';
import { BubbleLoader } from 'react-css-loaders';
import { timeout, waitFor, getNestedKey } from './utils';
import { Fade } from './Fade';
import Select from 'react-select';
import {
  Button,
  Col,
  ControlLabel,
  Form,
  FormGroup,
  FormControl,
  Modal,
  Navbar
} from 'react-bootstrap';

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // liveMatches is a constant
      liveMatches: null,
      // sets bubble loader for initial boot-time
      componentMounting: true,
      // tracks all filters that have been applied
      activeFilters: new Map(),
      // whether to show live games or all games,
      showAllGames: true
    };
  }

  handleElementClick(matchId) {
    this.props.history.push('/match/' + matchId + '/live');
  }

  componentWillMount() {
    this.fetchData('/api/v1/match').then(data =>
      this.setState({
        liveMatches: data['matches'],
        componentMounting: false
      })
    );
  }

  fetchData(url) {
    const self = this;
    return timeout(
      10000,
      fetch(url, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '.*'
        }
      })
    )
      .then(function(response) {
        if (response.status === 401) {
          // redirect to login
          self.props.resetAuthorization();
          return null;
        } else if (response.status > 400) {
          const error = new Error('Bad response from server');
          error.response = response;
          throw error;
        }
        return response.json();
      })
      .then(data => {
        return data;
      })
      .catch(err => {
        console.log(err);
        throw new Error(JSON.stringify(err));
      });
  }

  handleChange(fieldPath, selectedOption) {
    let liveMatches = this.state.liveMatches,
      activeFilters = this.state.activeFilters;
    this.setState({
      activeFilters: activeFilters.set(fieldPath, selectedOption)
    });
  }

  render() {
    let liveMatches = this.state.liveMatches,
      activeFilters = this.state.activeFilters,
      filteredMatches = Array.from(activeFilters).reduce(
        // accumulator is liveMatches,
        // curr is 2-element array [fieldPath, selectedOption]
        // if selectedOption is null => no filter is set
        (accumulator, curr) =>
          accumulator.filter(
            x => !curr[1] || getNestedKey(x, curr[0]) == curr[1]
          ),
        liveMatches
      ),
      matchesDisplayed = this.state.showAllGames
        ? filteredMatches
        : filteredMatches &&
          filteredMatches.filter(
            x => x.playersHomeTeam && x.playersAwayTeam && x.tossWonBy
          ),
      scorerMatchId = sessionStorage.getItem('scorerMatchId'),
      scorerMatches =
        scorerMatchId &&
        liveMatches &&
        liveMatches.length > 0 &&
        liveMatches.filter(x => scorerMatchId.split(',').includes(x.matchId));

    return this.state.componentMounting ? (
      <div className="row loader-wrapper">
        <BubbleLoader
          className="loader-on-component"
          color="#6f6f6f"
          size={4}
        />
      </div>
    ) : (
      <div>
        <div className="container">
          {!scorerMatches || scorerMatches.length == 0 ? null : (
            <div>
              <h3 className="dashboard-row-title">My Games</h3>
              {scorerMatches
                .sort((a, b) =>
                  [a.timeEpoch, a.groupOrder]
                    .join()
                    .localeCompare([b.timeEpoch, b.groupOrder].join())
                )
                .map(scorerMatch => (
                  <GameHeaderWrapper
                    shortMatchState={scorerMatch}
                    handleElementClick={this.handleElementClick.bind(
                      this,
                      scorerMatch.matchId
                    )}
                    key={'gameheader-' + scorerMatch.matchId}
                  />
                ))}
            </div>
          )}
        </div>
        <div className="container">
          <h3 className="dashboard-row-title">Live Games</h3>
        </div>
        <DashboardFilter
          filterFields={[
            {
              fieldPath: 'gameGroup',
              alias: 'Division'
            },
            {
              fieldPath: 'venue.city',
              alias: 'City'
            }
          ]}
          activeFilters={activeFilters}
          matches={filteredMatches}
          handleChange={this.handleChange.bind(this)}
        />
        <div className="container">
          {!(matchesDisplayed && matchesDisplayed.length > 0) ? (
            <div className="row loader-wrapper">No matches found</div>
          ) : (
            matchesDisplayed
              .sort((a, b) =>
                [a.timeEpoch, a.groupOrder]
                  .join()
                  .localeCompare([b.timeEpoch, b.groupOrder].join())
              )
              .map(matchDetail => (
                <GameHeaderWrapper
                  shortMatchState={matchDetail}
                  handleElementClick={this.handleElementClick.bind(
                    this,
                    matchDetail.matchId
                  )}
                  key={'gameheader-' + matchDetail.matchId}
                />
              ))
          )}
        </div>
      </div>
    );
  }
}

class DashboardFilter extends React.Component {
  constructor(props) {
    super(props);
    let matches = this.props.matches;
    this.allFilters = this.props.filterFields.map(filterField => ({
      options: Array.from(
        new Set(
          matches
            .map(x => getNestedKey(x, filterField.fieldPath))
            .filter(x => x)
        )
      ),
      name: filterField.alias,
      fieldPath: filterField.fieldPath
    }));
  }

  handleChange(fieldPath, selectedOption) {
    return this.props.handleChange(
      fieldPath,
      selectedOption && selectedOption.value
    );
  }

  render() {
    let activeFilters = this.props.activeFilters;
    return (
      <div className="container">
        <div className="row">
          {this.allFilters.map(filter => {
            let myFilters = activeFilters.get(filter.fieldPath);
            let myFilter = myFilters ? myFilters : null;
            return (
              <div className="col-lg-3 col-xs-4">
                <Select
                  name={'division-filter'}
                  placeholder={filter.name}
                  options={filter.options.map(x => ({ value: x, label: x }))}
                  onChange={this.handleChange.bind(this, filter.fieldPath)}
                  value={myFilter}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

class GameHeaderWrapper extends React.Component {
  render() {
    return (
      <div onClick={this.props.handleElementClick}>
        <GameHeader {...this.props} />
      </div>
    );
  }
}
