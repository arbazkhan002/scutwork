import React from 'react';
import { Alert, ProgressBar, Well } from 'react-bootstrap';
import { convertTimestamp } from './utils';

export default class ExpiryTimer extends React.Component {
  constructor(props) {
    super(props);
    let minutes = 60000;
    this.state = {
      timeLeft:
        new Date(this.props.startTime * 1000 + 210 * minutes) - new Date()
    };
  }

  componentDidMount() {
    this.timer = setInterval(this.tick.bind(this), 60 * 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  tick() {
    // This function is called every minute. It updates the
    // elapsed counter.
    let minutes = 60000;
    this.setState({
      timeLeft:
        new Date(this.props.startTime * 1000 + 210 * minutes) - new Date()
    });
  }

  render() {
    // Calculate elapsed to tenth of a second:
    let minutes = 60000,
      secondsLeft = this.state.timeLeft % 1000,
      minutesLeft = Math.floor((this.state.timeLeft / minutes) % 60),
      hoursLeft = Math.floor(this.state.timeLeft / (60 * minutes));

    return (
      <div className="expiry-timer">
        {this.state.timeLeft > 0 ? (
          <div className="header">
            <span className="header-text">Ground permit expires in </span>
            <span className="text-danger timer-text">
              {hoursLeft}:{minutesLeft}
            </span>
          </div>
        ) : (
          <div className="header">
            <span className="header-text">Ground permit expired!</span>
          </div>
        )}
      </div>
    );
  }
}
