import React from 'react';
import Summary from './Summary';
import Scorecard from './Scorecard';
import { BubbleLoader } from 'react-css-loaders';
import { Button } from 'react-bootstrap';
import GameHeader from './GameHeader';
import ReactCSSTransitionReplace from 'react-css-transition-replace';
import { timeout } from './utils';
import Sticky from 'react-stickynode';
import ExpiryTimer from './ExpiryTimer';

class Container extends React.Component {
  constructor(props) {
    super(props);
    this.matchId = this.props.match.params.matchId;
    this.state = {
      matchState: null,
      showScorecard: false
    };
  }

  tick() {
    const self = this;
    let matchState = self.state.matchState;
    let isScorer = self.props.isScorer;
    this.fetchMatchState(this.matchId).then(data => {
      console.log(data);
      if (
        (matchState && matchState.ballSequence) !== (data && data.ballSequence)
      ) {
        self.setState({
          matchState: data
        });
      }
    });

    if (isScorer) {
      clearInterval(self.refreshInterval);
    }
  }

  componentWillUnmount() {
    clearInterval(this.refreshInterval);
  }

  componentDidMount() {
    let isScorer = this.props.isScorer;
    this.tick();
    if (!isScorer) {
      this.refreshInterval = setInterval(this.tick.bind(this), 10000);
    }
  }

  componentWillReceiveProps(nextProps) {
    let isScorer = nextProps.isScorer;
    clearInterval(this.refreshInterval);
    if (!isScorer) {
      this.refreshInterval = setInterval(this.tick.bind(this), 10000);
    }
  }

  fetchMatchState(matchId) {
    /* Check if match-state is already available
    which would mean set-up is not required */
    const self = this;
    const url = '/api/v1/match/' + matchId + '/live';
    return timeout(
      5000,
      fetch(url, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '.*'
        }
      })
    )
      .then(function(response) {
        if (response.status === 404) {
          // redirect to match-setup
          self.props.history.push('/match/' + matchId);
          return null;
        } else if (response.status > 400) {
          const error = new Error('Bad response from server');
          error.response = response;
          throw error;
        }
        return response.json();
      })
      .then(data => {
        return data;
      })
      .catch(err => {
        throw new Error('Network Error');
      });
  }

  handleChange(matchState) {
    this.setState({ matchState: matchState });
  }

  render() {
    let shortMatchState = this.state.matchState,
      timeEpoch = shortMatchState && shortMatchState.matchDetails.timeEpoch;
    return (
      <div className="container" id="scorer">
        {!this.state.matchState || !this.props.isScorer ? null : (
          <ExpiryTimer startTime={timeEpoch} />
        )}

        {this.state.showScorecard ? (
          <GameHeader class="game-header" shortMatchState={shortMatchState} />
        ) : (
          <Sticky
            enabled={true}
            top={0}
            bottomBoundary={'#page-bottom'}
            innerZ={1001}
            activeClass={'fixed-header'}
          >
            {!this.state.matchState ? null : (
              <GameHeader
                class="game-header"
                shortMatchState={Object.assign(
                  {},
                  shortMatchState,
                  shortMatchState.matchDetails
                )}
              />
            )}
          </Sticky>
        )}

        {!this.state.matchState ? (
          <BubbleLoader color="#6f6f6f" size={5} />
        ) : (
          <div>
            <ul className="list-inline screen-toggle">
              <li
                className={
                  'col-xs-6 ' + (!this.state.showScorecard ? 'active' : '')
                }
                onClick={() => this.setState({ showScorecard: false })}
              >
                <h4>Live</h4>
              </li>
              <li
                className={
                  'col-xs-6 ' + (this.state.showScorecard ? 'active' : '')
                }
                onClick={() => this.setState({ showScorecard: true })}
              >
                <h4>Scorecard</h4>
              </li>
              <hr />
            </ul>
            <ReactCSSTransitionReplace
              transitionName="cross-fade"
              transitionEnterTimeout={500}
              transitionLeaveTimeout={200}
            >
              {!this.state.showScorecard ? (
                <Summary
                  runs={0}
                  scorerButtons={['Add ball']}
                  matchState={this.state.matchState}
                  handleChange={this.handleChange.bind(this)}
                  isScorer={this.props.isScorer}
                  logoutCallBack={this.props.logoutCallBack}
                />
              ) : (
                <Scorecard matchId={this.state.matchState.matchId} />
              )}
            </ReactCSSTransitionReplace>
          </div>
        )}
      </div>
    );
  }
}

export default Container;
