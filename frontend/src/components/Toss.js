import React from 'react';
import newId from './newId';
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  ControlLabel,
  DropdownButton,
  FormGroup,
  MenuItem,
  Well
} from 'react-bootstrap';
import Select from 'react-select';
import { unCamelCase } from './utils.js';
import { FlipFade } from './Fade';

class Toss extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      captain1: props.captain1,
      captain2: props.captain2,
      keeper1: props.keeper1,
      keeper2: props.keeper2,
      umpire1: props.umpire1,
      umpire2: props.umpire2,
      tossWonBy: props.tossWonBy,
      teamBattingFirst: props.teamBattingFirst
    };
  }

  componentWillMount() {
    this.id = newId();
  }

  handleDropdownChange(targetField, eventKey) {
    let state = Object.assign({}, this.state);
    state[targetField] = eventKey;
    this.setState({
      [targetField]: eventKey /*ES6 syntax for setting key using variable*/
    });
    this.props.onChange(state);
  }

  handleEdit(targetField, eventKey) {
    let state = Object.assign({}, this.state);
    state[targetField] = eventKey;
    this.setState({
      [targetField]: eventKey /*ES6 syntax for setting key using variable*/
    });
    this.props.onChange(state);
  }

  setToss() {
    this.props.onSave(this.state);
  }

  render() {
    const team1 = this.props.team1,
      team2 = this.props.team2,
      team1Players = this.props.team1Players,
      team2Players = this.props.team2Players,
      umpire1Players = this.props.umpire1Players,
      umpire2Players = this.props.umpire2Players,
      formControlId = 'match-toss',
      content = [
        {
          dropdownTag: 'Home Captain',
          presetValue: this.state.captain1,
          dropdownOptions: team1Players,
          targetField: 'captain1',
          labelKey: 'name',
          valueKey: 'playerId'
        },
        {
          dropdownTag: 'Home WicketKeeper',
          presetValue: this.state.keeper1,
          dropdownOptions: team1Players,
          targetField: 'keeper1',
          labelKey: 'name',
          valueKey: 'playerId'
        },
        {
          dropdownTag: 'Away Captain',
          presetValue: this.state.captain2,
          dropdownOptions: team2Players,
          targetField: 'captain2',
          labelKey: 'name',
          valueKey: 'playerId'
        },
        {
          dropdownTag: 'Away WicketKeeper',
          presetValue: this.state.keeper2,
          dropdownOptions: team2Players,
          targetField: 'keeper2',
          labelKey: 'name',
          valueKey: 'playerId'
        },
        {
          dropdownTag: 'Umpire 1',
          presetValue: this.state.umpire1,
          dropdownOptions: umpire1Players,
          targetField: 'umpire1',
          labelKey: 'name',
          valueKey: 'playerId'
        },
        {
          dropdownTag: 'Umpire 2',
          presetValue: this.state.umpire2,
          dropdownOptions: umpire2Players,
          targetField: 'umpire2',
          labelKey: 'name',
          valueKey: 'playerId'
        },
        {
          dropdownTag: 'Toss Won By',
          presetValue: this.state.tossWonBy,
          dropdownOptions: [team1, team2],
          targetField: 'tossWonBy',
          labelKey: 'teamName',
          valueKey: 'teamId'
        },
        {
          dropdownTag: 'Team Batting First',
          presetValue: this.state.teamBattingFirst,
          dropdownOptions: [team1, team2],
          targetField: 'teamBattingFirst',
          labelKey: 'teamName',
          valueKey: 'teamId'
        }
      ],
      self = this,
      filledContent = content.filter(
        formContent => self.state[formContent.targetField]
      ),
      emptyContent = content.filter(
        formContent => !self.state[formContent.targetField]
      );
    return (
      <div className="container">
        {!filledContent.length ? null : (
          <div className="row">
            <div className="col-xs-10">
              <h4>Match Details</h4>
              <table className="table property-table">
                <tbody>
                  <FlipFade>
                    {filledContent.map(formContent => {
                      return (
                        <tr
                          key={formContent.dropdownTag}
                          onClick={this.handleEdit.bind(
                            this,
                            formContent.targetField,
                            null
                          )}
                        >
                          <td className="property-label">
                            {formContent.dropdownTag}
                          </td>
                          <td className="property-value">
                            {
                              self.state[formContent.targetField][
                                formContent.labelKey
                              ]
                            }
                          </td>
                        </tr>
                      );
                    })}
                  </FlipFade>
                </tbody>
              </table>
            </div>
          </div>
        )}
        <div className="row action-row">
          <div className="col-xs-1 col-md-3" />
          <div className="col-xs-10 col-md-6">
            <ButtonGroup justified className="ballevent-actions">
              <Button
                href="#"
                bsStyle="danger"
                className="dismiss"
                //onClick={this.sendBallEventTest.bind(this)}
                onClick={this.props.onBack}
              >
                Back
              </Button>
              <Button
                href="#"
                bsStyle="success"
                disabled={
                  !(this.state.teamBattingFirst && this.state.tossWonBy)
                }
                onClick={this.setToss.bind(this)}
              >
                Start Game
              </Button>
            </ButtonGroup>
          </div>
          <div className="col-xs-1 col-md-3" />
        </div>
        <div className="row">
          <div className="col-xs-12 event-setup">
            <FormGroup controlId={formControlId}>
              <FlipFade>
                {emptyContent.length > 0 &&
                  emptyContent.slice(0, 2).map(formContent => (
                    <Well
                      key={this.id + formContent.targetField + '-toss-well'}
                    >
                      <ButtonToolbar>
                        <TeamSelectionMenu
                          labelKey={formContent.labelKey}
                          valueKey={formContent.valueKey}
                          handleDropdownChange={this.handleDropdownChange.bind(
                            this,
                            formContent.targetField
                          )}
                          {...formContent}
                        />
                      </ButtonToolbar>
                    </Well>
                  ))}
              </FlipFade>
            </FormGroup>
          </div>
        </div>{' '}
        {/* end-toss-selection*/}
      </div>
    );
  }
}

class TeamSelectionMenu extends React.Component {
  componentWillMount() {
    this.id = newId();
  }

  handleDropdownChange(selectedOption) {
    this.props.handleDropdownChange(selectedOption);
  }

  render() {
    const presetValue = this.props.presetValue,
      dropdownOptions = this.props.dropdownOptions,
      dropdownTag = this.props.dropdownTag,
      labelKey = this.props.labelKey,
      valueKey = this.props.valueKey;
    return (
      <div className="section">
        <h5 className="section-heading">{dropdownTag}</h5>
        <Select
          name={dropdownTag}
          labelKey={labelKey}
          value={presetValue}
          valueKey={valueKey}
          options={dropdownOptions}
          required={true}
          onChange={this.handleDropdownChange.bind(this)}
        />
      </div>
    );
  }
}

export default Toss;
