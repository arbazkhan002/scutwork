import React from 'react';
import Toss from './Toss';
import PlayingXI from './PlayingXI';
import { GameConstants } from './Constants';
import { Nav, NavItem } from 'react-bootstrap';
import { BubbleLoader } from 'react-css-loaders';

export default class MatchSetup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      matchSetupState: null,
      stepActive: 1
    };
  }

  componentDidMount() {
    const self = this;
    let matchId = this.props.match.params.matchId;
    self.fetchMatchDetails(matchId).then(matchDetails => {
      matchDetails.playersHomeTeam =
        matchDetails.playersHomeTeam.length > 0
          ? matchDetails.playersHomeTeam
          : null;
      matchDetails.playersAwayTeam =
        matchDetails.playersAwayTeam.length > 0
          ? matchDetails.playersAwayTeam
          : null;
      let homeTeam = matchDetails.homeTeam,
        awayTeam = matchDetails.awayTeam,
        umpiringTeam1 = matchDetails.umpiringTeam1,
        umpiringTeam2 = matchDetails.umpiringTeam2,
        tossWonBy = matchDetails.tossWonBy,
        teamBattingFirst = matchDetails.teamBattingFirst,
        playersHomeTeam = matchDetails.playersHomeTeam,
        playersAwayTeam = matchDetails.playersAwayTeam,
        /* initialize undefined variables to null */
        captain1 = matchDetails.captain1 || null,
        captain2 = matchDetails.captain2 || null,
        umpire1 = matchDetails.umpire1 || null,
        umpire2 = matchDetails.umpire2 || null,
        scoringEnabled = matchDetails.scoringEnabled,
        matchDate = matchDetails.matchDate,
        matchTime = matchDetails.matchTime;
      const matchSetupState = {
        matchId: matchDetails.matchId,
        homeTeam: matchDetails.homeTeam,
        awayTeam: awayTeam,
        playersHomeTeam: playersHomeTeam,
        playersAwayTeam: playersAwayTeam,
        captain1: captain1,
        captain2: captain2,
        umpire1: umpire1,
        umpire2: umpire2,
        maxOvers: matchDetails.maxOvers,
        toss: {
          tossWonBy: tossWonBy,
          teamBattingFirst: teamBattingFirst
        },
        scoringEnabled: scoringEnabled,
        matchDate: matchDate,
        matchTime: matchTime
      };
      if (playersHomeTeam && playersAwayTeam && tossWonBy) {
        self.props.history.push('/match/' + matchId + '/live');
        return null;
      }
      self
        .pullMatchPlayers(matchId)
        .then(data => {
          return matchSetupState;
        })
        .then(matchSetupState => self.fetchRoster(homeTeam.teamId))
        .then(teamDetails => {
          matchSetupState.homeTeamRoster = teamDetails.roster;
          return matchSetupState;
        })
        .then(matchSetupState => self.fetchRoster(awayTeam.teamId))
        .then(teamDetails => {
          matchSetupState.awayTeamRoster = teamDetails.roster;
          return matchSetupState;
        })
        .then(matchSetupState => self.fetchRoster(umpiringTeam1.teamId))
        .then(teamDetails => {
          matchSetupState.umpiringTeam1Roster = teamDetails.roster;
          return matchSetupState;
        })
        .then(matchSetupState => self.fetchRoster(umpiringTeam2.teamId))
        .then(teamDetails => {
          matchSetupState.umpiringTeam2Roster = teamDetails.roster;
          this.setState({
            matchSetupState: matchSetupState,
            stepActive: 1
          });
        })
        .catch(err => {
          throw new Error(err);
        });
    });
  }

  pullMatchPlayers(matchId) {
    return fetch('/api/v1/match_players/' + matchId, {
      method: 'POST',
      body: JSON.stringify({
        remoteApiToken: sessionStorage.getItem('remoteApiToken')
      }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '.*'
      }
    })
      .then(function(response) {
        if (response.status >= 400) {
          throw new Error('error: ' + response.statusText);
        }
        return response.json();
      })
      .then(data => {
        return data;
      });
  }

  fetchMatchDetails(matchId) {
    return fetch('/api/v1/match/' + matchId, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '.*'
      }
    })
      .then(function(response) {
        if (response.status >= 400) {
          throw new Error('error: ' + response.statusText);
        }
        return response.json();
      })
      .then(data => {
        return data;
      });
  }

  fetchRoster(teamId) {
    return fetch('/api/v1/team/' + teamId, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '.*'
      }
    })
      .then(function(response) {
        if (response.status >= 400) {
          throw new Error('error');
        }
        return response.json();
      })
      .then(data => {
        return data;
      });
  }

  setPlayingXI(playersHomeTeam, playersAwayTeam, stepActive) {
    let matchSetupState = this.state.matchSetupState;
    playersHomeTeam && (matchSetupState.playersHomeTeam = playersHomeTeam);
    playersAwayTeam && (matchSetupState.playersAwayTeam = playersAwayTeam);
    this.setState({
      matchSetupState: matchSetupState,
      stepActive: stepActive
    });
  }

  saveToss(toss) {
    console.log(toss);
    let matchSetupState = this.state.matchSetupState;
    matchSetupState.toss.tossWonBy = toss.tossWonBy;
    matchSetupState.toss.teamBattingFirst = toss.teamBattingFirst;
    (matchSetupState.captain1 = toss.captain1),
      (matchSetupState.captain2 = toss.captain2),
      (matchSetupState.umpire1 = toss.umpire1),
      (matchSetupState.umpire2 = toss.umpire2),
      (matchSetupState.keeper1 = toss.keeper1),
      (matchSetupState.keeper2 = toss.keeper2);
    this.setState({
      matchSetupState: matchSetupState
    });
  }

  setToss(toss) {
    this.saveToss(toss);
    this.setState({
      stepActive: 4
    });
  }

  unsetToss(toss) {
    let matchSetupState = this.state.matchSetupState;
    this.setState({
      matchSetupState: matchSetupState,
      stepActive: 2
    });
  }

  endSetup() {
    const self = this;
    let matchSetupState = self.state.matchSetupState,
      matchId = matchSetupState.matchId,
      matchDetails = {
        matchId: matchId,
        homeTeam: matchSetupState.homeTeam.teamId,
        awayTeam: matchSetupState.awayTeam.teamId,
        playersHomeTeam: matchSetupState.playersHomeTeam,
        playersAwayTeam: matchSetupState.playersAwayTeam,
        tossWonBy: matchSetupState.toss.tossWonBy,
        teamBattingFirst: matchSetupState.toss.teamBattingFirst,
        captain1: matchSetupState.captain1,
        captain2: matchSetupState.captain2,
        umpire1: matchSetupState.umpire1,
        umpire2: matchSetupState.umpire2,
        keeper1: matchSetupState.keeper1,
        keeper2: matchSetupState.keeper2
      };

    fetch('/api/v1/match/' + matchId + '/live', {
      method: 'POST',
      body: JSON.stringify(matchDetails),
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('authToken')}`,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '.*'
      }
    })
      .then(function(response) {
        setTimeout(() => null, 0);
        return response.json();
      })
      .then(data => {
        console.log(data);
        this.props.history.push('/match/' + matchId + '/live');
        return null;
      })
      .catch(err => {
        throw new Error('error');
      });
  }

  handleSelect(selectedKey) {
    alert(`selected ${selectedKey}`);
  }

  render() {
    let matchSetupState = this.state.matchSetupState,
      currentTargetTeam = this.state.currentTargetTeam,
      stepActive = this.state.stepActive,
      isScorer = this.props.isScorer,
      isScoringEnabled = matchSetupState && matchSetupState.scoringEnabled;

    if (!matchSetupState) {
      return <BubbleLoader color="#6f6f6f" size={5} />;
    } else if (!isScorer) {
      return (
        <div class="container container-table">
          <div class="row vertical-center-row">
            <div className="well col-xs-12 meta-text">
              <h4>Waiting for the match to start</h4>
              <span className="light-meta-text">
                If you are scorer for this game,{' '}
                <a onClick={this.props.getLogin}>Login</a> to begin scoring.
              </span>
            </div>
          </div>
        </div>
      );
    } else if (!isScoringEnabled) {
      return (
        <div class="container container-table">
          <div class="row vertical-center-row">
            <div className="well col-xs-12 meta-text">
              <h4>Scoring not yet enabled for this game.</h4>
              <span className="light-meta-text">
                Game is scheduled to start at{' '}
                {matchSetupState &&
                  matchSetupState.matchDate + ' ' + matchSetupState.matchTime}
              </span>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="container">
          <ul className="list-inline screen-toggle col-xs-12">
            <li className={'col-xs-4 ' + (stepActive == 1 ? 'active' : '')}>
              Home Team
            </li>
            <li className={'col-xs-4 ' + (stepActive == 2 ? 'active' : '')}>
              Away Team
            </li>
            <li className={'col-xs-4 ' + (stepActive == 3 ? 'active' : '')}>
              Toss
            </li>
            <hr className="divides-three" />
          </ul>
          <div>
            {!matchSetupState || stepActive < 3 ? (
              <PlayingXI
                homeTeamRoster={matchSetupState.homeTeamRoster}
                homeTeam={matchSetupState.playersHomeTeam}
                awayTeamRoster={matchSetupState.awayTeamRoster}
                awayTeam={matchSetupState.playersAwayTeam}
                setPlayingXI={this.setPlayingXI.bind(this)}
                currentTargetTeam={
                  this.state.stepActive == 1 ? 'team1' : 'team2'
                }
              />
            ) : matchSetupState && stepActive == 3 ? (
              <Toss
                team1={matchSetupState.homeTeam}
                team2={matchSetupState.awayTeam}
                team1Players={matchSetupState.playersHomeTeam}
                team2Players={matchSetupState.playersAwayTeam}
                umpire1Players={matchSetupState.umpiringTeam1Roster}
                umpire2Players={matchSetupState.umpiringTeam2Roster}
                onSave={this.setToss.bind(this)}
                onBack={this.unsetToss.bind(this)}
                onChange={this.saveToss.bind(this)}
                tossWonBy={matchSetupState.toss.tossWonBy}
                teamBattingFirst={matchSetupState.toss.teamBattingFirst}
                captain1={matchSetupState.captain1}
                captain2={matchSetupState.captain2}
                keeper1={matchSetupState.keeper1}
                keeper2={matchSetupState.keeper2}
                umpire1={matchSetupState.umpire1}
                umpire2={matchSetupState.umpire2}
              />
            ) : matchSetupState && stepActive == 4 ? (
              this.endSetup()
            ) : null}
          </div>
        </div>
      );
    }
  }
}
