import React from 'react';

import { Button, ButtonToolbar, Modal } from 'react-bootstrap';

export default class DialogModal extends React.Component {
  onConfirm() {
    this.props.onConfirm();
    this.props.onHide();
  }

  render() {
    let props = this.props;
    return (
      <Modal {...props} bsSize="sm" aria-labelledby="contained-modal-title-sm">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-sm">
            {props.heading}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{props.content}</p>
        </Modal.Body>
        <Modal.Footer className="dialog-footer">
          <ButtonToolbar className="dialog-button-toolbar">
            <Button bsStyle="default" onClick={props.onHide}>
              Cancel
            </Button>
            <Button bsStyle="danger" onClick={this.onConfirm.bind(this)}>
              Confirm
            </Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}
