import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import 'whatwg-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './resources/transitions.css';
import './resources/admin.css';
import 'react-select/dist/react-select.css';
import registerServiceWorker from './registerServiceWorker';
import Home from './components/Home';
import { BrowserRouter } from 'react-router-dom';

ReactDOM.render(
  <BrowserRouter>
    <Home />
  </BrowserRouter>,
  document.getElementById('ball-event-maker')
);

registerServiceWorker();
